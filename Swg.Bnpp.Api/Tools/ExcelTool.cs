﻿using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Swg.Bnpp.Models.Mnv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Bnpp.Api.Tools
{
    public class ExcelTool
    {
        private readonly string ServiceName = "ExcelTool";
        public string GetErrorMessage(string MethodeName, Exception ex)
        {
            string errMessage = ex.Message;
            if (ex.InnerException != null)
            {
                errMessage = ex.InnerException.Message;
                if (ex.InnerException.InnerException != null)
                {
                    errMessage = ex.InnerException.InnerException.Message;
                    if (ex.InnerException.InnerException.InnerException != null)
                    {
                        errMessage = ex.InnerException.InnerException.InnerException.Message;
                    }
                }
            }
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return MethodeName + ", line: " + lineNumber + Environment.NewLine + "Error Message: " + errMessage;
        }
        public string InitExcelAllSheet(int IdUser, string FileName, out XSSFWorkbook xssfwb, out string OutFileName)
        {
            xssfwb = null;
            OutFileName = string.Empty;
            try
            {
                string TemplateFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("Files/Templates/{0}.xlsx", FileName));
                OutFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, IdUser.ToString() + ".xlsx");
                if (File.Exists(OutFileName))
                    File.Delete(OutFileName);

                xssfwb = new XSSFWorkbook();
                Console.WriteLine(OutFileName);
                FileStream filex = new FileStream(TemplateFileName, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string InitExcel(int IdUser, string FileName, out XSSFWorkbook xssfwb, out string OutFileName)
        {
            xssfwb = null;
            OutFileName = string.Empty;
            try
            {
                string TemplateFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("Templates/{0}.xlsx", FileName));
                OutFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, IdUser.ToString() + ".xlsx");
                if (File.Exists(OutFileName))
                    File.Delete(OutFileName);

                xssfwb = new XSSFWorkbook();
                Console.WriteLine(OutFileName);
                FileStream filex = new FileStream(TemplateFileName, FileMode.Open, FileAccess.Read);
                xssfwb = new XSSFWorkbook(filex);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string GetColumnName(int index)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var value = "";

            if (index >= letters.Length)
                value += letters[index / letters.Length - 1];

            value += letters[index % letters.Length];

            return value;
        }
        public string GetCellErrorMessage(string SheetName, int row, int cell)
        {
            return string.Format("sheet {0} baris {1} kolom {2} salah", SheetName, row + 1, GetColumnName(cell));
        }
        public string GetStringValue(ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;
                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return string.Empty;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return string.Empty;
                }
                return cell.ToString();
            }
            catch (Exception ex)
            {
                oMessage = err + ": " + ex.Message;
                return null;
            }
        }
        public double? GetNumberValue(XSSFWorkbook xssfwb, ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;

                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return null;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return 0;
                }
                XSSFFormulaEvaluator formula = new XSSFFormulaEvaluator(xssfwb);
                if (cell.CellType == CellType.Formula)
                    formula.Evaluate(cell);
                return double.Parse(cell.NumericCellValue.ToString());
            }
            catch (Exception ex)
            {
                oMessage = err + ": " + ex.Message;
                return null;
            }
        }
        public int? GetIntValue(XSSFWorkbook xssfwb, ISheet sheet, int rowNum, int cellNum, out string oMessage)
        {
            string err = GetCellErrorMessage(sheet.SheetName, rowNum, cellNum);
            try
            {
                oMessage = string.Empty;

                IRow row = sheet.GetRow(rowNum);
                if (row == null)
                {
                    oMessage = err + ": baris kosong";
                    return null;
                }
                ICell cell = row.GetCell(cellNum);
                if (cell == null)
                {
                    return 0;
                }
                XSSFFormulaEvaluator formula = new XSSFFormulaEvaluator(xssfwb);
                if (cell.CellType == CellType.Formula)
                    formula.Evaluate(cell);
                return int.Parse(cell.NumericCellValue.ToString());
            }
            catch (Exception ex)
            {
                oMessage = err + ": " + ex.Message;
                return null;
            }
        }
        public IFont SetFontStyle(XSSFWorkbook xssfwb)
        {
            XSSFFont font = (XSSFFont)xssfwb.CreateFont();
            font.FontHeightInPoints = 12;
            font.IsBold = true;

            IFont ifont = xssfwb.CreateFont();
            ifont.FontHeightInPoints = 12;
            ifont.IsItalic = false;
            return ifont;

        }
        public XSSFCellStyle SetTextStyle(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle TextStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            TextStyle.BorderBottom = BorderStyle.Thin;
            TextStyle.BorderLeft = BorderStyle.Thin;
            TextStyle.BorderRight = BorderStyle.Thin;
            TextStyle.BorderTop = BorderStyle.Thin;
            TextStyle.DataFormat = (short)CellType.String;
            TextStyle.SetFont(SetFontStyle(xssfwb));
            return TextStyle;
        }
        public XSSFCellStyle SetRpStyle(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle RpStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            RpStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0.00");
            RpStyle.BorderBottom = BorderStyle.Thin;
            RpStyle.BorderLeft = BorderStyle.Thin;
            RpStyle.BorderRight = BorderStyle.Thin;
            RpStyle.BorderTop = BorderStyle.Thin;
            RpStyle.Alignment = HorizontalAlignment.Right;
            RpStyle.SetFont(SetFontStyle(xssfwb));
            return RpStyle;
        }
        public XSSFCellStyle SetDoubleStyle(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle RpStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();
            RpStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0.00000000000000");
            RpStyle.BorderBottom = BorderStyle.Thin;
            RpStyle.BorderLeft = BorderStyle.Thin;
            RpStyle.BorderRight = BorderStyle.Thin;
            RpStyle.BorderTop = BorderStyle.Thin;
            RpStyle.Alignment = HorizontalAlignment.Right;
            RpStyle.SetFont(SetFontStyle(xssfwb));
            return RpStyle;
        }
        public XSSFCellStyle SetNumberStyle(XSSFWorkbook xssfwb)
        {
            XSSFCellStyle NumberStyle = (XSSFCellStyle)xssfwb.CreateCellStyle();

            NumberStyle.DataFormat = xssfwb.CreateDataFormat().GetFormat("#,##0");
            NumberStyle.BorderBottom = BorderStyle.Thin;
            NumberStyle.BorderLeft = BorderStyle.Thin;
            NumberStyle.BorderRight = BorderStyle.Thin;
            NumberStyle.BorderTop = BorderStyle.Thin;
            NumberStyle.Alignment = HorizontalAlignment.Right;
            NumberStyle.SetFont(SetFontStyle(xssfwb));
            return NumberStyle;
        }



        public string GenerateLaporanRekap(int IdUser, int Tahun, List<LaporanRekapModel> Rekaps, List<LaporanDaftarModel> DetilSudahInput, List<LaporanDaftarModel> DetilsBelumInput, out string OutFileName)
        {
            string templateFile = "TemplateLaporan";
            string pathTemplateFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("Templates/{0}.xlsx", templateFile));
            OutFileName = string.Empty;
            try
            {
                if (Rekaps == null || Rekaps.Count() == 0)
                {
                    OutFileName = pathTemplateFile;
                }
                else
                {
                    string sheetRekapName = string.Format("Rekap_{0}", Tahun.ToString());
                    string sheetDetilInputName = string.Format("DetilInput_{0}", Tahun.ToString());
                    string sheetDetilBelumInputName = string.Format("DetilBelumInput_{0}", Tahun.ToString());

                    string err = InitExcel(IdUser, templateFile, out XSSFWorkbook xssfwb, out OutFileName);
                    if (Rekaps != null)
                    {
                        XSSFCellStyle numberStyle = SetNumberStyle(xssfwb);
                        XSSFCellStyle textStyle = SetTextStyle(xssfwb);

                        ISheet sheetTemplateRekap = xssfwb.GetSheet("rekap");
                        if (sheetTemplateRekap == null)
                        {
                            OutFileName = pathTemplateFile;
                            return string.Format("sheet {0} tidak ada di file {1}", "rekap", "TemplateLaporan");
                        }
                        sheetTemplateRekap.CopySheet(sheetRekapName);
                        ISheet sheetRekap = xssfwb.GetSheet(sheetRekapName);
                        sheetRekap.GetRow(1).GetCell(0).SetCellValue(string.Format("Rekap Monitoring dan Evaluasi"));
                        sheetRekap.GetRow(1).GetCell(0).SetCellValue(string.Format("Periode Tahun {0}", Tahun));
                        int _row = 5;
                        foreach (var item in Rekaps)
                        {
                            IRow row = sheetRekap.CreateRow(_row);
                            for (int _cell = 0; _cell <= 3; _cell++)
                            {
                                row.CreateCell(_cell);
                                switch (_cell)
                                {
                                    case 0:
                                    case 2:
                                    case 3:
                                        row.GetCell(_cell).CellStyle = numberStyle;
                                        break;
                                    default:
                                        row.GetCell(_cell).CellStyle = textStyle;
                                        break;
                                }
                            }
                            int _cellNo = 0;
                            row.GetCell(_cellNo).SetCellValue(item.No);
                            row.GetCell(++_cellNo).SetCellValue(item.Provinsi);
                            row.GetCell(++_cellNo).SetCellValue(item.SudahInput);
                            row.GetCell(++_cellNo).SetCellValue(item.BelumInput);
                            _row++;
                        }

                        ISheet sheetTemplateDetil = xssfwb.GetSheet("daftar");
                        if (sheetTemplateDetil == null)
                        {
                            OutFileName = pathTemplateFile;
                            return string.Format("sheet {0} tidak ada di file {1}", "detil", "TemplateLaporan");
                        }

                        sheetTemplateDetil.CopySheet(sheetDetilInputName);
                        ISheet sheetDetilInput = xssfwb.GetSheet(sheetDetilInputName);
                        sheetDetilInput.GetRow(0).GetCell(0).SetCellValue(string.Format("Daftar Monitoring dan Evaluasi (SUDAH INPUT)"));
                        sheetDetilInput.GetRow(1).GetCell(0).SetCellValue(string.Format("Periode Tahun {0}", Tahun));
                        _row = 4;
                        foreach (var item in DetilSudahInput)
                        {
                            IRow row = sheetDetilInput.CreateRow(_row);
                            for (int _cell = 0; _cell <= 14; _cell++)
                            {
                                row.CreateCell(_cell);
                                switch (_cell)
                                {
                                    case 0:
                                    case 9:
                                    case 10:
                                        row.GetCell(_cell).CellStyle = numberStyle;
                                        break;
                                    default:
                                        row.GetCell(_cell).CellStyle = textStyle;
                                        break;
                                }
                            }
                            int _cellNo = 0;
                            row.GetCell(_cellNo).SetCellValue(item.No);
                            row.GetCell(++_cellNo).SetCellValue(item.NamaPejabat);
                            row.GetCell(++_cellNo).SetCellValue(item.Jabatan);
                            row.GetCell(++_cellNo).SetCellValue(item.Email);
                            row.GetCell(++_cellNo).SetCellValue(item.NoHp);
                            row.GetCell(++_cellNo).SetCellValue(item.AlamatKantor);
                            row.GetCell(++_cellNo).SetCellValue(item.Kecamatan);
                            row.GetCell(++_cellNo).SetCellValue(item.Kabupaten);
                            row.GetCell(++_cellNo).SetCellValue(item.Provinsi);
                            row.GetCell(++_cellNo).SetCellValue(item.Ciq);
                            row.GetCell(++_cellNo).SetCellValue(item.Pap);
                            row.GetCell(++_cellNo).SetCellValue(item.JenisLokpri);
                            row.GetCell(++_cellNo).SetCellValue(item.PetugasInput);
                            row.GetCell(++_cellNo).SetCellValue(item.TanggalInput);
                            row.GetCell(++_cellNo).SetCellValue(item.JamInput);
                            _row++;
                        }

                        sheetTemplateDetil.CopySheet(sheetDetilBelumInputName);
                        ISheet sheetDetilBelumInput = xssfwb.GetSheet(sheetDetilBelumInputName);
                        sheetDetilBelumInput.GetRow(0).GetCell(0).SetCellValue(string.Format("Daftar Monitoring dan Evaluasi (BELUM INPUT)"));
                        sheetDetilBelumInput.GetRow(1).GetCell(0).SetCellValue(string.Format("Periode Tahun {0}", Tahun));
                        _row = 4;
                        foreach (var item in DetilsBelumInput)
                        {
                            IRow row = sheetDetilBelumInput.CreateRow(_row);
                            for (int _cell = 0; _cell <= 14; _cell++)
                            {
                                row.CreateCell(_cell);
                                switch (_cell)
                                {
                                    case 0:
                                    case 9:
                                    case 10:
                                        row.GetCell(_cell).CellStyle = numberStyle;
                                        break;
                                    default:
                                        row.GetCell(_cell).CellStyle = textStyle;
                                        break;
                                }
                            }
                            int _cellNo = 0;
                            row.GetCell(_cellNo).SetCellValue(item.No);
                            row.GetCell(++_cellNo).SetCellValue(item.NamaPejabat);
                            row.GetCell(++_cellNo).SetCellValue(item.Jabatan);
                            row.GetCell(++_cellNo).SetCellValue(item.Email);
                            row.GetCell(++_cellNo).SetCellValue(item.NoHp);
                            row.GetCell(++_cellNo).SetCellValue(item.AlamatKantor);
                            row.GetCell(++_cellNo).SetCellValue(item.Kecamatan);
                            row.GetCell(++_cellNo).SetCellValue(item.Kabupaten);
                            row.GetCell(++_cellNo).SetCellValue(item.Provinsi);
                            row.GetCell(++_cellNo).SetCellValue(item.Ciq);
                            row.GetCell(++_cellNo).SetCellValue(item.Pap);
                            row.GetCell(++_cellNo).SetCellValue(item.JenisLokpri);
                            row.GetCell(++_cellNo).SetCellValue(item.PetugasInput);
                            row.GetCell(++_cellNo).SetCellValue(item.TanggalInput);
                            row.GetCell(++_cellNo).SetCellValue(item.JamInput);
                            _row++;
                        }
                        xssfwb.RemoveAt(xssfwb.GetSheetIndex("rekap"));
                        xssfwb.RemoveAt(xssfwb.GetSheetIndex("daftar"));
                        xssfwb.RemoveAt(xssfwb.GetSheetIndex("detil"));
                        xssfwb.RemoveAt(xssfwb.GetSheetIndex("petugas"));
                    }

                    FileStream file = new FileStream(OutFileName, FileMode.Create);
                    xssfwb.Write(file);
                    xssfwb.Close();
                    file.Close();
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                OutFileName = pathTemplateFile;
                return GetErrorMessage(ServiceName + "GenerateLaporan", ex);
            }
        }
        public string GenerateLaporanDetil(int IdUser, LaporanDetilMonevModel Detil, out string OutFileName)
        {
            string templateFile = "TemplateLaporan";
            string pathTemplateFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("Templates/{0}.xlsx", templateFile));
            OutFileName = string.Empty;
            try
            {
                if (Detil == null)
                {
                    OutFileName = pathTemplateFile;
                }
                else
                {
                    string sheetDetilName = string.Format("Detil_{0}", Detil.Tahun.ToString());
                    string sheetPetugasName = string.Format("Petugas_{0}", Detil.Tahun.ToString());

                    string err = InitExcel(IdUser, templateFile, out XSSFWorkbook xssfwb, out OutFileName);
                    if (!string.IsNullOrEmpty(err))
                    {
                        OutFileName = pathTemplateFile;
                        return err;
                    }
                    XSSFCellStyle numberStyle = SetNumberStyle(xssfwb);
                    XSSFCellStyle textStyle = SetTextStyle(xssfwb);

                    ISheet sheetTemplateDetil = xssfwb.GetSheet("detil");
                    if (sheetTemplateDetil == null)
                    {
                        OutFileName = pathTemplateFile;
                        return string.Format("sheet {0} tidak ada di file {1}", "detil", "TemplateLaporan");
                    }
                    sheetTemplateDetil.CopySheet(sheetDetilName);
                    ISheet sheetDetil = xssfwb.GetSheet(sheetDetilName);
                    sheetDetil.GetRow(0).GetCell(0).SetCellValue(string.Format("Detil Input Monitoring dan Evaluasi"));
                    sheetDetil.GetRow(1).GetCell(0).SetCellValue(string.Format("Periode Tahun {0}", Detil.Tahun.ToString()));
                    sheetDetil.GetRow(2).GetCell(0).SetCellValue(string.Format("Provinsi: {0}, Kabupaten/Kota: {1}, Kecamatan: {0}", Detil.Provinsi, Detil.Kabupaten, Detil.Kecamatan));

                    int _row = 3;
                    foreach (var itemVariabel in Detil.Monev.Variabels)
                    {
                        IRow row = sheetDetil.CreateRow(++_row);
                        row.CreateCell(0);
                        //sheetDetil.GetRow(_row).GetCell(0).CellStyle = textStyle;
                        row.GetCell(0).SetCellValue("VARIABEL: " + itemVariabel.Variabel);
                        row.CreateCell(2);
                        row.GetCell(2).SetCellValue("Prosentase: " + itemVariabel.Pct);

                        foreach (var itemIndikator in itemVariabel.Indikators)
                        {
                            sheetDetil.CreateRow(++_row).CreateCell(0);
                            //sheetDetil.GetRow(_row).GetCell(0).CellStyle = textStyle;
                            sheetDetil.GetRow(_row).GetCell(0).SetCellValue(itemIndikator.Indikator);

                            row = sheetDetil.CreateRow(++_row);
                            row.CreateCell(0);
                            row.GetCell(0).CellStyle = textStyle;
                            row.GetCell(0).SetCellValue("No");

                            row.CreateCell(1);
                            row.GetCell(1).CellStyle = textStyle;
                            row.GetCell(1).SetCellValue("Pertanyaan");

                            switch (itemIndikator.Type)
                            {
                                case 1:
                                    row.CreateCell(2);
                                    row.GetCell(2).CellStyle = textStyle;
                                    row.GetCell(2).SetCellValue("Ketersediaan");

                                    row.CreateCell(3);
                                    row.GetCell(3).CellStyle = textStyle;
                                    row.GetCell(3).SetCellValue("Bobot Ketersediaan");

                                    row.CreateCell(4);
                                    row.GetCell(4).CellStyle = textStyle;
                                    row.GetCell(4).SetCellValue("Kecukupan");

                                    row.CreateCell(5);
                                    row.GetCell(5).CellStyle = textStyle;
                                    row.GetCell(5).SetCellValue("Bobot Kecukupan");

                                    row.CreateCell(6);
                                    row.GetCell(6).CellStyle = textStyle;
                                    row.GetCell(6).SetCellValue("Kondisi");

                                    row.CreateCell(7);
                                    row.GetCell(7).CellStyle = textStyle;
                                    row.GetCell(7).SetCellValue("Bobot Kondisi");
                                    break;
                                case 2:
                                    row.CreateCell(2);
                                    row.GetCell(2).CellStyle = textStyle;
                                    row.GetCell(2).SetCellValue("Kebutuhan");

                                    row.CreateCell(3);
                                    row.GetCell(3).CellStyle = textStyle;
                                    row.GetCell(3).SetCellValue("Keterisian");

                                    row.CreateCell(4);
                                    row.GetCell(4).CellStyle = textStyle;
                                    row.GetCell(4).SetCellValue("Bobot Kebutuhan-Keterisian");
                                    break;
                                case 3:
                                    row.CreateCell(2);
                                    row.GetCell(2).CellStyle = textStyle;
                                    row.GetCell(2).SetCellValue("Jumlah");

                                    row.CreateCell(3);
                                    row.GetCell(3).CellStyle = textStyle;
                                    row.GetCell(3).SetCellValue("Bobot Nilai");

                                    row.CreateCell(4);
                                    row.GetCell(4).CellStyle = textStyle;
                                    row.GetCell(4).SetCellValue("Jumlah Bobot Nilai");
                                    break;
                                case 4:
                                    row.CreateCell(2);
                                    row.GetCell(2).CellStyle = textStyle;
                                    row.GetCell(2).SetCellValue("Ketersediaan");

                                    row.CreateCell(3);
                                    row.GetCell(3).CellStyle = textStyle;
                                    row.GetCell(3).SetCellValue("Bobot Ketersediaan");
                                    break;
                                default:
                                    break;
                            }
                            int _no = 0;
                            foreach (var itemPertanyaan in itemIndikator.Pertanyaans)
                            {
                                row = sheetDetil.CreateRow(++_row);
                                row.CreateCell(0);
                                row.GetCell(0).CellStyle = numberStyle;
                                row.GetCell(0).SetCellValue(++_no);

                                row.CreateCell(1);
                                row.GetCell(1).CellStyle = textStyle;
                                row.GetCell(1).SetCellValue(itemPertanyaan.Pertanyaan);
                                switch (itemIndikator.Type)
                                {
                                    case 1:
                                        row.CreateCell(2);
                                        row.GetCell(2).CellStyle = textStyle;
                                        if (!string.IsNullOrEmpty(itemPertanyaan.Ketersediaan))
                                            row.GetCell(2).SetCellValue(itemPertanyaan.Ketersediaan);

                                        row.CreateCell(3);
                                        row.GetCell(3).CellStyle = numberStyle;
                                        if (itemPertanyaan.BobotKetersediaan != null)
                                            row.GetCell(3).SetCellValue((double)itemPertanyaan.BobotKetersediaan);

                                        row.CreateCell(4);
                                        row.GetCell(4).CellStyle = textStyle;
                                        if (!string.IsNullOrEmpty(itemPertanyaan.Kecukupan))
                                            row.GetCell(4).SetCellValue(itemPertanyaan.Kecukupan);

                                        row.CreateCell(5);
                                        row.GetCell(5).CellStyle = numberStyle;
                                        if (itemPertanyaan.BobotKecukupan != null)
                                            row.GetCell(5).SetCellValue((double)itemPertanyaan.BobotKecukupan);

                                        row.CreateCell(6);
                                        row.GetCell(6).CellStyle = textStyle;
                                        if (!string.IsNullOrEmpty(itemPertanyaan.Kondisi))
                                            row.GetCell(6).SetCellValue(itemPertanyaan.Kondisi);

                                        row.CreateCell(7);
                                        row.GetCell(7).CellStyle = numberStyle;
                                        if (itemPertanyaan.BobotKondisi != null)
                                            row.GetCell(7).SetCellValue((double)itemPertanyaan.BobotKondisi);
                                        break;
                                    case 2:
                                        row.CreateCell(2);
                                        row.GetCell(2).CellStyle = numberStyle;
                                        if (itemPertanyaan.Kebutuhan != null)
                                            row.GetCell(2).SetCellValue((double)itemPertanyaan.Kebutuhan);

                                        row.CreateCell(3);
                                        row.GetCell(3).CellStyle = numberStyle;
                                        if (itemPertanyaan.Keterisian != null)
                                            row.GetCell(3).SetCellValue((double)itemPertanyaan.Keterisian);

                                        row.CreateCell(4);
                                        row.GetCell(4).CellStyle = numberStyle;
                                        if (itemPertanyaan.BobotkebKet != null)
                                            row.GetCell(4).SetCellValue((double)itemPertanyaan.BobotkebKet);
                                        break;
                                    case 3:
                                        row.CreateCell(2);
                                        row.GetCell(2).CellStyle = numberStyle;
                                        if (itemPertanyaan.Jumlah != null)
                                            row.GetCell(2).SetCellValue((double)itemPertanyaan.Jumlah);

                                        row.CreateCell(3);
                                        row.GetCell(3).CellStyle = numberStyle;
                                        if (itemPertanyaan.BobotNilai != null)
                                            row.GetCell(3).SetCellValue((double)itemPertanyaan.BobotNilai);

                                        row.CreateCell(4);
                                        row.GetCell(4).CellStyle = numberStyle;
                                        if (itemPertanyaan.JumlahBobotNilai != null)
                                            row.GetCell(4).SetCellValue((double)itemPertanyaan.JumlahBobotNilai);
                                        break;
                                    case 4:
                                        row.CreateCell(2);
                                        row.GetCell(2).CellStyle = textStyle;
                                        if (!string.IsNullOrEmpty(itemPertanyaan.Ketersediaan))
                                            row.GetCell(2).SetCellValue(itemPertanyaan.Ketersediaan);

                                        row.CreateCell(3);
                                        row.GetCell(3).CellStyle = numberStyle;
                                        if (itemPertanyaan.BobotKetersediaan != null)
                                            row.GetCell(3).SetCellValue((double)itemPertanyaan.BobotKetersediaan);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        sheetDetil.CreateRow(++_row).CreateCell(0);
                    }

                    ISheet sheetTemplatePetugas = xssfwb.GetSheet("petugas");
                    if (sheetTemplatePetugas == null)
                    {
                        OutFileName = pathTemplateFile;
                        return string.Format("sheet {0} tidak ada di file {1}", "petugas", "TemplateLaporan");
                    }
                    sheetTemplatePetugas.CopySheet(sheetPetugasName);
                    ISheet sheetPetugas = xssfwb.GetSheet(sheetPetugasName);
                    sheetPetugas.GetRow(0).GetCell(0).SetCellValue(string.Format("Data Diri Pengisi Monitoring dan Evaluasi"));
                    sheetPetugas.GetRow(1).GetCell(0).SetCellValue(string.Format("Periode Tahun {0}", Detil.Tahun.ToString()));
                    sheetPetugas.GetRow(2).GetCell(0).SetCellValue(string.Format("Provinsi: {0}, Kabupaten/Kota: {1}, Kecamatan: {0}", Detil.Provinsi, Detil.Kabupaten, Detil.Kecamatan));
                    _row = 3;
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.Nama);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.Jabatan);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.Email);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.NoHp);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.AlamatKantor);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.Kecamatan);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.Kabupaten);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.Provinsi);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.Rata2Ciq);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.JenisLokpri);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.Rata2Pap);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.PtgsNama);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.PtgsJabatan);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.PtgsEmail);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.PtgsNoHp);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.TanggalInput);
                    sheetPetugas.GetRow(++_row).GetCell(1).SetCellValue(Detil.JamInput);

                    xssfwb.RemoveAt(xssfwb.GetSheetIndex("rekap"));
                    xssfwb.RemoveAt(xssfwb.GetSheetIndex("daftar"));
                    xssfwb.RemoveAt(xssfwb.GetSheetIndex("detil"));
                    xssfwb.RemoveAt(xssfwb.GetSheetIndex("petugas"));
                    FileStream file = new FileStream(OutFileName, FileMode.Create);
                    xssfwb.Write(file);
                    xssfwb.Close();
                    file.Close();
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                OutFileName = pathTemplateFile;
                return GetErrorMessage(ServiceName + "GenerateLaporan", ex);
            }
        }
    }
}
