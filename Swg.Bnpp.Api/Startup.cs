using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Bnpp.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettingModel>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettingModel>();
            AppSetting.Secret = appSettings.Secret;
            AppSetting.ConnectionString = appSettings.ConnectionString;
            AppSetting.BaseUrl = appSettings.BaseUrl;
            AppSetting.PathFile = appSettings.PathFile;

            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<IUserAppService, UserAppService>();
            services.AddScoped<IMasterService, MasterService>();
            services.AddScoped<IMnvService, MnvService>();

            CommonService commonService = new CommonService();
            commonService.InitApplSetting();

           

            /**
             * add by safari eri
             * handle cross origin access from user interface or aother application
             */

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(
                                  builder =>
                                  {
                                      builder.WithOrigins("http://localhost:3000",
                                                          "http://monev.swg.co.id",
                                                          "http://103.143.170.117",
                                                          "http://103.143.170.115"
                                                          )
                                      .AllowAnyHeader()
                                      .AllowAnyMethod();
                                  });
            });

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Swg.Bnpp.Api", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            /**
            * config forward rules
            */
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Swg.Bnpp.Api v1"));
            }

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
