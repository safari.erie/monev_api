﻿using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.Master;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System.Collections.Generic;

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterVariabelsController : ControllerBase
    {
        private readonly IMasterService masterService;
        public MasterVariabelsController(IMasterService MasterService)
        {
            masterService = MasterService;
        }

        [HttpGet("GetVariabels", Name = "GetVariabelsWeb")]
        public ResponseModel<List<VariabelListModel>> GetVariabels()
        {
            var ret = masterService.GetVariabels(out string oMessage);
            return new ResponseModel<List<VariabelListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetVariabel", Name = "GetVariabelWeb")]
        public ResponseModel<VariabelModel> GetVariabel(int IdVariabel)
        {
            var ret = masterService.GetVariabel(IdVariabel, out string oMessage);
            return new ResponseModel<VariabelModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddVariabel", Name = "AddVariabelWeb")]
        public ResponseModel<string> AddVariabel(int IdUser, int IdJenisVariabel, string Nama, int Presentase)
        {
            var oMessage = masterService.AddVariabel(IdUser, IdJenisVariabel, Nama, Presentase);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditVariabel", Name = "EditVariabelWeb")]
        public ResponseModel<string> EditVariabel(int IdUser, VariabelModel Data)
        {
            var oMessage = masterService.EditVariabel(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteVariabel", Name = "DeleteVariabelWeb")]
        public ResponseModel<string> DeleteVariabel(int IdUser, int IdVariabel)
        {
            var oMessage = masterService.DeleteVariabel(IdUser, IdVariabel);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }

        [HttpGet("GetJenisVariabel", Name = "GetJenisVariabelWeb")]
        public ResponseModel<List<JenisVariabelModel>> GetJenisVariabels()
        {
            var ret = masterService.GetJenisVariabels(out string oMessage);
            return new ResponseModel<List<JenisVariabelModel>>
            {
                IsSuccess = (string.IsNullOrEmpty(oMessage)) ? true : false,
                ReturnMessage = oMessage,
                Data = ret
            };
        }
    }
}
