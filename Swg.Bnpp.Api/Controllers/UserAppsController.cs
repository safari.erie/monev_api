﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System.Collections.Generic;

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAppsController : ControllerBase
    {
        private readonly IUserAppService userAppService;
        public UserAppsController(IUserAppService UserAppService)
        {
            userAppService = UserAppService;
        }

        [HttpGet("Login", Name = "LoginWeb")]
        public ResponseModel<UserModel> Login(string UsernameOrEmail, string Password)
        {
            var ret = userAppService.Login(UsernameOrEmail, Password, 1, out string oMessage);
            return new ResponseModel<UserModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("UpdateProfile", Name = "UpdateProfileWeb")]
        public ResponseModel<string> UpdateProfile(int IdUser, string Email, string FirstName, string MiddleName, string LastName, string Address, string PhoneNumber, string MobileNumber, IFormFile FileImage)
        {
            var oMessage = userAppService.UpdateProfile(IdUser, Email, FirstName, MiddleName, LastName, Address, PhoneNumber, MobileNumber, FileImage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("ChangePassword", Name = "ChangePasswordWeb")]
        public ResponseModel<string> ChangePassword(string UsernamOrEmail, string OldPassword, string NewPassword1, string NewPassword2)
        {
            var oMessage = userAppService.ChangePassword(UsernamOrEmail, OldPassword, NewPassword1, NewPassword2);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("ResetPassword", Name = "ResetPasswordWeb")]
        public ResponseModel<string> ResetPassword(string UsernamOrEmail)
        {
            var ret = userAppService.ResetPassword(UsernamOrEmail, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }


        [HttpGet("GetUsers", Name = "GetUsersWeb")]
        public ResponseModel<List<UserModel>> GetUsers()
        {
            var ret = userAppService.GetUsers(out string oMessage);
            return new ResponseModel<List<UserModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetUser", Name = "GetUserWeb")]
        public ResponseModel<UserModel> GetUser(int IdUser)
        {
            var ret = userAppService.GetUser(IdUser, out string oMessage);
            return new ResponseModel<UserModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddUser", Name = "AddUserWeb")]
        public ResponseModel<string> AddUser(int IdUser, UserAddModel Data)
        {
            var oMessage = userAppService.AddUser(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditUser", Name = "EditUserWeb")]
        public ResponseModel<string> EditUser(int IdUser, UserModel Data)
        {
            var oMessage = userAppService.EditUser(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("SetUserActive", Name = "SetUserActiveWeb")]
        public ResponseModel<string> SetUserActive(int IdUser, int SetIdUser)
        {
            var ret = userAppService.SetUserActive(IdUser, SetIdUser, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("SetUserInActive", Name = "SetUserInActiveWeb")]
        public ResponseModel<string> SetUserInActive(int IdUser, int SetIdUser)
        {
            var ret = userAppService.SetUserInActive(IdUser, SetIdUser, out string oMessage);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetRoles", Name = "GetRolesWeb")]
        public ResponseModel<List<RoleModel>> GetRoles()
        {
            var ret = userAppService.GetRoles(out string oMessage);
            return new ResponseModel<List<RoleModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetRole", Name = "GetRoleWeb")]
        public ResponseModel<RoleModel> GetRole(int IdRole)
        {
            var ret = userAppService.GetRole(IdRole, out string oMessage);
            return new ResponseModel<RoleModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddRole", Name = "AddRoleWeb")]
        public ResponseModel<string> AddRole(int IdRole, string RoleName)
        {
            var oMessage = userAppService.AddRole(IdRole, RoleName);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteRole", Name = "DeleteRoleWeb")]
        public ResponseModel<string> DeleteRole(int IdRole)
        {
            var oMessage = userAppService.DeleteRole(IdRole);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }

        [HttpGet("GetApplSettings", Name = "GetApplSettingsWeb")]
        public ResponseModel<List<ApplSettingModel>> GetApplSettings()
        {
            var ret = userAppService.GetApplSettings(out string oMessage);
            return new ResponseModel<List<ApplSettingModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetApplSetting", Name = "GetApplSettingWeb")]
        public ResponseModel<ApplSettingModel> GetApplSetting(string Code)
        {
            var ret = userAppService.GetApplSetting(Code, out string oMessage);
            return new ResponseModel<ApplSettingModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddApplSetting", Name = "AddApplSettingWeb")]
        public ResponseModel<string> AddApplSetting(ApplSettingModel Data)
        {
            var oMessage = userAppService.AddApplSetting(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditApplSetting", Name = "EditApplSettingWeb")]
        public ResponseModel<string> EditApplSetting(ApplSettingModel Data)
        {
            var oMessage = userAppService.EditApplSetting(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteApplSetting", Name = "DeleteApplSettingWeb")]
        public ResponseModel<string> DeleteApplSetting(string Code)
        {
            var oMessage = userAppService.DeleteApplSetting(Code);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }

        [HttpGet("GetApplTasks", Name = "GetApplTasksWeb")]
        public ResponseModel<List<ApplTaskModel>> GetApplTasks(int IdAppl)
        {
            var ret = userAppService.GetApplTasks(IdAppl, out string oMessage);
            return new ResponseModel<List<ApplTaskModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetMenusByUsername", Name = "GetMenusByUsernameWeb")]
        public ResponseModel<List<MenuModel>> GetMenusByUsername(string Username)
        {
            var ret = userAppService.GetMenus(1, null, Username, out string oMessage);
            return new ResponseModel<List<MenuModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetMenusByIdUser", Name = "GetMenusByIdUserWeb")]
        public ResponseModel<List<MenuModel>> GetMenusByIdUsers(int IdUser)
        {
            var ret = userAppService.GetMenus(1, IdUser,null, out string oMessage);
            return new ResponseModel<List<MenuModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
    }
}
