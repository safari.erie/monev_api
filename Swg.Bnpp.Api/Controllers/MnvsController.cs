﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.Mnv;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MnvsController : ControllerBase
    {
        private readonly IMnvService mnvService;
        public MnvsController(IMnvService MnvService)
        {
            mnvService = MnvService;
        }
        [HttpGet("GetMonevs", Name = "GetMonevs")]
        public ResponseModel<List<MonevListModel>> GetMonevs(int IdUser, int Tahun)
        {
            var oMessage = mnvService.GetMonevs(IdUser, Tahun, out List<MonevListModel> oData);
            return new ResponseModel<List<MonevListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = oData
            };
        }
        [HttpGet("GetMonevSpasial", Name = "GetMonevSpasialWeb")]
        public ResponseModel<DashboardMonevxModel> GetMonevSpasial(int Tahun)
        {
            var oMessage = mnvService.GetMonevSpasial(Tahun, out DashboardMonevxModel oData);
            return new ResponseModel<DashboardMonevxModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = oData
            };
        }
        [HttpGet("GetMonevDashboard", Name = "GetMonevDashboard")]
        public ResponseModel<DashboardMonevModel> GetMonevDashboard(int Tahun)
        {
            var oMessage = mnvService.GetMonevDashboard(Tahun, out DashboardMonevModel oData);
            return new ResponseModel<DashboardMonevModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = oData
            };
        }
        [HttpGet("GetMonev", Name = "GetMonev")]
        public ResponseModel<MonevModel> GetMonev(int IdMonev)
        {
            MonevModel ret = new MonevModel();
            var oMessage = mnvService.GetMonev(IdMonev, out ret);
            return new ResponseModel<MonevModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddMonev", Name = "AddMonevWeb")]
        public ResponseModel<MonevModel> AddMonev(int IdUser, int IdPegawai, int IdJenisLokpri)
        {
            var oMessage = mnvService.AddMonev(IdUser, IdPegawai, IdJenisLokpri, out MonevModel ret);
            return new ResponseModel<MonevModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("SetGetJawaban", Name = "SetGetJawaban")]
        public ResponseModel<MonevIndikatorModel> SetGetJawaban(int IdUser, int IdMonev, int IdVariabel, int IdIndikator, MonevIndikatorModel InJawaban)
        {
            MonevIndikatorModel ret = new MonevIndikatorModel();
            var oMessage = mnvService.SetGetJawaban(IdUser, IdMonev, InJawaban, IdVariabel, IdIndikator, out ret);
            return new ResponseModel<MonevIndikatorModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("DeleteMonev", Name = "DeleteMonev")]
        public ResponseModel<string> DeleteMonev(int IdUser, int IdMonev)
        {
            var oMessage = mnvService.DeleteMonev(IdUser, IdMonev);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = string.Empty
            };
        }
        [HttpGet("DownloadLaporanRekapExcel", Name = "DownloadLaporanRekapExcel")]
        public ActionResult DownloadLaporanRekapExcel(int IdUser, int Tahun)
        {
            try
            {
                var oMessage = mnvService.GetlaporanRekap(IdUser, Tahun, out List<LaporanRekapModel> Rekaps, out List<LaporanDaftarModel> DetilSudahInput, out List<LaporanDaftarModel> DetilsBelumInput);
                Tools.ExcelTool excel = new Tools.ExcelTool();
                oMessage = excel.GenerateLaporanRekap(IdUser, Tahun, Rekaps, DetilSudahInput, DetilsBelumInput, out string filename);
                var stream = new FileStream(filename, FileMode.Open);
                string fileDownload = string.Format("{0}_{1}_{2}.xlsx", "LaporanRekap", Tahun.ToString(), IdUser.ToString());
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
        [HttpGet("DownloadLaporanDetilExcel", Name = "DownloadLaporanDetilExcel")]
        public ActionResult DownloadLaporanDetilExcel(int IdUser, int IdMonev)
        {
            try
            {
                var oMessage = mnvService.GetLaporanDetil(IdUser, IdMonev, out LaporanDetilMonevModel Detil);
                if (!string.IsNullOrEmpty(oMessage)) Console.WriteLine(oMessage);
                string filename = string.Empty;
                string fileDownload = "Gagal";
                if (string.IsNullOrEmpty(oMessage))
                {
                    Tools.ExcelTool excel = new Tools.ExcelTool();
                    oMessage = excel.GenerateLaporanDetil(IdUser, Detil, out filename);
                    if (string.IsNullOrEmpty(oMessage))
                        fileDownload = string.Format("{0}_{1}_{2}_{3}_{4}.xlsx", "LaporanDetil", Detil.Tahun.ToString(), IdUser.ToString(), Detil.Kecamatan.ToString(), Detil.JenisLokpri.ToString());
                }
                Console.WriteLine(oMessage);
                var stream = new FileStream(filename, FileMode.Open);
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileDownload);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { ErrorMessage = ex });
            }
        }
    }
}
