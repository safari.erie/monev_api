﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.Master;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    public class MasterIndikatorPertanyaansController : Controller
    {
        private readonly IMasterService masterService;
        public MasterIndikatorPertanyaansController(IMasterService MasterService)
        {
            masterService = MasterService;
        }

        [HttpGet("GetIndikatorPertanyaans", Name = "GetIndikatorPertanyaans")]
        public ResponseModel<List<IndikatorPertanyaanModel>> GetIndikatorPertanyaans()
        {
            var ret = masterService.GetIndikatorPertanyaans(out string oMessage);
            return new ResponseModel<List<IndikatorPertanyaanModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetIndikatorPertanyaanByIdIndikator", Name = "GetIndikatorPertanyaanByIdIndikator")]
        public ResponseModel<List<IndikatorPertanyaanModel>> GetIndikatorPertanyaanByIdIndikator(int IdIndikator)
        {
            var ret = masterService.GetIndikatorPertanyaanByIdIndikator(IdIndikator,out string oMessage);
            return new ResponseModel<List<IndikatorPertanyaanModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetIndikatorPertanyaan", Name = "GetIndikatorPertanyaan")]
        public ResponseModel<IndikatorPertanyaanModel> GetIndikatorPertanyaan(int IdIndikatorPertanyaan)
        {
            var ret = masterService.GetIndikatorPertanyaan(IdIndikatorPertanyaan, out string oMessage);
            return new ResponseModel<IndikatorPertanyaanModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpPost("AddIndikatorPertanyaan", Name = "AddIndikatorPertanyaan")]
        public ResponseModel<string> AddIndikatorPertanyaan(int IdUser, int IdIndikator, int IdJenisLokpri, int IdPertanyaan)
        {
            var oMessage = masterService.AddIndikatorPertanyaan(IdUser, IdIndikator, IdJenisLokpri, IdPertanyaan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("AddIndikatorPertanyaans", Name = "AddIndikatorPertanyaans")]
        public ResponseModel<string> AddIndikatorPertanyaans(int IdUser, int IdIndikator, int IdJenisLokpri, List<int> IdPertanyaans)
        {
            var oMessage = masterService.AddIndikatorPertanyaan(IdUser, IdIndikator, IdJenisLokpri, IdPertanyaans);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteIndikatorPertanyaan", Name = "DeleteIndikatorPertanyaan")]
        public ResponseModel<string> DeleteIndikatorPertanyaan(int IdUser, int IdIndikatorPertanyaan)
        {
            var oMessage = masterService.DeleteIndikatorPertanyaan(IdUser, IdIndikatorPertanyaan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
    }
}
