﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.Master;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterJenisLokprisController : ControllerBase
    {
        private readonly IMasterService masterService;
        public MasterJenisLokprisController(IMasterService MasterService)
        {
            masterService = MasterService;
        }

        [HttpGet("GetJenisLokpris", Name = "GetJenisLokprisWeb")]
        public ResponseModel<List<JenisLokpriModel>> GetJenisLokpris()
        {
            var ret = masterService.GetJenisLokpris(out string oMessage);
            return new ResponseModel<List<JenisLokpriModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
    }
}
