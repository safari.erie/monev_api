﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.Master;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterPertanyaansController : ControllerBase
    {
        private readonly IMasterService masterService;
        public MasterPertanyaansController(IMasterService MasterService)
        {
            masterService = MasterService;
        }

        [HttpGet("GetPertanyaans", Name = "GetPertanyaansWeb")]
        public ResponseModel<List<PertanyaanModel>> GetPertanyaans()
        {
            var ret = masterService.GetPertanyaans(out string oMessage);
            return new ResponseModel<List<PertanyaanModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }

        [HttpGet("GetPertanyaan", Name = "GetPertanyaanWeb")]
        public ResponseModel<PertanyaanModel> GetPertanyaan(int IdPertanyaan)
        {
            var ret = masterService.GetPertanyaan(IdPertanyaan, out string oMessage);
            return new ResponseModel<PertanyaanModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddPertanyaan", Name = "AddPertanyaanWeb")]
        public ResponseModel<string> AddPertanyaan(int IdUser, string Pertanyaan, double? BobotDefault)
        {
            var oMessage = masterService.AddPertanyaan(IdUser, Pertanyaan, BobotDefault);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditPertanyaan", Name = "EditPertanyaanWeb")]
        public ResponseModel<string> EditPertanyaan(int IdUser, PertanyaanModel Data)
        {
            var oMessage = masterService.EditPertanyaan(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeletePertanyaan", Name = "DeletePertanyaanWeb")]
        public ResponseModel<string> DeletePertanyaan(int IdUser, int IdPertanyaan)
        {
            var oMessage = masterService.DeletePertanyaan(IdUser, IdPertanyaan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
    }
}
