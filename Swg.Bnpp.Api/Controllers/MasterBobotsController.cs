﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.Master;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterBobotsController : ControllerBase
    {
        private readonly IMasterService masterService;
        public MasterBobotsController(IMasterService MasterService)
        {
            masterService = MasterService;
        }

        [HttpGet("GetBobotKecukupans", Name = "GetBobotKecukupansWeb")]
        public ResponseModel<List<BobotModel>> GetBobotKecukupans()
        {
            var ret = masterService.GetBobotKecukupans(out string oMessage);
            return new ResponseModel<List<BobotModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetBobotKecukupan", Name = "GetBobotKecukupanWeb")]
        public ResponseModel<BobotModel> GetBobotKecukupan(int Id)
        {
            var ret = masterService.GetBobotKecukupan(Id, out string oMessage);
            return new ResponseModel<BobotModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddBobotKecukupan", Name = "AddBobotKecukupanWeb")]
        public ResponseModel<string> AddBobotKecukupan(int IdUser, string Nama, double Bobot)
        {
            var oMessage = masterService.AddBobotKecukupan(IdUser, Nama, Bobot);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditBobotKecukupan", Name = "EditBobotKecukupanWeb")]
        public ResponseModel<string> EditBobotKecukupan(int IdUser, BobotModel Data)
        {
            var oMessage = masterService.EditBobotKecukupan(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteBobotKecukupan", Name = "DeleteBobotKecukupanWeb")]
        public ResponseModel<string> DeleteBobotKecukupan(int IdUser, int Id)
        {
            var oMessage = masterService.DeleteBobotKecukupan(IdUser, Id);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpGet("GetBobotKetersediaans", Name = "GetBobotKetersediaansWeb")]
        public ResponseModel<List<BobotModel>> GetBobotKetersediaans()
        {
            var ret = masterService.GetBobotKetersediaans(out string oMessage);
            return new ResponseModel<List<BobotModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetBobotKetersediaan", Name = "GetBobotKetersediaanWeb")]
        public ResponseModel<BobotModel> GetBobotKetersediaan(int Id)
        {
            var ret = masterService.GetBobotKetersediaan(Id, out string oMessage);
            return new ResponseModel<BobotModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddBobotKetersediaan", Name = "AddBobotKetersediaanWeb")]
        public ResponseModel<string> AddBobotKetersediaan(int IdUser, string Nama, double Bobot)
        {
            var oMessage = masterService.AddBobotKetersediaan(IdUser, Nama, Bobot);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditBobotKetersediaan", Name = "EditBobotKetersediaanWeb")]
        public ResponseModel<string> EditBobotKetersediaan(int IdUser, BobotModel Data)
        {
            var oMessage = masterService.EditBobotKetersediaan(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteBobotKetersediaan", Name = "DeleteBobotKetersediaanWeb")]
        public ResponseModel<string> DeleteBobotKetersediaan(int IdUser, int Id)
        {
            var oMessage = masterService.DeleteBobotKetersediaan(IdUser, Id);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpGet("GetBobotKondisis", Name = "GetBobotKondisisWeb")]
        public ResponseModel<List<BobotModel>> GetBobotKondisis()
        {
            var ret = masterService.GetBobotKondisis(out string oMessage);
            return new ResponseModel<List<BobotModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetBobotKondisi", Name = "GetBobotKondisiWeb")]
        public ResponseModel<BobotModel> GetBobotKondisi(int Id)
        {
            var ret = masterService.GetBobotKondisi(Id, out string oMessage);
            return new ResponseModel<BobotModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddBobotKondisi", Name = "AddBobotKondisiWeb")]
        public ResponseModel<string> AddBobotKondisi(int IdUser, string Nama, double Bobot)
        {
            var oMessage = masterService.AddBobotKondisi(IdUser, Nama, Bobot);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditBobotKondisi", Name = "EditBobotKondisiWeb")]
        public ResponseModel<string> EditBobotKondisi(int IdUser, BobotModel Data)
        {
            var oMessage = masterService.EditBobotKondisi(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteBobotKondisi", Name = "DeleteBobotKondisiWeb")]
        public ResponseModel<string> DeleteBobotKondisi(int IdUser, int Id)
        {
            var oMessage = masterService.DeleteBobotKondisi(IdUser, Id);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
    }
}
