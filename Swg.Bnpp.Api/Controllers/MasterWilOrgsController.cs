﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.Master;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterWilOrgsController : ControllerBase
    {
        private readonly IMasterService masterService;
        public MasterWilOrgsController(IMasterService MasterService)
        {
            masterService = MasterService;
        }
        #region wilayah

        [HttpGet("GetProvinsis", Name = "GetProvinsisWeb")]
        public ResponseModel<List<ProvinsiModel>> GetProvinsis()
        {
            var ret = masterService.GetProvinsis(out string oMessage);
            return new ResponseModel<List<ProvinsiModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetProvinsi", Name = "GetProvinsiWeb")]
        public ResponseModel<ProvinsiModel> GetProvinsi(int IdProvinsi)
        {
            var ret = masterService.GetProvinsi(IdProvinsi, out string oMessage);
            return new ResponseModel<ProvinsiModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddProvinsi", Name = "AddProvinsiWeb")]
        public ResponseModel<string> AddProvinsi(string Kode, string Nama)
        {
            var oMessage = masterService.AddProvinsi(Kode, Nama);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditProvinsi", Name = "EditProvinsiWeb")]
        public ResponseModel<string> EditProvinsi(ProvinsiModel Data)
        {
            var oMessage = masterService.EditProvinsi(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteProvinsi", Name = "DeleteProvinsiWeb")]
        public ResponseModel<string> DeleteProvinsi(int IdProvinsi)
        {
            var oMessage = masterService.DeleteProvinsi(IdProvinsi);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }

        [HttpGet("GetKabupatens", Name = "GetKabupatensWeb")]
        public ResponseModel<List<KabupatenModel>> GetKabupatens(int IdProvinsi)
        {
            var ret = masterService.GetKabupatens(IdProvinsi, out string oMessage);
            return new ResponseModel<List<KabupatenModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKabupaten", Name = "GetKabupatenWeb")]
        public ResponseModel<KabupatenModel> GetKabupaten(int IdKabupaten)
        {
            var ret = masterService.GetKabupaten(IdKabupaten, out string oMessage);
            return new ResponseModel<KabupatenModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddKabupaten", Name = "AddKabupatenWeb")]
        public ResponseModel<string> AddKabupaten(int IdProvinsi, string Kode, string Nama)
        {
            var oMessage = masterService.AddKabupaten(IdProvinsi, Kode, Nama);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditKabupaten", Name = "EditKabupatenWeb")]
        public ResponseModel<string> EditKabupaten(KabupatenModel Data)
        {
            var oMessage = masterService.EditKabupaten(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteKabupaten", Name = "DeleteKabupatenWeb")]
        public ResponseModel<string> DeleteKabupaten(int IdKabupaten)
        {
            var oMessage = masterService.DeleteKabupaten(IdKabupaten);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }

        [HttpGet("GetKecamatans", Name = "GetKecamatansWeb")]
        public ResponseModel<List<KecamatanEditModel>> GetKecamatans(int IdKabupaten)
        {
            var ret = masterService.GetKecamatans(IdKabupaten, out string oMessage);
            return new ResponseModel<List<KecamatanEditModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetKecamatan", Name = "GetKecamatanWeb")]
        public ResponseModel<KecamatanEditModel> GetKecamatan(int IdKecamatan)
        {
            var ret = masterService.GetKecamatan(IdKecamatan, out string oMessage);
            return new ResponseModel<KecamatanEditModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddKecamatan", Name = "AddKecamatanWeb")]
        public ResponseModel<string> AddKecamatan(int IdKabupaten, string Kode, string Nama, string Alamat, double? Longitude, double? Latitude)
        {
            var oMessage = masterService.AddKecamatan(IdKabupaten, Kode, Nama, Alamat, Longitude, Latitude);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditKecamatan", Name = "EditKecamatanWeb")]
        public ResponseModel<string> EditKecamatan(KecamatanEditModel Data)
        {
            var oMessage = masterService.EditKecamatan(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteKecamatan", Name = "DeleteKecamatanWeb")]
        public ResponseModel<string> DeleteKecamatan(int IdKecamatan)
        {
            var oMessage = masterService.DeleteKecamatan(IdKecamatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        #endregion
        #region jabatan
        [HttpGet("GetJabatans", Name = "GetJabatansWeb")]
        public ResponseModel<List<JabatanModel>> GetJabatans()
        {
            var ret = masterService.GetJabatans(out string oMessage);
            return new ResponseModel<List<JabatanModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetJabatan", Name = "GetJabatanWeb")]
        public ResponseModel<JabatanModel> GetJabatan(int IdJabatan)
        {
            var ret = masterService.GetJabatan(IdJabatan, out string oMessage);
            return new ResponseModel<JabatanModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddJabatan", Name = "AddJabatanWeb")]
        public ResponseModel<string> AddJabatan(string Kode, string Nama)
        {
            var oMessage = masterService.AddJabatan(Kode, Nama);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditJabatan", Name = "EditJabatanWeb")]
        public ResponseModel<string> EditJabatan(JabatanModel Data)
        {
            var oMessage = masterService.EditJabatan(Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteJabatan", Name = "DeleteJabatanWeb")]
        public ResponseModel<string> DeleteJabatan(int IdJabatan)
        {
            var oMessage = masterService.DeleteJabatan(IdJabatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        #endregion
        #region pegawai kecamatan
        [HttpGet("GetPegawaiKecamatans", Name = "GetPegawaiKecamatansWeb")]
        public ResponseModel<List<PegawaiListModel>> GetPegawaiKecamatans()
        {
            var ret = masterService.GetPegawaiKecamatans(out string oMessage);
            return new ResponseModel<List<PegawaiListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPegawaiKecamatansByKecamatan", Name = "GetPegawaiKecamatansByKecamatanWeb")]
        public ResponseModel<List<PegawaiListModel>> GetPegawaiKecamatansByKecamatan(int IdKecamatan)
        {
            var ret = masterService.GetPegawaiKecamatansByKecamatan(IdKecamatan, out string oMessage);
            return new ResponseModel<List<PegawaiListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPegawaiKecamatansByJabatan", Name = "GetPegawaiKecamatansByJabatanWeb")]
        public ResponseModel<List<PegawaiListModel>> GetPegawaiKecamatansByJabatan(int IdJabatan, int IdKecamatan)
        {
            var ret = masterService.GetPegawaiKecamatansByJabatan(IdJabatan,IdKecamatan, out string oMessage);
            return new ResponseModel<List<PegawaiListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetPegawaiKecamatan", Name = "GetPegawaiKecamatanWeb")]
        public ResponseModel<PegawaiJabatanModel> GetPegawaiKecamatan(int IdPegawai)
        {
            var ret = masterService.GetPegawaiKecamatan(IdPegawai, out string oMessage);
            return new ResponseModel<PegawaiJabatanModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddPegawaiKecamatan", Name = "AddPegawaiKecamatanWeb")]
        public ResponseModel<string> AddPegawaiKecamatan(PegawaiModel Data)
        {
            int IdUser = 1;
            List<RoleModel> Roles = new List<RoleModel>();
            Roles.Add(new RoleModel {
                IdRole = 3,
                RoleName = "Pegawai Kecamatan"
            });
            UserAddModel DataUser = new UserAddModel
            {
                Address = Data.Address,
                Email = Data.Email,
                FileImage = Data.FileImage,
                FirstName = Data.FirstName,
                LastName = Data.LastName,
                MiddleName = Data.MiddleName,
                MobileNumber = Data.MobileNumber,
                Password = Data.Password,
                PhoneNumber = Data.PhoneNumber,
                Roles = Roles,
                Username = Data.Username
            };
            var oMessage = masterService.AddPegawaiKecamatan(IdUser, Data, Data.IdJabatan, Data.IdKecamatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditPegawai", Name = "EditPegawaiWeb")]
        public ResponseModel<string> EditPegawai(PegawaiEditModel Data)
        {
            int IdUser = 1;
            UserModel DataUser = new UserModel
            {
                Address = Data.Address,
                Email = Data.Email,
                FileImage = Data.FileImage,
                FirstName = Data.FirstName,
                LastName = Data.LastName,
                MiddleName = Data.MiddleName,
                MobileNumber = Data.MobileNumber,
                Password = Data.Password,
                PhoneNumber = Data.PhoneNumber,
                Roles = Data.Roles,
                Username = Data.Username,
                IdUser = Data.IdPegawai
            };
            var oMessage = masterService.EditPegawaiKecamatan(IdUser, DataUser, Data.IdJabatan, Data.IdKecamatan);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        #endregion
    }
}
