﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swg.Bnpp.Models.Master;
using Swg.Bnpp.Models.UserApp;
using Swg.Bnpp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swg.Bnpp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterIndikatorsController : ControllerBase
    {
        private readonly IMasterService masterService;
        public MasterIndikatorsController(IMasterService MasterService)
        {
            masterService = MasterService;
        }

        [HttpGet("GetIndikators", Name = "GetIndikatorsWeb")]
        public ResponseModel<List<IndikatorListModel>> GetIndikators()
        {
            var ret = masterService.GetIndikators(out string oMessage);
            return new ResponseModel<List<IndikatorListModel>>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetIndikatorByIdVariabels", Name = "GetIndikatorByIdVariabelsWeb")]
        public ResponseModel<IndikatorVariabelModel> GetIndikatorByIdVariabels(int IdVariabel)
        {
            var ret = masterService.GetIndikatorByIdVariabels(IdVariabel, out string oMessage);
            return new ResponseModel<IndikatorVariabelModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpGet("GetIndikator", Name = "GetIndikatorWeb")]
        public ResponseModel<IndikatorModel> GetIndikator(int IdIndikator)
        {
            var ret = masterService.GetIndikator(IdIndikator, out string oMessage);
            return new ResponseModel<IndikatorModel>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = ret
            };
        }
        [HttpPost("AddIndikator", Name = "AddIndikatorWeb")]
        public ResponseModel<string> AddIndikator(int IdUser, int IdVariabel, string Nama, int Type)
        {
            var oMessage = masterService.AddIndikator(IdUser, IdVariabel, Nama, Type);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("EditIndikator", Name = "EditIndikatorWeb")]
        public ResponseModel<string> EditIndikator(int IdUser, IndikatorModel Data)
        {
            var oMessage = masterService.EditIndikator(IdUser, Data);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
        [HttpPost("DeleteIndikator", Name = "DeleteIndikatorWeb")]
        public ResponseModel<string> DeleteIndikator(int IdUser, int IdIndikator)
        {
            var oMessage = masterService.DeleteIndikator(IdUser, IdIndikator);
            return new ResponseModel<string>
            {
                IsSuccess = string.IsNullOrEmpty(oMessage),
                ReturnMessage = oMessage,
                Data = null
            };
        }
    }
}
