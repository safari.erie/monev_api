﻿using System;
using System.Collections.Generic;

namespace Swg.Bnpp.Models.Constants
{
    public class BobotKondisiConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>();
        public static int LayakAtas;
        public static int LayakBawah;
        public static int CukupLayakAtas;
        public static int CukupLayakBawah;
        public static int TidakLayakAtas;
        public static int TidakLayakBawah;
    }
}
