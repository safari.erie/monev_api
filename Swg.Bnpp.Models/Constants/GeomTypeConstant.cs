﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Constants
{
    public class GeomTypeConstant
    {
        public static Dictionary<int, string> DictGeomType = new Dictionary<int, string>()
        {
            {1,"Titik" },
            {2,"Garis" },
            {3,"Area" }
        };

        public const int Point = 1;
        public const int Line = 2;
        public const int Polygon = 3;
    }
}
