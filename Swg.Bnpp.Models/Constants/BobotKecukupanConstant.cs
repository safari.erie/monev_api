﻿using System;
using System.Collections.Generic;

namespace Swg.Bnpp.Models.Constants
{
    public class BobotKecukupanConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>();
        public static int Cukup;
        public static int LumayanCukup;
        public static int TidakCukup;
    }
}
