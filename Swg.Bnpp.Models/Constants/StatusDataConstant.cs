﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Constants
{
    public class StatusDataConstant
    {
        public static Dictionary<int, string> Dict = new Dictionary<int, string>()
        {
            {Aktif,"Aktif" },
            {TidakAktif,"Tidak Aktif" }
        };
        public const int Aktif = 1;
        public const int TidakAktif = -1;
    }
}
