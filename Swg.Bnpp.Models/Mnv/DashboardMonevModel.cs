﻿using Newtonsoft.Json;
using Swg.Bnpp.Models.Master;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Mnv
{
    public class DashboardMonevModel
    {
        public List<Graph1Model> GraphInput { get; set; } //Grafik Kecamatan Berdasarkan Input
        public List<Graph1Model> GraphProvBelumInput { get; set; } //Grafik Kecamatan Berdasarkan Prosestase CIQ
        public List<Graph1Model> GraphProvSudahInput { get; set; } //Grafik Kecamaran Berdasarkan Prosentase PAP
        public MonevSpasialTableModel DataTable { get; set; }
        public MonevSpasialMapModel Map { get; set; }

    }
    public class Graph1Model
    {
        public string Kategori { get; set; } //Sudah Input dan Belum Input
        public int Jumlah { get; set; }
    }

    public class DashboardMonevxModel
    {
        public Graph1xModel GraphInput { get; set; } //Grafik Kecamatan Berdasarkan Input
        public Graph2xModel GraphCiq { get; set; } //Grafik Kecamatan Berdasarkan Prosestase CIQ
        public Graph2xModel GraphPap { get; set; } //Grafik Kecamaran Berdasarkan Prosentase PAP
        public MonevSpasialTableModel DataTable { get; set; }
        public MonevSpasialMapModel Map { get; set; }

    }
    public class Graph1xModel
    {
        public int JmlInput { get; set; } //Sudah Input
        public int JmlBlmInput { get; set; } //Belum Input
    }
    public class Graph2xModel
    {
        public int Under25 { get; set; } //Dibawah 25%
        public int Between25_75 { get; set; } //Anatar 25% s/d 75%
        public int Up75 { get; set; }//Diatas 75%
    }
    public class MonevSpasialBaseModel
    {
        [Display(Name = "Provinsi", GroupName = "false")]
        public string Provinsi { get; set; }
        [Display(Name = "Kabupaten", GroupName = "false")]
        public string Kabupaten { get; set; }
        [Display(Name = "Kecamatan", GroupName = "false")]
        public string Kecamatan { get; set; }
        [Display(Name = "Nama Pegawai", GroupName = "false")]
        public string Pegawai { get; set; }
        [Display(Name = "Jabatan", GroupName = "false")]
        public string Jabatan { get; set; }
        [Display(Name = "Tanggal Input", GroupName = "false")]
        public string TanggalInput { get; set; }
        [Display(Name = "Rata-rata CIQ", GroupName = "false")]
        public double Rata2Ciq { get; set; }
        [Display(Name = "Rata-rata PAP", GroupName = "false")]
        public double Rata2Pap { get; set; }
        [Display(Name = "Longitude/Bujur", GroupName = "true")]
        public double Lon { get; set; }
        [Display(Name = "Latitude/Lintang", GroupName = "true")]
        public double Lat { get; set; }
        [Display(Name = "Sudah Input", GroupName = "false")]
        public int SudahInput { get; set; }
    }

    public class MonevSpasialTableListModel : MonevSpasialBaseModel
    {
        public MonevSpasialTableListModel(MonevSpasialBaseModel baseModel)
        {
            this.Provinsi = baseModel.Provinsi;
            this.Kabupaten = baseModel.Kabupaten;
            this.Kecamatan = baseModel.Kecamatan;
            this.Pegawai = baseModel.Pegawai;
            this.Jabatan = baseModel.Jabatan;
            this.TanggalInput = baseModel.TanggalInput;
            this.Rata2Ciq = baseModel.Rata2Ciq;
            this.Rata2Pap = baseModel.Rata2Pap;
            this.Lon = baseModel.Lon;
            this.Lat = baseModel.Lat;
        }
    }

    public class MonevSpasialTempModel : MonevSpasialBaseModel
    {
        public string GJson { get; set; }
    }


    public class MonevSpasialTableModel
    {
        public List<MonevSpasialTableListModel> ListData { get; set; }
        public List<TableColumnModel> ListColumn { get; set; }

    }
    public class MonevSpasialMapModel : SpasialModel
    {
        public MonevSpasialGJsonModel GJson { get; set; }
    }
    public class MonevSpasialPropertyModel : MonevSpasialBaseModel
    {
        public MonevSpasialPropertyModel(MonevSpasialBaseModel baseModel)
        {
            this.Provinsi = baseModel.Provinsi;
            this.Kabupaten = baseModel.Kabupaten;
            this.Kecamatan = baseModel.Kecamatan;
            this.Pegawai = baseModel.Pegawai;
            this.Jabatan = baseModel.Jabatan;
            this.TanggalInput = baseModel.TanggalInput;
            this.Rata2Ciq = baseModel.Rata2Ciq;
            this.Rata2Pap = baseModel.Rata2Pap;
            this.Lon = baseModel.Lon;
            this.Lat = baseModel.Lat;
            this.Lon = baseModel.Lon;
            this.Lat = baseModel.Lat;
        }
        [JsonProperty(PropertyName = "marker_color")]
        public string marker_color { get; set; }
        [JsonProperty(PropertyName = "marker_size")]
        public string marker_size { get; set; }
        [JsonProperty(PropertyName = "marker_symbol")]
        public string marker_symbol { get; set; }
    }

    public class MonevSpasialFeatureModel : PointFeaturesModel
    {
        public MonevSpasialPropertyModel properties { get; set; }
    }

    public class MonevSpasialGJsonModel
    {
        public string type { get; set; }
        public List<MonevSpasialFeatureModel> features { get; set; }
    }
}
