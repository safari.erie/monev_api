﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Mnv
{
    public class LaporanDaftarModel
    {
        public int No { get; set; }
        public string NamaPejabat { get; set; }
        public string Jabatan { get; set; }
        public string Email { get; set; }
        public string NoHp { get; set; }
        public string AlamatKantor { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
        public double Pap { get; set; }
        public double Ciq { get; set; }
        public string JenisLokpri { get; set; }
        public string PetugasInput { get; set; }
        public string TanggalInput { get; set; }
        public string JamInput { get; set; }
    }
    public class LaporanRekapModel
    {
        public int No { get; set; }
        public string Provinsi { get; set; }
        public int SudahInput { get; set; }
        public int BelumInput { get; set; }
    }

    public class LaporanDetilMonevModel
    {
        public int Tahun { get; set; }
        public string Nama { get; set; }
        public string Jabatan { get; set; }
        public string Email { get; set; }
        public string NoHp { get; set; }
        public string AlamatKantor { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
        public double Rata2Ciq { get; set; }
        public double Rata2Pap { get; set; }
        public string JenisLokpri { get; set; }
        public string TanggalInput { get; set; }
        public string JamInput { get; set; }
        public string PtgsNama { get; set; }
        public string PtgsJabatan { get; set; }
        public string PtgsEmail { get; set; }
        public string PtgsNoHp { get; set; }
        public MonevModel Monev { get; set; }
    }
}
