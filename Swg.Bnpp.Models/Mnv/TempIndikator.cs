﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Mnv
{
    public class TempIndikatorx
    {
        public int IdMonevDetil { get; set; }
        public double _count { get; set; }
        public double _sum { get; set; }
    }
}
