﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Mnv
{
    public class TableColumnModel
    {
        public string FieldName { get; set; }
        public string FieldAlias { get; set; }
        public bool FieldDisplay { get; set; }
    }
}
