﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Mnv
{  
    public class MonevListModel
    {
        public int IdMovev { get; set; }
        public string Provinsi { get; set; }
        public string Kabupaten { get; set; }
        public string Kecamatan { get; set; }
        public string Pegawai { get; set; }
        public string Jabatan { get; set; }
        public string JenisLokpri { get; set; }
        public double RataRataCiq { get; set; }
        public double RataRataPap { get; set; }
        public string TanggalInput { get; set; }
    }

    public class MonevModel
    {
        public bool IsEdit { get; set; }
        public int IdMonev { get; set; }
        public int IdJenisLokpri { get; set; }
        public string JenisLokpri { get; set; }
        public double RataRataCiq { get; set; }
        public double RataRataPap { get; set; }
        public List<MonevVariabelModel> Variabels { get; set; }
    }
    public class MonevVariabelModel
    {
        public int IdVariabel { get; set; }
        public string Variabel { get; set; }
        public int IdJenisVariabel { get; set; }
        public string JenisVariabel { get; set; }
        public double Pct { get; set; }
        public double TotalRata2 { get; set; }
        public List<MonevIndikatorModel> Indikators { get; set; }
    }
    public class MonevIndikatorModel
    {
        public int IdIndikator { get; set; }
        public string Indikator { get; set; }
        public int Type { get; set; }
        public List<MonevPertanyaanJawabanModel> Pertanyaans { get; set; }
    }
    public class MonevPertanyaanJawabanModel : MonevJawabanModel
    {
        public int IdPertanyaan { get; set; }
        public string Pertanyaan { get; set; }
    }
    public class MonevJawabanModel
    {
        public int IdIndikatorPertanyaan { get; set; }
        public int IdJawaban { get; set; }
        public double? BobotDefault { get; set; }
        public string Ketersediaan { get; set; }
        public double? BobotKetersediaan { get; set; }
        public string Kecukupan { get; set; }
        public double? BobotKecukupan { get; set; }
        public string Kondisi { get; set; }
        public double? BobotKondisi { get; set; }
        public double? Kebutuhan { get; set; }
        public double? Keterisian { get; set; }
        public double? BobotkebKet { get; set; }
        public double? Jumlah { get; set; }
        public double? BobotNilai { get; set; }
        public double? JumlahBobotNilai { get; set; }
    }
    public class MonevPertanyaanJawabanAddModel : MonevJawabanModel
    {
        public int IdIndikator { get; set; }
        public int IdPertanyaan { get; set; }
        public int Type { get; set; }
    }
}
