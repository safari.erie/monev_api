﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Master
{
    public class JabatanModel
    {
        public int IdJabatan { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
    }
}
