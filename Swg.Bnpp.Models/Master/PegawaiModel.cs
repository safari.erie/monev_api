﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swg.Bnpp.Models.UserApp;

namespace Swg.Bnpp.Models.Master
{
    public class PegawaiListModel
    {
        public int IdPegawai { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int IdJabatan { get; set; }
        public string Jabatan { get; set; }
        public string AlamatKantor { get; set; }
        public string MobileNumber { get; set; }
        public string Kecamatan { get; set; }
        public string Kabupaten { get; set; }
        public string Provinsi { get; set; }
    }
    public class PegawaiModel : UserAddModel
    {
        public int IdJabatan { get; set; }
        public int IdKecamatan { get; set; }
    }

    public class PegawaiJabatanModel : UserAddModel
    {
        public int IdUser { get; set; }
        public string FullName { get; set; }
        public int IdJabatan { get; set; }
        public int IdKecamatan { get; set; }
        public int IdKabupaten { get; set; }
        public int IdProvinsi { get; set; }
        public string OfficeAddress { get; set; }
    }
    public class PegawaiEditModel : PegawaiModel
    {
        public int IdPegawai { get; set; }

    }
}
