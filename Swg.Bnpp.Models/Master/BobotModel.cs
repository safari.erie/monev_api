﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Master
{
    public class BobotModel
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public double Bobot { get; set; }
    }
}
