using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Master
{
    public class ProvinsiModel
    {
        public int IdProvinsi { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
    }
    public class KabupatenModel
    {
        public int IdKabupaten { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public int IdProvinsi { get; set; }
    }

    public class KecamatanAddModel
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
        public int IdKabupaten { get; set; }
        public string Alamat { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
    }
    public class KecamatanEditModel : KecamatanAddModel
    {
        public int IdKecamatan { get; set; }
    }
}
