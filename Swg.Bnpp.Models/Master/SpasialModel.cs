﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Master
{
    public class SpasialModel
    {
        public PointModel CenterPoint { get; set; }
        public string LayerName { get; set; }
        public int ZoomLevel { get; set; }
    }
    public class PointModel
    {
        [Display(Name = "Longitude/Bujur", GroupName = "true")]
        public double Lon { get; set; }
        [Display(Name = "Latitude/Lintang", GroupName = "true")]
        public double Lat { get; set; }
    }
    public class PolLineGeometryModel
    {
        public string type { get; set; }
        public List<double[,]> coordinates { get; set; }
    }
    public class LineGeometryModel
    {
        public string type { get; set; }
        public double[,] coordinates { get; set; }
    }
    public class PointGeometryModel
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }
    public class PolLineFeaturesModel
    {
        public string type { get; set; }
        public PolLineGeometryModel geometry { get; set; }
    }
    public class LineFeaturesModel
    {
        public string type { get; set; }
        public LineGeometryModel geometry { get; set; }
    }
    public class PointFeaturesModel
    {
        public string type { get; set; }
        public PointGeometryModel geometry { get; set; }
    }

    public class LineColorPropertyModel
    {
        [JsonProperty(PropertyName = "stroke")]
        public string stroke { get; set; }
        [JsonProperty(PropertyName = "stroke_width")]
        public string stroke_width { get; set; }
        [JsonProperty(PropertyName = "stroke_opacity")]
        public string stroke_opacity { get; set; }
    }
    public class PolygonColorPropertyModel : LineColorPropertyModel
    {
        [JsonProperty(PropertyName = "fill")]
        public string fill { get; set; }
        [JsonProperty(PropertyName = "fill_opacity")]
        public string fill_opacity { get; set; }
    }
}
