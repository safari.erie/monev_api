﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Master
{
    public class IndikatorPertanyaanModel 
    {
        public int IdIndikatorPertanyaan { get; set; }
        public int IdIndikator { get; set; }
        public string Indikator { get; set; }
        public int IdJenisLokpri { get; set; }
        public string JenisLokpri { get; set; }
        public int IdPertanyaan { get; set; }
        public string Pertanyaan { get; set; }
        public double? BobotDefault { get; set; }
    }
}
