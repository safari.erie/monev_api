﻿using System;
namespace Swg.Bnpp.Models.Master
{
    public class PertanyaanModel
    {
        public int IdPertanyaan { get; set; }
        public string Pertanyaan { get; set; }
        public double? BobotDefault { get; set; }
    }
}
