﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Master
{
    public class IndikatorListModel
    {
        public int IdIndikator { get; set; }
        public string Variabel { get; set; }
        public int Type { get; set; }
        public string Nama { get; set; }
    }
    public class IndikatorModel : IndikatorListModel
    {
        public int IdVariabel { get; set; }
    }

    public class IndikatorVariabelModel
    {
        public string Variabel { get; set; }
        public List<IndikatorListModel> Indikators { get; set; }
    }
}
