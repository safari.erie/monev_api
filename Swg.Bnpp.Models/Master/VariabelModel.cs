﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.Master
{
    public class VariabelListModel
    {
        public int IdVariabel { get; set; }
        public string JenisVariabel { get; set; }
        public string Nama { get; set; }
        public int Presentase { get; set; }
    }
    public class VariabelModel : VariabelListModel
    {
        public int IdJenisVariabel { get; set; }
    }

    public class JenisVariabelModel
    {
        public int IdJenisVariabel { get; set; }
        public string  Nama { get; set; }
    }
}
