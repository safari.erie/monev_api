﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.UserApp
{
    public class ApplSettingModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int ValueType { get; set; }
        public string ValueDate { get; set; }
        public double ValueNumber { get; set; }
        public string ValueString { get; set; }

        public IFormFile FileUpload { get; set; }
    }
}
