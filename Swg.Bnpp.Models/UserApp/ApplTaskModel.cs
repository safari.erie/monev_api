﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.UserApp
{
    public class ApplTaskModel : ApplTaskAddModel
    {
        public int IdApplTask { get; set; }
        public string ApplName { get; set; }
    }
    public class ApplTaskAddModel
    {
        public int? IdApplTaskParent { get; set; }
        public string ApplTaskName { get; set; }
        public int IdAppl { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Description { get; set; }
        public string IconName { get; set; }
    }
    public class ApplTaskEditModel
    {
        public int IdApplTask { get; set; }
        public string ApplTaskName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Description { get; set; }
        public string IconName { get; set; }
    }
    public class ApplTaskRoleModel
    {
        public int IdRole { get; set; }
        public string RoleName { get; set; }
        public List<ApplTaskModel> AssignTasks { get; set; }
        public List<ApplTaskModel> NoAssignTasks { get; set; }
    }
}
