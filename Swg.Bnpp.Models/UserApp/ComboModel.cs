﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.UserApp
{
    public class ComboModel
    {
        public System.Int64 Id { get; set; }
        public string Kode { get; set; }
        public string Nama { get; set; }
        public string NamaSingkat { get; set; }
    }
}
