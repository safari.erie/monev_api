﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.UserApp
{
    public class RoleModel
    {
        public int IdRole { get; set; }
        public string RoleName { get; set; }
    }
}
