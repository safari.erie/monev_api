﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.UserApp
{
    public class AppSettingModel
    {
        public string Secret { get; set; }
        public string ConnectionString { get; set; }
        public string PathFile { get; set; }
        public string BaseUrl { get; set; }

    }
    public static class AppSetting
    {
        public static string Secret { get; set; }
        public static string ConnectionString { get; set; }
        public static string PathFile { get; set; }
        public static string BaseUrl { get; set; }
    }

}
