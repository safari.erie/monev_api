﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Models.UserApp
{
    public class MenuModel
    {
        public string _tag { get; set; }
        public string name { get; set; }
        public string to { get; set; }
        public string route { get; set; }
        public string icon { get; set; }
        public List<MenuChildModel> _children { get; set; }
    }

    public class MenuChildModel
    {
        public string _tag { get; set; }
        public string name { get; set; }
        public string to { get; set; }
        public List<MenuChildModel> _children { get; set; }
    }
}
