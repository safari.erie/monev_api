﻿using Dapper;
using Newtonsoft.Json;
using Swg.Bnpp.Entities.Mnv;
using Swg.Bnpp.Entities.Org;
using Swg.Bnpp.Entities.Public;
using Swg.Bnpp.Entities.Wil;
using Swg.Bnpp.Models.Constants;
using Swg.Bnpp.Models.Master;
using Swg.Bnpp.Models.Mnv;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Swg.Bnpp.Services
{
    public interface IMnvService
    {
        string GetMonevs(int IdUser, int Tahun, out List<MonevListModel> oData);
        string GetMonevSpasial(int Tahun, out DashboardMonevxModel oData);
        string GetMonevDashboard(int Tahun, out DashboardMonevModel oData);
        string GetMonev(int IdMonev, out MonevModel oData);
        string AddMonev(int IdUser, int IdPegawai, int IdJenisLokpri, out MonevModel oJawaban);
        string DeleteMonev(int IdUser, int IdMonev);
        string GetlaporanRekap(int IdUser, int Tahun, out List<LaporanRekapModel> Rekaps, out List<LaporanDaftarModel> DetilSudahInputs, out List<LaporanDaftarModel> DetilBelumInputs);
        string GetLaporanDetil(int IdUser, int IdMonev, out LaporanDetilMonevModel oData);

        string SetGetJawaban(int IdUser, int IdMonev, MonevIndikatorModel InJawaban, int IdVariabel, int IdIndikator, out MonevIndikatorModel OutJawaban);

    }
    public class MnvService : IMnvService
    {
        private readonly string ServiceName = "MnvService.";
        private readonly ICommonService commonService;
        public MnvService(ICommonService CommonService)
        {
            commonService = CommonService;
        }
        private List<TableColumnModel> SetTableColumn(Type type)
        {
            List<TableColumnModel> tempData = new List<TableColumnModel>();
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            for (int i = 0; i < properties.Length; i++)
            {
                string FieldName = properties[i].Name;
                var dd = properties[i].GetCustomAttribute(typeof(DisplayAttribute)) as DisplayAttribute;
                string FieldAlias = FieldName;
                bool FieldDisplay = true;
                if (dd != null)
                {
                    FieldAlias = dd.Name;
                    FieldDisplay = dd.GroupName == "true";
                }
                tempData.Add(new TableColumnModel { FieldName = FieldName, FieldAlias = FieldAlias, FieldDisplay = FieldDisplay });
            }
            List<TableColumnModel> ret = new List<TableColumnModel>();
            TableColumnModel actionModel = (from a in tempData
                                            where a.FieldName == "ActionScript"
                                            select a).FirstOrDefault();
            if (actionModel != null)
            {
                ret.Add(actionModel);

                foreach (var item in tempData.Where(x => x.FieldName != actionModel.FieldName))
                {
                    ret.Add(item);
                }
            }
            else
            {
                ret = tempData;
            }
            return ret;
        }
        private string SetActionScript(double Lon, double Lat, int GeomType, string FieldName)
        {
            int geomType = GeomType;
            if (geomType == 0) geomType = GeomTypeConstant.Point;
            switch (GeomType)
            {
                case GeomTypeConstant.Point:
                case GeomTypeConstant.Line:
                case GeomTypeConstant.Polygon:
                    break;
                default:
                    geomType = GeomTypeConstant.Point;
                    break;
            }
            string ret = string.Format("'<a className= 'btn btn-outline-info' href ='javascript:void(0)' onClick ='zoomTo({0},{1},map,{2})'> {3} </a>'", Lon.ToString().Replace(",", "."), Lat.ToString().Replace(",", "."), geomType, FieldName);
            return ret;
        }
        private string SetActionScript1(double Lon, double Lat, int GeomType, string FieldName)
        {
            int geomType = GeomType;
            if (geomType == 0) geomType = GeomTypeConstant.Point;
            switch (GeomType)
            {
                case GeomTypeConstant.Point:
                case GeomTypeConstant.Line:
                case GeomTypeConstant.Polygon:
                    break;
                default:
                    geomType = GeomTypeConstant.Point;
                    break;
            }
            string ret = string.Format("<a className= 'btn btn-outline-info' href ='javascript:void(0)' onClick ='zoomTo({0},{1},map,{2})'> {3} </a>", Lon.ToString().Replace(",", "."), Lat.ToString().Replace(",", "."), geomType, FieldName);
            return ret;
        }
        private string GetMonev(System.Data.IDbConnection conn, int IdMonev, out MonevModel oData)
        {
            oData = new MonevModel
            {
                IdMonev = IdMonev,                 
                Variabels = new List<MonevVariabelModel>()
            };
            try
            {
                var data = from monev in conn.GetList<TbMonev>()
                           join jenisLokpri in conn.GetList<TbJenisLokpri>() on monev.IdJenisLokpri equals jenisLokpri.IdJenisLokpri
                           join monevDetil in conn.GetList<TbMonevDetil>() on monev.IdMonev equals monevDetil.IdMonev
                           join jawaban in conn.GetList<TbJawaban>() on monevDetil.IdMonevDetil equals jawaban.IdMonevDetil
                           join indikatorPertanyaan in conn.GetList<TbIndikatorPertanyaan>() on jawaban.IdIndikatorPertanyaan equals indikatorPertanyaan.IdIndikatorPertanyaan
                           join pertanyaan in conn.GetList<TbPertanyaan>() on indikatorPertanyaan.IdPertanyaan equals pertanyaan.IdPertanyaan
                           join indikator in conn.GetList<TbIndikator>() on indikatorPertanyaan.IdIndikator equals indikator.IdIndikator
                           join variabel in conn.GetList<TbVariabel>() on indikator.IdVariabel equals variabel.IdVariabel
                           join jenisVariabel in conn.GetList<TbJenisVariabel>() on variabel.IdJenisVariabel equals jenisVariabel.IdJenisVariabel
                           where monev.IdMonev == IdMonev
                           select new { monev, jenisLokpri, monevDetil, jawaban, pertanyaan, indikator, variabel, jenisVariabel };
                if (data == null || data.Count() == 0) return "data tidak ada";
                oData.RataRataCiq = data.ToList()[0].monev.RataRataCiq;
                oData.RataRataPap = data.ToList()[0].monev.RataRataPap;
                oData.IdJenisLokpri = data.ToList()[0].jenisLokpri.IdJenisLokpri;
                oData.JenisLokpri = data.ToList()[0].jenisLokpri.Nama;
                var dataVariabel = data
                    .GroupBy(x => x.variabel)
                    .Select(g => g.First())
                    .ToList()
                    .OrderBy(x => x.variabel.IdVariabel);
                foreach (var itemVariabel in dataVariabel)
                {
                    MonevVariabelModel monevVariabelModel = new MonevVariabelModel
                    {
                        IdJenisVariabel = itemVariabel.jenisVariabel.IdJenisVariabel,
                        IdVariabel = itemVariabel.variabel.IdVariabel,
                        JenisVariabel = itemVariabel.jenisVariabel.Nama,
                        Variabel = itemVariabel.variabel.Nama,
                        TotalRata2 = (from a in data
                                      where a.variabel == itemVariabel.variabel
                                      select a.monevDetil.Rata2).Sum(),
                        Indikators = new List<MonevIndikatorModel>()
                    };
                    var dataIndikator = data.Where(x => x.variabel == itemVariabel.variabel)
                    .GroupBy(x => x.indikator)
                    .Select(g => g.First())
                    .ToList()
                    .OrderBy(x => x.indikator.IdIndikator);
                    foreach (var itemIndikator in dataIndikator)
                    {
                        MonevIndikatorModel monevIndikatorModel = new MonevIndikatorModel
                        {
                            IdIndikator = itemIndikator.indikator.IdIndikator,
                            Indikator = itemIndikator.indikator.Nama,
                            Type = itemIndikator.indikator.Type,
                            Pertanyaans = new List<MonevPertanyaanJawabanModel>()
                        };
                        var dataPertanyaan = data.Where(x => x.variabel == itemVariabel.variabel );
                        dataPertanyaan = dataPertanyaan.Where(x => x.indikator == itemIndikator.indikator);
                        foreach (var itemPertanyaan in dataPertanyaan)
                        {
                            MonevPertanyaanJawabanModel monevPertanyaanJawabanModel = new MonevPertanyaanJawabanModel
                            {
                                IdIndikatorPertanyaan = itemPertanyaan.jawaban.IdIndikatorPertanyaan,
                                IdPertanyaan = itemPertanyaan.pertanyaan.IdPertanyaan,
                                Pertanyaan = itemPertanyaan.pertanyaan.Pertanyaan,
                                IdJawaban = itemPertanyaan.jawaban.IdJawaban,
                                BobotDefault = itemPertanyaan.pertanyaan.BobotDefault,
                                BobotkebKet = itemPertanyaan.jawaban.BobotKebKet,
                                BobotKecukupan = itemPertanyaan.jawaban.BobotKecukupan,
                                BobotKetersediaan = itemPertanyaan.jawaban.BobotKetersediaan,
                                BobotKondisi = itemPertanyaan.jawaban.BobotKondisi,
                                BobotNilai = itemPertanyaan.jawaban.BobotNilai,
                                Jumlah = itemPertanyaan.jawaban.Jumlah,
                                JumlahBobotNilai = itemPertanyaan.jawaban.JumlahBobotNilai,
                                Kebutuhan = itemPertanyaan.jawaban.Kebutuhan,
                                Kecukupan = itemPertanyaan.jawaban.Kecukupan,
                                Keterisian = itemPertanyaan.jawaban.Keterisian,
                                Ketersediaan = itemPertanyaan.jawaban.Ketersediaan,
                                Kondisi = itemPertanyaan.jawaban.Kondisi
                            };
                            monevIndikatorModel.Pertanyaans.Add(monevPertanyaanJawabanModel);
                        }
                        monevVariabelModel.Indikators.Add(monevIndikatorModel);
                    }
                    monevVariabelModel.Pct = 0;
                    oData.Variabels.Add(monevVariabelModel);
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GetMonev", ex);
            }
        }

        public string GetMonevs(int IdUser, int Tahun, out List<MonevListModel> oData)
        {
            oData = new List<MonevListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var pegawai = (from a in conn.GetList<TbPegawaiKecamatan>()
                                   where a.IdPegawai == IdUser
                                   select a).FirstOrDefault();

                    var data = from a in conn.GetList<TbMonev>()
                               join b in conn.GetList<TbPegawaiKecamatan>() on a.IdPegawai equals b.IdPegawai
                               join c in conn.GetList<TbUser>() on b.IdPegawai equals c.IdUser
                               join d in conn.GetList<TbJabatan>() on b.IdJabatan equals d.IdJabatan
                               join e in conn.GetList<TbUser>() on a.CreatedBy equals e.IdUser
                               join f in conn.GetList<TbKecamatan>() on b.IdKecamatan equals f.IdKecamatan
                               join g in conn.GetList<TbKabupaten>() on f.IdKabupaten equals g.IdKabupaten
                               join h in conn.GetList<TbProvinsi>() on g.IdProvinsi equals h.IdProvinsi
                               where a.CreatedDate.Year == Tahun
                               & b.IdKecamatan == (pegawai == null ? b.IdKecamatan : pegawai.IdKecamatan)
                               select new { a, b, c, d, e, Kecamatan = f.Nama, Kabupaten = g.Nama, Provinsi = h.Nama };
                    if (data == null || data.Count() == 0)
                        return "data tidak ada";
                    foreach (var item in data
                    )
                    {
                        MonevListModel m = new MonevListModel();
                        m.IdMovev = item.a.IdMonev;
                        m.Provinsi = item.Provinsi;
                        m.Kabupaten = item.Kabupaten;
                        m.Kecamatan = item.Kecamatan;
                        m.Pegawai = item.e.FirstName;
                        if (!string.IsNullOrEmpty(item.e.MiddleName))
                            m.Pegawai += " " + item.e.MiddleName;
                        if (!string.IsNullOrEmpty(item.e.LastName))
                            m.Pegawai += " " + item.e.LastName;
                        m.Jabatan = item.d.Nama;
                        m.JenisLokpri = "";
                        m.RataRataCiq = item.a.RataRataCiq;
                        m.RataRataPap = item.a.RataRataPap;
                        m.TanggalInput = item.a.CreatedDate.ToString("dd-MM-yyyy");
                        oData.Add(m);
                    }
                    return string.Empty;
                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GetMonevs", ex);
            }
        }
        public string GetMonevSpasial(int Tahun, out DashboardMonevxModel oData)
        {
            Type type = typeof(MonevSpasialTableListModel);

            oData = new DashboardMonevxModel
            {
                GraphInput = new Graph1xModel { JmlInput = 0, JmlBlmInput = 0 },
                GraphCiq = new Graph2xModel { Under25 = 0, Between25_75 = 0, Up75 = 0 },
                GraphPap = new Graph2xModel { Under25 = 0, Between25_75 = 0, Up75 = 0 },
                DataTable = new MonevSpasialTableModel
                {
                    ListColumn = SetTableColumn(type),
                    ListData = new List<MonevSpasialTableListModel>()
                },
                Map = new MonevSpasialMapModel
                {
                    LayerName = "Sebaran Monev Kecamatan",
                    ZoomLevel = 11,
                    CenterPoint = new PointModel
                    {
                        Lon = -6.1831142,
                        Lat = 106.8301226
                    },
                    GJson = new MonevSpasialGJsonModel
                    {
                        features = new List<MonevSpasialFeatureModel>(),
                        type = "FeatureCollection"
                    }
                }
            };
            try
            {
                string sql = "";
                sql += " select ";
                sql += " 	prov.nama as Prov,";
                sql += " 	kab.nama as Kabupaten,";
                sql += " 	kec.nama as Kecamatan,";
                sql += " 	trim(mon.first_name||' '||coalesce(mon.middle_name, '')||' '||coalesce(mon.last_name, '')) as Pegawai,";
                sql += " 	mon.jabatan,";
                sql += " 	(case when mon.created_date is null then '' else to_char(mon.created_date,'dd-mm-yyyy') end) as TanggalInput,";
                sql += " 	mon.rata_rata_ciq as Rata2Ciq,";
                sql += " 	mon.rata_rata_pap as Rata2Pap,";
                sql += " 	kecf.longitude as Lon,";
                sql += " 	kecf.latitude as Lat,";
                sql += " 	st_asgeojson(kecf.geom) AS gjson";
                sql += " from wil.tb_kecamatan kec";
                sql += " join org.tb_kecamatan_profil kecf on kec.id_kecamatan = kecf.id_kecamatan";
                sql += " join wil.tb_kabupaten kab on kec.id_kabupaten = kab.id_kabupaten";
                sql += " join wil.tb_provinsi prov on kab.id_provinsi = prov.id_provinsi";
                sql += " left join (";
                sql += " 	select ";
                sql += " 		b.id_kecamatan, d.first_name, d.middle_name, d.last_name,";
                sql += " 	    c.nama as jabatan, a.created_date, a.rata_rata_ciq, a.rata_rata_pap";
                sql += " 	from mnv.tb_monev a";
                sql += " 	join org.tb_pegawai_kecamatan b on a.id_pegawai = b.id_pegawai";
                sql += " 	join org.tb_jabatan c on b.id_jabatan = c.id_jabatan";
                sql += " 	join public.tb_user d on b.id_pegawai = d.id_user";
                sql += string.Format(" 	where date_trunc('year', a.created_date) = date_trunc('year', to_date('01-01-{0}','dd-mm-yyyy'))", Tahun);
                sql += " ) mon on kec.id_kecamatan = mon.id_kecamatan";

                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Query<MonevSpasialTempModel>(sql);
                    if (data == null || data.Count() == 0)
                    {
                        return "data tidak ada";
                    }

                    foreach (var item in data.OrderBy(x => x.Provinsi).ThenBy(x => x.Kabupaten).ThenBy(x => x.Kecamatan))
                    {
                        if (item.Pegawai != null)
                        {
                            oData.GraphInput.JmlInput += 1;
                        }
                        else
                        {
                            oData.GraphInput.JmlBlmInput += 1;
                        }

                        if (item.Rata2Ciq < 25)
                        {
                            oData.GraphCiq.Under25 += 1;
                        }
                        else if (item.Rata2Ciq > 75)
                        {
                            oData.GraphCiq.Up75 += 1;
                        }
                        else
                        {
                            oData.GraphCiq.Between25_75 += 1;
                        }

                        if (item.Rata2Pap < 25)
                        {
                            oData.GraphPap.Under25 += 1;
                        }
                        else if (item.Rata2Pap > 75)
                        {
                            oData.GraphPap.Up75 += 1;
                        }
                        else
                        {
                            oData.GraphPap.Between25_75 += 1;
                        }

                        MonevSpasialBaseModel baseModel = new MonevSpasialBaseModel
                        {
                            Jabatan = item.Jabatan,
                            Kabupaten = item.Kabupaten,
                            Kecamatan = item.Kecamatan,
                            Pegawai = item.Pegawai,
                            Provinsi = item.Provinsi,
                            Rata2Ciq = item.Rata2Ciq,
                            Rata2Pap = item.Rata2Pap,
                            TanggalInput = item.TanggalInput,
                            Lat = item.Lat,
                            Lon = item.Lon
                        };
                        MonevSpasialTableListModel m = new MonevSpasialTableListModel(baseModel);
                        m.Kecamatan = SetActionScript1(item.Lon, item.Lat, GeomTypeConstant.Point, item.Kecamatan);
                        oData.DataTable.ListData.Add(m);

                        MonevSpasialPropertyModel properties = new MonevSpasialPropertyModel(baseModel);
                        properties.marker_color = "blue";
                        if (!string.IsNullOrEmpty(item.Pegawai))
                            properties.marker_color = "red";
                        properties.marker_size = "small";
                        properties.marker_symbol = "building";
                        MonevSpasialFeatureModel feature = new MonevSpasialFeatureModel
                        {
                            type = "Feature",
                            geometry = new PointGeometryModel(),
                            properties = properties
                        };
                        feature.geometry = JsonConvert.DeserializeObject<PointGeometryModel>(item.GJson);
                        //set protrties
                        //fill dll
                        oData.Map.GJson.features.Add(feature);
                    }

                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GetMonevSpasial", ex);
            }
        }
        public string GetMonevDashboard(int Tahun, out DashboardMonevModel oData)
        {
            Type type = typeof(MonevSpasialTableListModel);

            oData = new DashboardMonevModel
            {
                GraphInput = new  List<Graph1Model>(),
                GraphProvSudahInput = new List<Graph1Model>(),
                GraphProvBelumInput = new List<Graph1Model>(),
                DataTable = new MonevSpasialTableModel
                {
                    ListColumn = SetTableColumn(type),
                    ListData = new List<MonevSpasialTableListModel>()
                },
                Map = new MonevSpasialMapModel
                {
                    LayerName = "Sebaran Monev Kecamatan",
                    ZoomLevel = 11,
                    CenterPoint = new PointModel
                    {
                        Lon = -6.1831142,
                        Lat = 106.8301226
                    },
                    GJson = new MonevSpasialGJsonModel
                    {
                        features = new List<MonevSpasialFeatureModel>(),
                        type = "FeatureCollection"
                    }
                }
            };
            try
            {
                string sql = "";
                sql += " select ";
                sql += " 	prov.nama as Provinsi,";
                sql += " 	kab.nama as Kabupaten,";
                sql += " 	kec.nama as Kecamatan,";
                sql += " 	trim(mon.first_name||' '||coalesce(mon.middle_name, '')||' '||coalesce(mon.last_name, '')) as Pegawai,";
                sql += " 	mon.jabatan,";
                sql += " 	(case when mon.created_date is null then '' else to_char(mon.created_date,'dd-mm-yyyy') end) as TanggalInput,";
                sql += " 	mon.rata_rata_ciq as Rata2Ciq,";
                sql += " 	mon.rata_rata_pap as Rata2Pap,";
                sql += " 	kecf.longitude as Lon,";
                sql += " 	kecf.latitude as Lat,";
                sql += " 	(case when mon is null then 0 else 1 end) SudahInput,";
                sql += " 	st_asgeojson(kecf.geom) AS gjson";
                sql += " from wil.tb_kecamatan kec";
                sql += " join org.tb_kecamatan_profil kecf on kec.id_kecamatan = kecf.id_kecamatan";
                sql += " join wil.tb_kabupaten kab on kec.id_kabupaten = kab.id_kabupaten";
                sql += " join wil.tb_provinsi prov on kab.id_provinsi = prov.id_provinsi";
                sql += " left join (";
                sql += " 	select ";
                sql += " 		b.id_kecamatan, d.first_name, d.middle_name, d.last_name,";
                sql += " 	    c.nama as jabatan, a.created_date, a.rata_rata_ciq, a.rata_rata_pap";
                sql += " 	from mnv.tb_monev a";
                sql += " 	join org.tb_pegawai_kecamatan b on a.id_pegawai = b.id_pegawai";
                sql += " 	join org.tb_jabatan c on b.id_jabatan = c.id_jabatan";
                sql += " 	join public.tb_user d on b.id_pegawai = d.id_user";
                sql += string.Format(" 	where date_trunc('year', a.created_date) = date_trunc('year', to_date('01-01-{0}','dd-mm-yyyy'))", Tahun);
                sql += " ) mon on kec.id_kecamatan = mon.id_kecamatan";

                using (var conn = commonService.DbConnection())
                {
                    var data = conn.Query<MonevSpasialTempModel>(sql);
                    if (data == null || data.Count() == 0)
                    {
                        return "data tidak ada";
                    }
                    oData.GraphInput.Add(new Graph1Model
                    {
                        Kategori = "Sudah Input",
                        Jumlah = (from a in data
                                  where a.SudahInput == 1
                                  select a).Count()
                    });
                    oData.GraphInput.Add(new Graph1Model
                    {
                        Kategori = "Belum Input",
                        Jumlah = (from a in data
                                  where a.SudahInput == 0
                                  select a).Count()
                    });
                    var g2 = data.GroupBy(x => x.Provinsi)
                        .Select(g => new { Kategori = g.Key, JmlSudahInput = g.Sum(c => c.SudahInput == 0 ? 0 : 1), JmlBelumInput = g.Sum(c => c.SudahInput == 0 ? 1 : 0) })
                        .ToList();

                    foreach (var item in g2)
                    {
                        var pctSudahInput = 100 * item.JmlSudahInput / (item.JmlSudahInput + item.JmlBelumInput);
                        var pctBelumInput = 100 * item.JmlBelumInput / (item.JmlSudahInput + item.JmlBelumInput);
                        oData.GraphProvSudahInput.Add(new Graph1Model { Kategori = item.Kategori, Jumlah = pctSudahInput });
                        oData.GraphProvBelumInput.Add(new Graph1Model { Kategori = item.Kategori, Jumlah = pctBelumInput });
                    }
                    foreach (var item in data.OrderBy(x => x.Provinsi).ThenBy(x => x.Kabupaten).ThenBy(x => x.Kecamatan))
                    {
                        MonevSpasialBaseModel baseModel = new MonevSpasialBaseModel
                        {
                            Jabatan = item.Jabatan,
                            Kabupaten = item.Kabupaten,
                            Kecamatan = item.Kecamatan,
                            Pegawai = item.Pegawai,
                            Provinsi = item.Provinsi,
                            Rata2Ciq = item.Rata2Ciq,
                            Rata2Pap = item.Rata2Pap,
                            TanggalInput = item.TanggalInput,
                            Lat = item.Lat,
                            Lon = item.Lon
                        };
                        MonevSpasialTableListModel m = new MonevSpasialTableListModel(baseModel);
                        //m.Kecamatan = SetActionScript(item.Lon, item.Lat, GeomTypeConstant.Point, item.Kecamatan);
                        oData.DataTable.ListData.Add(m);

                        MonevSpasialPropertyModel properties = new MonevSpasialPropertyModel(baseModel);
                        properties.marker_color = "blue";
                        if (!string.IsNullOrEmpty(item.Pegawai))
                            properties.marker_color = "red";
                        properties.marker_size = "small";
                        properties.marker_symbol = "building";
                        MonevSpasialFeatureModel feature = new MonevSpasialFeatureModel
                        {
                            type = "Feature",
                            geometry = new PointGeometryModel(),
                            properties = properties
                        };
                        feature.geometry = JsonConvert.DeserializeObject<PointGeometryModel>(item.GJson);
                        //set protrties
                        //fill dll
                        oData.Map.GJson.features.Add(feature);
                    }
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GetMonevDashboard", ex);
            }
        }
        public string GetMonev(int IdMonev, out MonevModel oData)
        {
            oData = new MonevModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    return GetMonev(conn, IdMonev, out oData);
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GetMonev", ex);
            }
        }
        public string AddMonev(int IdUser, int IdPegawai, int IdJenisLokpri, out MonevModel oJawaban)
        {
            oJawaban = new MonevModel { IsEdit = false};
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    int TempIdMonev = 0;
                    string err = string.Empty;
                    bool isEdit = false;
                    using (var tx = conn.BeginTransaction())
                    {
                        var pegawai = (from a in conn.GetList<TbPegawaiKecamatan>()
                                       where a.IdPegawai == IdPegawai
                                       select a).FirstOrDefault();
                        if (pegawai == null) return "data pegawai tidak terdaftar";

                        var tbMonev = (from a in conn.GetList<TbMonev>()
                                     join b in conn.GetList<TbPegawaiKecamatan>() on a.IdPegawai equals b.IdPegawai
                                     where a.IdJenisLokpri == IdJenisLokpri
                                     & b.IdKecamatan == pegawai.IdKecamatan
                                     & a.CreatedDate.Year == commonService.GetCurrdate().Year
                                     select a).FirstOrDefault();
                        if(tbMonev!=null)
                        {
                            tbMonev.UpdatedBy = IdUser;
                            tbMonev.UpdatedDate = commonService.GetCurrdate();
                            tbMonev.IdPegawai = IdPegawai;
                            conn.Update(tbMonev);
                            isEdit = true;
                            TempIdMonev = tbMonev.IdMonev;
                        }
                        else
                        {
                            tbMonev = new TbMonev
                            {
                                IdPegawai = IdPegawai,
                                CreatedBy = IdUser,
                                CreatedDate = commonService.GetCurrdate(),
                                IdJenisLokpri = IdJenisLokpri
                            };
                            var idMonev = conn.Insert(tbMonev);
                            TempIdMonev = (int)idMonev;
                            var data = from variabel in conn.GetList<TbVariabel>()
                                              join indikator in conn.GetList<TbIndikator>() on variabel.IdVariabel equals indikator.IdVariabel
                                              join indikatorPertanyaan in conn.GetList<TbIndikatorPertanyaan>() on indikator.IdIndikator equals indikatorPertanyaan.IdIndikator
                                              join pertanyaan in conn.GetList<TbPertanyaan>() on indikatorPertanyaan.IdPertanyaan equals pertanyaan.IdPertanyaan
                                              where indikatorPertanyaan.IdJenisLokpri == IdJenisLokpri
                                              select new { variabel, indikator, indikatorPertanyaan, pertanyaan };
                            var dataVariabel = data
                                .GroupBy(x => new { x.variabel })
                                .Select(g => g.First())
                                .ToList()
                                .OrderBy(x => x.variabel.IdVariabel);
                            foreach (var itemVariabel in dataVariabel)
                            {
                                TbMonevDetil tbMonevDetil = new TbMonevDetil
                                {
                                    IdMonev = TempIdMonev,
                                    IdVariabel = itemVariabel.variabel.IdVariabel,
                                    CreatedBy = IdUser,
                                    CreatedDate = commonService.GetCurrdate()
                                };
                                var idMonevDetil = (int)conn.Insert(tbMonevDetil);
                                var dataIndikator = data.Where(x => x.variabel == itemVariabel.variabel)
                                    .GroupBy(x => x.indikator)
                                    .Select(g => g.First())
                                    .ToList()
                                    .OrderBy(x => x.indikator.IdIndikator);
                                foreach (var itemIndikator in dataIndikator)
                                {
                                    var dataPertanyaan = from a in data
                                                         where a.variabel == itemVariabel.variabel
                                                         select a;
                                    dataPertanyaan = from a in dataPertanyaan
                                                     where a.indikator.IdIndikator == itemIndikator.indikator.IdIndikator
                                                     select a;

                                    foreach (var itemPertanyaan in dataPertanyaan)
                                    {
                                        TbJawaban tbJawaban = new TbJawaban
                                        {
                                            IdMonevDetil = idMonevDetil,
                                            IdIndikatorPertanyaan = itemPertanyaan.indikatorPertanyaan.IdIndikatorPertanyaan
                                        };
                                        var idJawaban = conn.Insert(tbJawaban);
                                    }
                                }

                            }
                        }
                        tx.Commit();
                        err = GetMonev(conn, TempIdMonev, out oJawaban);
                        if (!string.IsNullOrEmpty(err)) return err;
                        oJawaban.IsEdit = isEdit;
                    }
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddMonev", ex);
            }
        }
        public string DeleteMonev(int IdUser, int IdMonev)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbMonev = conn.Get<TbMonev>(IdMonev);
                        if (tbMonev == null) return "data tidak ada";
                        var tbMonevDetils = from a in conn.GetList<TbMonevDetil>()
                                            where a.IdMonev == tbMonev.IdMonev
                                            select a;
                        foreach (var tbMonevDetil in tbMonevDetils)
                        {
                            var tbJawabans = from a in conn.GetList<TbJawaban>()
                                             where a.IdMonevDetil == tbMonevDetil.IdMonevDetil
                                             select a;
                            foreach (var tbJawaban in tbJawabans)
                            {
                                conn.Delete(tbJawaban);
                            }
                            conn.Delete(tbMonevDetil);
                        }
                        conn.Delete(tbMonev);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteMonev", ex);
            }
        }
        public string GetlaporanRekap(int IdUser, int Tahun, out List<LaporanRekapModel> Rekaps, out List<LaporanDaftarModel> DetilSudahInputs, out List<LaporanDaftarModel> DetilBelumInputs)
        {
            Rekaps = new List<LaporanRekapModel>();
            DetilBelumInputs = new List<LaporanDaftarModel>();
            DetilSudahInputs = new List<LaporanDaftarModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbKecamatanProfil>()
                               join b in conn.GetList<TbKecamatan>() on a.IdKecamatan equals b.IdKecamatan
                               join c in conn.GetList<TbKabupaten>() on b.IdKabupaten equals c.IdKabupaten
                               join d in conn.GetList<TbProvinsi>() on c.IdProvinsi equals d.IdProvinsi
                               join e in (from a1 in conn.GetList<TbMonev>()
                                          join a2 in conn.GetList<TbJenisLokpri>() on a1.IdJenisLokpri equals a2.IdJenisLokpri
                                          join b1 in conn.GetList<TbPegawaiKecamatan>() on a1.IdPegawai equals b1.IdPegawai
                                          join c1 in conn.GetList<TbUser>() on b1.IdPegawai equals c1.IdUser
                                          join d1 in conn.GetList<TbJabatan>() on b1.IdJabatan equals d1.IdJabatan
                                          join e1 in conn.GetList<TbUser>() on a1.CreatedBy equals e1.IdUser
                                          where a1.CreatedDate.Year == Tahun
                                          select new
                                          {
                                              a1.RataRataCiq,
                                              a1.RataRataPap,
                                              JenisLokpri = a2.Nama,
                                              b1.IdKecamatan,
                                              NamaPejabat = c1.FirstName
                                                            + (string.IsNullOrEmpty(c1.MiddleName) ? "" : " " + c1.MiddleName)
                                                            + (string.IsNullOrEmpty(c1.LastName) ? "" : " " + c1.LastName),
                                              c1.MiddleName,
                                              c1.LastName,
                                              c1.PhoneNumber,
                                              c1.Email,
                                              Jabatan = d1.Nama,
                                              TanggalInput = a1.CreatedDate.ToString("dd-MM-yyyy"),
                                              JamInput = a1.CreatedDate.ToString("HH:mm:ss"),
                                              NamaPetugas = e1.FirstName
                                                            + (string.IsNullOrEmpty(e1.MiddleName) ? "" : " " + e1.MiddleName)
                                                            + (string.IsNullOrEmpty(e1.LastName) ? "" : " " + e1.LastName)
                                          }) on a.IdKecamatan equals e.IdKecamatan into lefte
                               from e in lefte.DefaultIfEmpty()
                               select new { a, b, c, d, e };
                    if (data == null || data.Count() == 0) return "data tidak ada";
                    int j = 0;
                    int belumInput = 0;
                    int sudahInput = 0;
                    int noBelumInput = 0;
                    int noSudahInput = 0;
                    foreach (var item in data.OrderBy(x => x.d.IdProvinsi).ThenBy(x => x.c.IdKabupaten).ThenBy(x => x.b.IdKecamatan))
                    {

                        LaporanDaftarModel det = new LaporanDaftarModel
                        {
                            AlamatKantor = item.a.Alamat,
                            Ciq = item.e == null ? 0 : item.e.RataRataCiq,
                            Email = item.e == null ? string.Empty : item.e.Email,
                            Jabatan = item.e == null ? string.Empty : item.e.Jabatan,
                            JamInput = item.e == null ? string.Empty : item.e.JamInput,
                            JenisLokpri = item.e==null?string.Empty:item.e.JenisLokpri,
                            Kabupaten = item.c.Nama,
                            Kecamatan = item.b.Nama,
                            NamaPejabat = item.e == null ? string.Empty : item.e.NamaPejabat,
                            No = 0,
                            NoHp = item.e == null ? string.Empty : item.e.PhoneNumber,
                            Pap = item.e == null ? 0 : item.e.RataRataPap,
                            PetugasInput = item.e == null ? string.Empty : item.e.NamaPetugas,
                            Provinsi = item.d.Nama,
                            TanggalInput = item.e == null ? string.Empty : item.e.TanggalInput
                        };
                        if (item.e == null)
                        {
                            det.No = ++noBelumInput;
                            DetilBelumInputs.Add(det);
                            belumInput = 1;
                            sudahInput = 0;
                        }
                        else
                        {
                            det.No = ++noSudahInput;
                            DetilSudahInputs.Add(det);
                            belumInput = 0;
                            sudahInput = 1;
                        }
                        var cek = (from a in Rekaps
                                   where a.Provinsi == det.Provinsi
                                   select a).FirstOrDefault();
                        if (cek == null)
                        {
                            cek = new LaporanRekapModel
                            {
                                Provinsi = det.Provinsi,
                                BelumInput = belumInput,
                                SudahInput = sudahInput,
                                No = ++j
                            };
                        }
                        else
                        {
                            Rekaps.Remove(cek);
                            cek.SudahInput += sudahInput;
                            cek.BelumInput += belumInput;
                        }
                        Rekaps.Add(cek);
                    }
                    Rekaps = Rekaps.OrderBy(x => x.No).ToList();
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "Getlaporan", ex);
            }
        }
        public string GetLaporanDetil(int IdUser, int IdMonev, out LaporanDetilMonevModel oData)
        {
            oData = new LaporanDetilMonevModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    string err =  GetMonev(conn, IdMonev, out MonevModel monev);
                    if (!string.IsNullOrEmpty(err)) return err;

                    var item = (from a in conn.GetList<TbMonev>(IdMonev)
                                join peg in conn.GetList<TbPegawaiKecamatan>() on a.IdPegawai equals peg.IdPegawai
                                join pegUser in conn.GetList<TbUser>() on peg.IdPegawai equals pegUser.IdUser
                                join jabatan in conn.GetList<TbJabatan>() on peg.IdJabatan equals jabatan.IdJabatan
                                join kec in conn.GetList<TbKecamatan>() on peg.IdKecamatan equals kec.IdKecamatan
                                join kecProfil in conn.GetList<TbKecamatanProfil>() on kec.IdKecamatan equals kecProfil.IdKecamatan
                                join kab in conn.GetList<TbKabupaten>() on kec.IdKabupaten equals kab.IdKabupaten
                                join prov in conn.GetList<TbProvinsi>() on kab.IdProvinsi equals prov.IdProvinsi
                                join ptgs in (from user in conn.GetList<TbUser>()
                                              join userRole in conn.GetList<TbUserRole>() on user.IdUser equals userRole.IdUser
                                              join role in conn.GetList<TbRole>() on userRole.IdRole equals role.IdRole
                                              join peg in (from a in conn.GetList<TbPegawaiKecamatan>()
                                                           join b in conn.GetList<TbJabatan>() on a.IdPegawai equals b.IdJabatan
                                                           join c in conn.GetList<TbKecamatanProfil>() on a.IdKecamatan equals c.IdKecamatan
                                                           select new { a.IdPegawai, Jabatan = b.Nama, c.Alamat }) on user.IdUser equals peg.IdPegawai into peg1
                                              from peg in peg1.DefaultIfEmpty()
                                              select new { user.IdUser, user.FirstName, user.MiddleName, user.LastName, Email = user.Email, user.PhoneNumber, Jabatan = peg == null ? role.RoleName : peg.Jabatan, AlamatKantor = peg == null ? "" : peg.Alamat }
                                              ) on a.CreatedBy equals ptgs.IdUser
                                select new { a , peg, pegUser, jabatan, ptgs, kec, kecProfil, kab, prov }).FirstOrDefault();
                    oData = new LaporanDetilMonevModel
                    {
                        AlamatKantor = item.kecProfil.Alamat,
                        Email = item.pegUser.Email,
                        Jabatan = item.jabatan.Nama,
                        JamInput = item.a.CreatedDate.ToString("HH:mm:ss"),
                        JenisLokpri = monev.JenisLokpri,
                        Kabupaten = item.kab.Nama,
                        Kecamatan = item.kec.Nama,
                        Nama = item.pegUser.FirstName+(string.IsNullOrEmpty(item.pegUser.MiddleName)?"":" "+ item.pegUser.MiddleName) + (string.IsNullOrEmpty(item.pegUser.LastName) ? "" : " " + item.pegUser.LastName),
                        NoHp = item.pegUser.PhoneNumber,
                        Provinsi = item.prov.Nama,
                        PtgsEmail = item.ptgs.Email,
                        PtgsJabatan = item.ptgs.Jabatan,
                        PtgsNama = item.ptgs.FirstName + (string.IsNullOrEmpty(item.ptgs.MiddleName) ? "" : " " + item.ptgs.MiddleName) + (string.IsNullOrEmpty(item.ptgs.LastName) ? "" : " " + item.ptgs.LastName),
                        PtgsNoHp = item.ptgs.PhoneNumber,
                        Rata2Ciq = item.a.RataRataCiq,
                        Rata2Pap = item.a.RataRataPap,
                        Tahun = item.a.CreatedDate.Year,
                        TanggalInput = item.a.CreatedDate.ToString("dd-MM-yyyy"),
                        Monev = monev,
                    };
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GetLaporanDetil", ex);
            }
        }

        private string SetJawaban(IDbConnection conn, int IdUser, int IdMonev, int Type, List<MonevPertanyaanJawabanModel> Jawabans)
        {
            try
            {
                TbMonev tbMonev = conn.Get<TbMonev>(IdMonev);
                foreach (var item in Jawabans.OrderBy(x => x.IdPertanyaan))
                {
                    TbJawaban tbJawaban = conn.Get<TbJawaban>(item.IdJawaban);
                    switch (Type)
                    {
                        case 1:
                            tbJawaban.Ketersediaan = item.Ketersediaan;

                            tbJawaban.BobotKetersediaan = (from a in conn.GetList<TbBobotKetersediaan>()
                                                           where a.Nama == tbJawaban.Ketersediaan
                                                           select a.Bobot).FirstOrDefault();
                            tbJawaban.Kecukupan = item.Kecukupan;
                            tbJawaban.BobotKecukupan = (from a in conn.GetList<TbBobotKecukupan>()
                                                        where a.Nama == tbJawaban.Kecukupan
                                                        select a.Bobot).FirstOrDefault();
                            tbJawaban.Kondisi = item.Kondisi;
                            tbJawaban.BobotKondisi = (from a in conn.GetList<TbBobotKondisi>()
                                                      where a.Nama == tbJawaban.Kondisi
                                                      select a.Bobot).FirstOrDefault();
                            break;
                        case 2:
                            tbJawaban.Kebutuhan = item.Kebutuhan;
                            tbJawaban.Keterisian = item.Keterisian;
                            tbJawaban.BobotKebKet = item.Kebutuhan == 0 ? null : (100 * item.Kebutuhan) * item.Keterisian;
                            break;
                        case 3: //perlu dicek lagi fomulanya
                            tbJawaban.Jumlah = item.Jumlah == null ? 0 : item.Jumlah;
                            tbJawaban.BobotNilai = (from a in conn.GetList<TbPertanyaan>()
                                                    where a.IdPertanyaan == item.IdPertanyaan
                                                    select a.BobotDefault).FirstOrDefault();
                            tbJawaban.JumlahBobotNilai = (tbJawaban.Jumlah == null ? 0 : tbJawaban.Jumlah) * (tbJawaban.BobotNilai == null ? 0 : (int)tbJawaban.BobotNilai);
                            break;
                        case 4:
                            tbJawaban.Ketersediaan = item.Ketersediaan;
                            tbJawaban.BobotKetersediaan = (from a in conn.GetList<TbBobotKetersediaan>()
                                                           where a.Nama == tbJawaban.Ketersediaan
                                                           select a.Bobot).FirstOrDefault();
                            break;

                        default:
                            return "Type salah";
                    }
                    conn.Update(tbJawaban);
                }

                var dataMonevDetil = from monevDetil in conn.GetList<TbMonevDetil>()
                                     join variabel in conn.GetList<TbVariabel>() on monevDetil.IdVariabel equals variabel.IdVariabel
                                     where monevDetil.IdMonev == IdMonev
                                     select new { monevDetil, variabel.Presentase, variabel.IdJenisVariabel };
                double _ciq = 0;
                double _pap = 0;
                foreach (var itemMonevDetil in dataMonevDetil.OrderBy(x=>x.monevDetil.IdVariabel).ThenBy(x=>x.monevDetil.IdMonevDetil))
                {
                    TbMonevDetil tbMonevDetil = itemMonevDetil.monevDetil;
                    var dataJawaban = from jawaban in conn.GetList<TbJawaban>()
                                      join indikatorPertanyaan in conn.GetList<TbIndikatorPertanyaan>() on jawaban.IdIndikatorPertanyaan equals indikatorPertanyaan.IdIndikatorPertanyaan
                                      join indikator in conn.GetList<TbIndikator>() on indikatorPertanyaan.IdIndikator equals indikator.IdIndikator
                                      where jawaban.IdMonevDetil == itemMonevDetil.monevDetil.IdMonevDetil
                                      select new { indikator.Type, jawaban };
                    double _count = 0;
                    double _sum = 0;
                    foreach (var itemJawaban in dataJawaban.OrderBy(x=>x.jawaban.IdJawaban))
                    {
                        switch (itemJawaban.Type)
                        {
                            case 1:
                                _count += 1;
                                _sum += (itemJawaban.jawaban.BobotKetersediaan == null ? 0 : (double)itemJawaban.jawaban.BobotKetersediaan)
                                    + (itemJawaban.jawaban.BobotKecukupan == null ? 0 : (double)itemJawaban.jawaban.BobotKecukupan)
                                    + (itemJawaban.jawaban.BobotKondisi == null ? 0 : (double)itemJawaban.jawaban.BobotKondisi);
                                break;
                            case 2:
                                _count += 1;
                                _sum += itemJawaban.jawaban.BobotKebKet == null ? 0 : (double)itemJawaban.jawaban.BobotKebKet;
                                break;
                            case 3:
                                double _jumlah = itemJawaban.jawaban.Jumlah == null ? 0 : (double)itemJawaban.jawaban.Jumlah;
                                _count += _jumlah;
                                if (_jumlah == 0)
                                {
                                    _sum += 0;
                                }
                                else
                                {
                                    _sum += (double)itemJawaban.jawaban.JumlahBobotNilai / _jumlah;
                                }
                                break;
                            case 4:
                                _count += 1;
                                _sum += (itemJawaban.jawaban.BobotKetersediaan == null ? 0 : (double)itemJawaban.jawaban.BobotKetersediaan);
                                break;
                            default:
                                break;
                        }
                    }
                    if (itemMonevDetil.IdJenisVariabel == 1) //PAP
                    {
                        _pap += _sum;
                    }
                    else //CIQ
                    {
                        _ciq += _sum;
                    }
                    tbMonevDetil.Rata2 = (_sum / _count) * (itemMonevDetil.Presentase / (double)100);
                    conn.Update(tbMonevDetil);
                }
                tbMonev.RataRataCiq = _ciq;
                tbMonev.RataRataPap = _pap;
                tbMonev.UpdatedBy = IdUser;
                tbMonev.UpdatedDate = commonService.GetCurrdate();
                conn.Update(tbMonev);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "SetJawaban", ex);
            }
        }
        public string SetGetJawaban(int IdUser, int IdMonev, MonevIndikatorModel InJawaban, int IdVariabel, int IdIndikator, out MonevIndikatorModel OutJawaban)
        {
            OutJawaban = new MonevIndikatorModel
            {
                Pertanyaans = new List<MonevPertanyaanJawabanModel>()
            };
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction()) 

                    {
                        if (InJawaban != null & InJawaban.Pertanyaans != null)
                        {
                            if (InJawaban.Pertanyaans.Count() > 0)
                            {
                                string err = SetJawaban(conn, IdUser, IdMonev, InJawaban.Type, InJawaban.Pertanyaans);
                                if (!string.IsNullOrEmpty(err)) return err;
                                tx.Commit();
                            }
                        }
                    }
                    var dataJawaban = from jawaban in conn.GetList<TbJawaban>()
                                      join monevDetil in conn.GetList<TbMonevDetil>() on jawaban.IdMonevDetil equals monevDetil.IdMonevDetil
                                      join indikatorPertanyaan in conn.GetList<TbIndikatorPertanyaan>() on jawaban.IdIndikatorPertanyaan equals indikatorPertanyaan.IdIndikatorPertanyaan
                                      join indikator in conn.GetList<TbIndikator>() on indikatorPertanyaan.IdIndikator equals indikator.IdIndikator
                                      join pertanyaan in conn.GetList<TbPertanyaan>() on indikatorPertanyaan.IdPertanyaan equals pertanyaan.IdPertanyaan
                                      where indikatorPertanyaan.IdIndikator == IdIndikator
                                      & indikator.IdVariabel == IdVariabel
                                      & monevDetil.IdMonev == IdMonev
                                      select new { jawaban, indikator, pertanyaan };
                    if (dataJawaban != null & dataJawaban.Count() > 0)
                    {
                        foreach (var item in dataJawaban)
                        {
                            OutJawaban.IdIndikator = item.indikator.IdIndikator;
                            OutJawaban.Type = item.indikator.Type;
                            OutJawaban.Indikator = item.indikator.Nama;
                            MonevPertanyaanJawabanModel jawabanPertanyaan = new MonevPertanyaanJawabanModel
                            {
                                IdIndikatorPertanyaan = item.jawaban.IdIndikatorPertanyaan,
                                IdPertanyaan = item.pertanyaan.IdPertanyaan,
                                IdJawaban = item.jawaban.IdJawaban,
                                Ketersediaan = item.jawaban.Ketersediaan,
                                BobotKetersediaan = item.jawaban.BobotKetersediaan,
                                Kecukupan = item.jawaban.Kecukupan,
                                BobotKecukupan = item.jawaban.BobotKecukupan,
                                Kondisi = item.jawaban.Kondisi,
                                BobotKondisi = item.jawaban.BobotKondisi,
                                Kebutuhan = item.jawaban.Kebutuhan,
                                Keterisian = item.jawaban.Keterisian,
                                Jumlah = item.jawaban.Jumlah,
                                BobotNilai = item.jawaban.BobotNilai,
                                JumlahBobotNilai = item.jawaban.JumlahBobotNilai,
                                BobotDefault = null,
                                BobotkebKet = item.jawaban.BobotKebKet
                            };
                            OutJawaban.Pertanyaans.Add(jawabanPertanyaan);
                        }
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "GetLaporanDetil", ex);
            }
        }

    }
}
