﻿using Dapper;
using Microsoft.AspNetCore.Http;
using Swg.Bnpp.Entities.Public;
using Swg.Bnpp.Models.Constants;
using Swg.Bnpp.Models.UserApp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using Swg.Bnpp.Entities.Org;

namespace Swg.Bnpp.Services
{
    public interface IUserAppService
    {
        //ApplSetting
        List<ApplSettingModel> GetApplSettings(out string oMessage);
        ApplSettingModel GetApplSetting(string Code, out string oMessage);

        string AddApplSetting(ApplSettingModel Data);
        string EditApplSetting(ApplSettingModel Data);
        string DeleteApplSetting(string Code);

        //Role
        List<RoleModel> GetRoles(out string oMessage);
        RoleModel GetRole(int IdRole, out string oMessage);
        string AddRole(int IdRole, string RoleName);
        string DeleteRole(int IdRole);

        //User
        UserModel Login(string UsernameOrEmail, string Password, int IdAppl, out string oMessage);
        List<UserModel> GetUsers(out string oMessage);
        UserModel GetUser(int IdUser, out string oMessage);

        string AddUser(int IdUser, UserAddModel Data);
        string AddUser(IDbConnection conn, int IdUser, UserAddModel Data, out int oIdUser);
        string EditUser(int IdUser, UserModel Data);
        public string EditUser(IDbConnection conn, int IdUser, UserModel Data);
        string ChangePassword(string UsernamOrEmail, string OldPassword, string NewPassword1, string NewPassword2);
        string ResetPassword(string UsernameOrEmail, out string oMessage);
        string UpdateProfile(int IdUser, string Email, string FirstName, string MiddleName, string LastName, string Address, string PhoneNumber, string MobileNumber, IFormFile FileImage);
        string SetUserActive(int IdUser, int SetIdUser, out string oMessage);
        string SetUserInActive(int IdUser, int SetIdUser, out string oMessage);


        //ApplTask
        List<ApplModel> GetAppls(out string oMessage);
        string AddAppl(int IdAppl, string ApplName);
        string EditAppl(int IdAppl, string ApplName);

        string AddApplTask(ApplTaskAddModel Data);
        string EditApplTask(ApplTaskEditModel Data);
        string DeleteApplTask(int IdApplTask);

        ApplTaskRoleModel GetApplTaskByRole(int IdRole, out string oMessage);
        string AddTasksRole(int IdRole, List<int> IdApplTasks);

        List<ApplTaskModel> GetMenus(int IdAppl, int IdUser, out string oMessage);
        List<ApplTaskModel> GetMenus(int IdAppl, string Username, out string oMessage);
        List<ApplTaskModel> GetApplTasks(int IdAppl, out string oMessage);

        List<MenuModel> GetMenus(int IdAppl, int? IdUser, string Username, out string oMessage);

    }
    public class UserAppService : IUserAppService
    {
        private readonly string ServiceName = "UserAppService.";
        private readonly ICommonService commonService;
        public UserAppService(ICommonService CommonService)
        {
            commonService = CommonService;
        }
        #region ApplSetting
        private string[] DefaultCodes = { "namaorg", "namaorgpendek", "alamatorg", "longitudeorg", "latituteorg", "telporg", "faxorg", "weborg", "emailorgsmtpserver", "emailorgsmtpport", "emailorg", "emailorgpassword" };

        public string AddApplSetting(ApplSettingModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var a = string.Empty;
                        TbApplSetting tb = new TbApplSetting();
                        tb.Code = Data.Code;
                        tb.Name = Data.Name;
                        tb.ValueType = Data.ValueType;
                        if (tb.ValueType == ValueTypeConstant.Bilangan)
                        {
                            tb.ValueNumber = Data.ValueNumber;
                        }
                        else if (tb.ValueType == ValueTypeConstant.Karakter)
                        {
                            tb.ValueString = Data.ValueString;
                            if (Data.FileUpload != null)
                            {
                                string pathUpload = Path.Combine(AppSetting.PathFile, "Appl");
                                tb.ValueString = string.Format("{0}{1}", Data.Code, Path.GetExtension(Data.FileUpload.FileName));
                                if (File.Exists(Path.Combine(pathUpload, tb.ValueString)))
                                    File.Delete(Path.Combine(pathUpload, tb.ValueString));
                                using (var fileStream = new FileStream(Path.Combine(pathUpload, tb.ValueString), FileMode.Create))
                                {
                                    Data.FileUpload.CopyTo(fileStream);
                                }
                            }
                        }
                        else
                        {
                            tb.ValueDate = commonService.StrToDateTime(Data.ValueDate);
                        }
                        string sqlIns = "INSERT INTO public.tb_appl_setting(code, name, value_type, value_date, value_number, value_string) VALUES";
                        sqlIns += " (@code, @name, @valueType, @valueDate, @valueNumber, @valueString);";
                        var affectedRows = conn.Execute(sqlIns, tb);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddApplSetting", ex);
            }

        }

        public string DeleteApplSetting(string Code)
        {
            try
            {
                if (DefaultCodes.Contains(Code.ToLower())) return "data tidak bisa dihapus";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbApplSetting = (from a in conn.GetList<TbApplSetting>()
                                             where a.Code.ToLower() == Code.ToLower()
                                             select a).FirstOrDefault();
                        if (tbApplSetting == null) return "data tidak ada";

                        conn.Delete(tbApplSetting);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteApplSetting", ex);
            }
        }

        public string EditApplSetting(ApplSettingModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbApplSetting = (from a in conn.GetList<TbApplSetting>()
                                             where a.Code == Data.Code
                                             select a).FirstOrDefault();
                        if (tbApplSetting == null) return "data tidak ada";
                        Console.WriteLine("masuk Data.ValueType:" + Data.ValueType);
                        if (Data.ValueType == ValueTypeConstant.Bilangan)
                        {
                            tbApplSetting.ValueNumber = Data.ValueNumber;
                            tbApplSetting.ValueString = string.Empty;
                            tbApplSetting.ValueDate = null;
                        }
                        else if (Data.ValueType == ValueTypeConstant.Karakter)
                        {
                            tbApplSetting.ValueNumber = null;
                            tbApplSetting.ValueString = Data.ValueString;
                            tbApplSetting.ValueDate = null;
                            if (Data.FileUpload != null)
                            {
                                string pathUpload = Path.Combine(AppSetting.PathFile, "Appl");
                                tbApplSetting.ValueString = string.Format("{0}{1}", Data.Code, Path.GetExtension(Data.FileUpload.FileName));
                                if (File.Exists(Path.Combine(pathUpload, tbApplSetting.ValueString)))
                                    File.Delete(Path.Combine(pathUpload, tbApplSetting.ValueString));
                                using (var fileStream = new FileStream(Path.Combine(pathUpload, tbApplSetting.ValueString), FileMode.Create))
                                {
                                    Data.FileUpload.CopyTo(fileStream);
                                }
                            }

                        }
                        else
                        {
                            tbApplSetting.ValueNumber = null;
                            tbApplSetting.ValueString = string.Empty;
                            tbApplSetting.ValueDate = commonService.StrToDateTime(Data.ValueDate);
                        }
                        tbApplSetting.Name = Data.Name;
                        conn.Update(tbApplSetting);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditApplSetting", ex);
            }
        }

        public ApplSettingModel GetApplSetting(string Code, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                ApplSettingModel ret = new ApplSettingModel();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var tbApplSetting = (from a in conn.GetList<TbApplSetting>()
                                         where a.Code.ToLower() == Code.ToLower()
                                         select a).FirstOrDefault();
                    if (tbApplSetting == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    return new ApplSettingModel
                    {
                        Code = tbApplSetting.Code,
                        Name = tbApplSetting.Name,
                        ValueDate = tbApplSetting.ValueDate == null ? "" : tbApplSetting.ValueDate.Value.ToString("dd-MM-yyyy"),
                        ValueNumber = tbApplSetting.ValueNumber == null ? 0 : tbApplSetting.ValueNumber.Value,
                        ValueString = tbApplSetting.ValueString,
                        ValueType = tbApplSetting.ValueType
                    };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetApplSetting", ex);
                return null;
            }
        }

        public List<ApplSettingModel> GetApplSettings(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<ApplSettingModel> ret = new List<ApplSettingModel>();
                using (var conn = commonService.DbConnection())
                {
                    var tbApplSettings = conn.GetList<TbApplSetting>();
                    if (tbApplSettings == null || tbApplSettings.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var tbApplSetting in tbApplSettings)
                    {
                        ApplSettingModel m = new ApplSettingModel
                        {
                            Code = tbApplSetting.Code,
                            Name = tbApplSetting.Name,
                            ValueDate = tbApplSetting.ValueDate == null ? "" : tbApplSetting.ValueDate.Value.ToString("dd-MM-yyyy"),
                            ValueNumber = tbApplSetting.ValueNumber == null ? 0 : tbApplSetting.ValueNumber.Value,
                            ValueString = tbApplSetting.ValueString,
                            ValueType = tbApplSetting.ValueType
                        };
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetApplSettings", ex);
                return null;
            }

        }
        #endregion
        #region Role
        public List<RoleModel> GetRoles(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    return (from a in conn.GetList<TbRole>()
                            select new RoleModel
                            {
                                IdRole = a.IdRole,
                                RoleName = a.RoleName
                            }).OrderBy(x => x.IdRole).ToList();
                }
            }
            catch (Exception ex)
            {

                oMessage = commonService.GetErrorMessage(ServiceName + "GetRoles", ex);
                return null;
            }

        }
        public RoleModel GetRole(int IdRole, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    return (from a in conn.GetList<TbRole>()
                            where a.IdRole == IdRole
                            select new RoleModel
                            {
                                IdRole = a.IdRole,
                                RoleName = a.RoleName
                            }).OrderBy(x => x.IdRole).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                oMessage = commonService.GetErrorMessage(ServiceName + "GetRole", ex);
                return null;
            }
        }
        public string AddRole(int IdRole, string RoleName)
        {
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbRole tbRole = conn.Get<TbRole>(IdRole);
                        if (tbRole != null) return "IdRole Sudah ada";
                        tbRole = new TbRole { IdRole = IdRole, RoleName = RoleName };
                        conn.Insert(tbRole);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddRole", ex);
            }
        }
        public string DeleteRole(int IdRole)
        {
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbRole tbRole = conn.Get<TbRole>(IdRole);
                        if (tbRole == null) return "Role tidak ada";
                        conn.Delete(tbRole);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteRole", ex);
            }
        }
        #endregion
        #region User
        public UserModel Login(string UsernameOrEmail, string Password, int IdAppl, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(UsernameOrEmail))
                {
                    oMessage = "UsernameOrEmail tidak boleh kosong";
                    return null;
                }
                if (string.IsNullOrEmpty(Password))
                {
                    oMessage = "Password tidak boleh kosong";
                    return null;
                }
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();

                    var tbUser = (from a in conn.GetList<TbUser>()
                                  where a.Username == UsernameOrEmail || a.Email == UsernameOrEmail
                                  select a).FirstOrDefault();
                    if (tbUser == null)
                    {
                        oMessage = "User tidak terdaftar";
                        return null;
                    }
                    if (tbUser.Status != StatusDataConstant.Aktif)
                    {
                        oMessage = "User Sudah Terdaftar, Sudah Tidak Aktif/Belum Aktif";
                        return null;
                    }

                    //string xPassword = commonService.EncryptString("Admin2021");
                    string password = commonService.DecryptString(tbUser.Password);
                    if (password != Password)
                    {
                        oMessage = "Password Salah";
                        return null;
                    }


                    var _userRoles = (from a in conn.GetList<TbUserRole>()
                                      join b in conn.GetList<TbRole>() on a.IdRole equals b.IdRole
                                      where a.IdUser == tbUser.IdUser
                                      select new RoleModel
                                      {
                                          IdRole = b.IdRole,
                                          RoleName = b.RoleName
                                      }).ToList();
                    var _pegKecamatan = (from a in conn.GetList<TbPegawaiKecamatan>()
                                        where a.IdPegawai == tbUser.IdUser
                                        select a).FirstOrDefault();
                    return new UserModel
                    {
                        IdUser = tbUser.IdUser,
                        Username = tbUser.Username,
                        Password = null,
                        Email = tbUser.Email,
                        StrStatus = StatusDataConstant.Dict[tbUser.Status],
                        LastLogin = tbUser.LastLogin == null ? "" : tbUser.LastLogin.ToString(),
                        Address = tbUser.Address,
                        FirstName = tbUser.FirstName,
                        LastName = tbUser.LastName,
                        MiddleName = tbUser.MiddleName,
                        MobileNumber = tbUser.MobileNumber,
                        PhoneNumber = tbUser.PhoneNumber,
                        FileImage = tbUser.FileImage,
                        Roles = _userRoles,
                        IdKecamatan = _pegKecamatan == null ? null : _pegKecamatan.IdKecamatan
                    };
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "Login", ex);
                return null;
            }
        }
        public List<UserModel> GetUsers(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<UserModel> ret = new List<UserModel>();
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var users = (from a in conn.GetList<TbUser>()
                                 select a).ToList();
                    foreach (var user in users)
                    {
                        var roles = (from a in conn.GetList<TbRole>()
                                     join b in conn.GetList<TbUserRole>() on a.IdRole equals b.IdRole
                                     where b.IdUser == user.IdUser
                                     select new RoleModel { IdRole = a.IdRole, RoleName = a.RoleName }).ToList();
                        string password = commonService.DecryptString(user.Password);
                        UserModel m = new UserModel
                        {
                            Address = user.Address,
                            Email = user.Email,
                            FirstName = user.FirstName,
                            IdUser = user.IdUser,
                            LastLogin = user.LastLogin.ToString(),
                            LastName = user.LastName,
                            MiddleName = user.MiddleName,
                            MobileNumber = user.MobileNumber,
                            Password = password,
                            PhoneNumber = user.PhoneNumber,
                            Roles = roles,
                            StrStatus = StatusDataConstant.Dict[user.Status],
                            Username = user.Username,
                            FileImage = user.FileImage,
                            FullName = string.Empty
                        };
                        m.FullName = this.SetFullName(m.FirstName, m.MiddleName,m.LastName);

                        //if (!string.IsNullOrEmpty(m.MiddleName.Trim()))
                        //    m.FullName += " " + m.MiddleName;
                        //if (string.IsNullOrEmpty(m.LastName.Trim()))
                        //    m.FullName += " " + m.LastName;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUsers", ex);
                return null;
            }

        }
        
        private string SetFullName(string FirstName, string MiddleName, string LastName)
        {
            string nama = FirstName;
            if (!string.IsNullOrEmpty(MiddleName))
                nama += " " + MiddleName;
            if (!string.IsNullOrEmpty(LastName))
                nama += " " + LastName;
            return nama;
        }


        public UserModel GetUser(int IdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var user = (from a in conn.GetList<TbUser>()
                                where a.IdUser == IdUser
                                select a).FirstOrDefault();
                    if (user == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    var roles = (from a in conn.GetList<TbRole>()
                                 join b in conn.GetList<TbUserRole>() on a.IdRole equals b.IdRole
                                 where b.IdUser == user.IdUser
                                 select new RoleModel { IdRole = a.IdRole, RoleName = a.RoleName }).ToList();
                    return new UserModel
                    {
                        Address = user.Address,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        IdUser = user.IdUser,
                        LastLogin = user.LastLogin.ToString(),
                        LastName = user.LastName,
                        MiddleName = user.MiddleName,
                        MobileNumber = user.MobileNumber,
                        Password = "",
                        PhoneNumber = user.PhoneNumber,
                        Roles = roles,
                        StrStatus = StatusDataConstant.Dict[user.Status],
                        Username = user.Username,
                        FileImage = user.FileImage,
                        FullName=this.SetFullName(user.FirstName,user.MiddleName, user.LastName)
                        
                    };

                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetUser", ex);
                return null;

            }
        }
        private static Regex sUserNameAllowedRegEx = new Regex(@"^[a-zA-Z]{1}[a-zA-Z0-9\._\-]{0,23}[^.-]$", RegexOptions.Compiled);
        private static Regex sUserNameIllegalEndingRegEx = new Regex(@"(\.|\-|\._|\-_)$", RegexOptions.Compiled);
        public bool ValidatePassword(string password, out string ErrorMessage)
        {
            var input = password;
            ErrorMessage = string.Empty;

            if (string.IsNullOrWhiteSpace(input))
            {
                throw new Exception("Password tidak boleh kosong");
            }

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMiniMaxChars = new Regex(@".{8,15}");
            var hasLowerChar = new Regex(@"[a-z]+");
            //            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(input))
            {
                ErrorMessage = "Password harus mempunyai minimal satu huruf kecil";
                return false;
            }
            else if (!hasUpperChar.IsMatch(input))
            {
                ErrorMessage = "Password harus mempunyai minimal satu huruf besar";
                return false;
            }
            else if (input.Length < 8)
            {
                ErrorMessage = "Password minimal 8 karakter";
                return false;
            }
            else if (!hasNumber.IsMatch(input))
            {
                ErrorMessage = "Password harus mempunyai nilai numerik";
                return false;
            }
            else
            {
                return true;
            }
        }

        private string ValidateDataUser(UserAddModel Data)
        {
            if (Data == null)
                return "data harus diisi";
            if (string.IsNullOrEmpty(Data.Username))
                return "Username harus diisi";
            if (string.IsNullOrEmpty(Data.Username)
                || !sUserNameAllowedRegEx.IsMatch(Data.Username)
                || sUserNameIllegalEndingRegEx.IsMatch(Data.Username))
            {
                return "Username invalid";
            }

            if (string.IsNullOrEmpty(Data.Password))
                return "Password harus diisi";
            if (!ValidatePassword(Data.Password, out string err))
                return err;

            if (string.IsNullOrEmpty(Data.Email))
                return "Email harus diisi";
            try
            {
                var addr = new System.Net.Mail.MailAddress(Data.Email);
                if (addr.Address != Data.Email)
                    return "alamat email salah";
            }
            catch (Exception)
            {
                return "alamat email salah";

            }
            if (string.IsNullOrEmpty(Data.FirstName))
                return "FirstName harus diisi";

            if (string.IsNullOrEmpty(Data.PhoneNumber))
                return "PhoneNumber harus diisi";

            if (string.IsNullOrEmpty(Data.MobileNumber))
                return "MobileNumber harus diisi";
            if (Data.Roles == null)
                return "Roles harus dipilih";

            return string.Empty;
        }
        public string AddUser(IDbConnection conn, int IdUser, UserAddModel Data, out int oIdUser)
        {
            oIdUser = 0;
            try
            {

                string err = ValidateDataUser(Data);
                if (!string.IsNullOrEmpty(err))
                    return err;
                var tbUsers = (from a in conn.GetList<TbUser>()
                               where a.Username == Data.Username || a.Email == Data.Email
                               select a).ToList();

                if (tbUsers != null && tbUsers.Count > 0)
                {
                    return "Username/email sudah terdaftar";
                }
                string password = commonService.EncryptString(Data.Password);

                TbUser tbUser = new TbUser
                {
                    CreatedDate = DateTime.Now,
                    Email = Data.Email,
                    Password = password,
                    Status = StatusDataConstant.Aktif,
                    Username = Data.Username,
                    Address = Data.Address,
                    FirstName = Data.FirstName,
                    LastName = Data.LastName,
                    MiddleName = Data.MiddleName,
                    MobileNumber = Data.MobileNumber,
                    PhoneNumber = Data.PhoneNumber, FileImage = "file_image.jpg"
                };
                var _idUser = conn.Insert(tbUser);
                oIdUser = (int)_idUser;
                foreach (var item in Data.Roles)
                {
                    TbUserRole tbUserRole = new TbUserRole
                    {
                        IdRole = item.IdRole,
                        IdUser = (int)_idUser
                    };
                    conn.Insert(tbUserRole);
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddUser", ex);
            }

        }
        public string AddUser(int IdUser, UserAddModel Data)
        {
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = AddUser(conn, IdUser, Data, out int oIdUser);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddUser", ex);
            }

        }
        public string EditUser(IDbConnection conn, int IdUser, UserModel Data)
        {
            try
            {
                TbUser tbUser = conn.Get<TbUser>(Data.IdUser);
                if (tbUser == null)
                    return "data tidak ada";
                Data.Username = tbUser.Username;
                Data.Password = tbUser.Password;

                string err = ValidateDataUser(Data);
                if (!string.IsNullOrEmpty(err))
                    return err;

                var cekEmail = (from a in conn.GetList<TbUser>()
                                where a.Email == Data.Email
                                select a).FirstOrDefault();

                if (cekEmail != null)
                {
                    if (Data.IdUser != cekEmail.IdUser)
                        return "email sudah terdaftar untuk username: " + cekEmail.Username;
                }
                tbUser.Email = Data.Email;
                tbUser.UpdatedBy = IdUser;
                tbUser.UpdatedDate = DateTime.Now;
                tbUser.Address = Data.Address;
                tbUser.FirstName = Data.FirstName;
                tbUser.LastName = Data.LastName;
                tbUser.MiddleName = Data.MiddleName;
                tbUser.MobileNumber = Data.MobileNumber;
                tbUser.PhoneNumber = Data.PhoneNumber;

                conn.Update(tbUser);

                var oldRoles = from a in conn.GetList<TbUserRole>()
                               where a.IdUser == tbUser.IdUser
                               select a;
                foreach (var item in oldRoles)
                {
                    conn.Delete(item);
                }

                foreach (var item in Data.Roles)
                {
                    TbUserRole tbUserRole = new TbUserRole
                    {
                        IdRole = item.IdRole,
                        IdUser = tbUser.IdUser
                    };
                    conn.Insert(tbUserRole);
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditUser", ex);
            }

        }
        public string EditUser(int IdUser, UserModel Data)
        {
            try
            {

                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = EditUser(conn, IdUser, Data);
                        if (!string.IsNullOrEmpty(err)) return err;
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditUser", ex);
            }

        }
        public string ChangePassword(string UsernamOrEmail, string OldPassword, string NewPassword1, string NewPassword2)
        {
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        try
                        {
                            var tbUser = conn.GetList<TbUser>("where username = @UsernamOrEmail or email = @UsernamOrEmail", new { UsernamOrEmail }).FirstOrDefault();
                            if (tbUser == null)
                            {
                                return "User tidak terdaftar";
                            }

                            string password = commonService.DecryptString(tbUser.Password);
                            if (password != OldPassword)
                            {
                                return "Password lama salah";
                            }

                            if (string.IsNullOrEmpty(NewPassword1))
                            {
                                return "Password baru kosong";
                            }
                            if (string.IsNullOrEmpty(NewPassword2))
                            {
                                return "Konfirmasi password kosong";
                            }
                            if (NewPassword1 != NewPassword2)
                            {
                                return "Konfirmasi password tidak sama dengan password baru";
                            }
                            tbUser.Password = commonService.EncryptString(NewPassword1);
                            tbUser.LastLogin = DateTime.Now;
                            conn.Update(tbUser);
                            tx.Commit();
                            return string.Empty;
                        }
                        catch (Exception ex)
                        {
                            tx.Rollback();
                            return commonService.GetErrorMessage(ServiceName + "ChangePassword", ex);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "ChangePassword", ex);
            }

        }
        private string CreateRandomPassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
        public string ResetPassword(string UsernameOrEmail, out string oMessage)
        {
            oMessage = string.Empty;

            if (string.IsNullOrEmpty(UsernameOrEmail))
            {
                oMessage = "UsernameOrEmail tidak boleh kosong";
                return null;
            }
            string _newPassword = CreateRandomPassword(8);
            string _newMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbUser = (from a in conn.GetList<TbUser>()
                                      where a.Username == UsernameOrEmail
                                      || a.Email == UsernameOrEmail
                                      select a
                                      ).FirstOrDefault();

                        if (tbUser == null)
                        {
                            oMessage = "User tidak terdaftar";
                            return null;
                        }
                        //sementara dihardcode untuk bnpp
                        tbUser.Password = commonService.EncryptString(tbUser.Username);
                        //tbUser.Password = commonService.EncryptString(_newPassword);
                        //_newMessage = "Password anda telah di reset, silahkan buka email " + tbUser.Email;


                        conn.Update(tbUser);
                        tx.Commit();
                        /*
                        StringBuilder stb = new StringBuilder();
                        stb.Append("<html>");
                        stb.Append("<body>");
                        stb.Append("Berikut kami informasikan password baru anda adalah <strong>" + _newPassword + "</strong><br><br>");
                        stb.Append("</body>");
                        stb.Append("</html>");
                        List<string> emailTos = new List<string>();
                        emailTos.Add(tbUser.Email);
                        SendEmailModel email = new SendEmailModel
                        {
                            To = emailTos,
                            Title = "Bapak/Ibu " + common.SetFullName(tbUserProfile.FirstName, tbUserProfile.MiddleName, tbUserProfile.LastName),
                            Subject = "Permintaan Reset Password Akun " + tbUser.Username,
                            Message = stb.ToString(),
                            Attachments = null
                        };
                        _ = emailService.SendHtmlEmailAsync(email);
                        */
                    }
                }
                return _newMessage;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "ResetPassword", ex);
                return string.Empty;
            }

        }
        public string UpdateProfile(int IdUser, string Email, string FirstName, string MiddleName, string LastName, string Address, string PhoneNumber, string MobileNumber, IFormFile FileImage)
        {
            try
            {
                List<RoleModel> roles = new List<RoleModel>();
                roles.Add(new RoleModel { IdRole = 1, RoleName = "" }); //hanya untuk validasi saja
                UserModel user = new UserModel
                {
                    Address = Address,
                    Email = Email,
                    FirstName = FirstName,
                    IdUser = IdUser,
                    LastLogin = null,
                    LastName = LastName,
                    MiddleName = MiddleName,
                    MobileNumber = MobileNumber,
                    PhoneNumber = PhoneNumber,
                    Password = "A12345678bc", //hanya untuk validasi
                    Roles = roles, //hanya untuk validasi
                    Username = "username", //hanya untuk validasi
                    StrStatus = "strstatus"//hanya untuk validasi
                };
                string err = ValidateDataUser(user);
                if (!string.IsNullOrEmpty(err)) return err;
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUser tbUser = conn.Get<TbUser>(IdUser);
                        if (tbUser == null) return "data tidak ada";

                        var cekEmail = (from a in conn.GetList<TbUser>()
                                        where a.Email == Email
                                        select a).FirstOrDefault();

                        if (cekEmail != null)
                        {
                            if (IdUser != cekEmail.IdUser)
                                return "email sudah terdaftar untuk username: " + cekEmail.Username;
                        }
                        tbUser.Email = Email;
                        tbUser.FirstName = FirstName;
                        tbUser.MiddleName = MiddleName;
                        tbUser.LastName = LastName;
                        tbUser.Address = Address;
                        tbUser.PhoneNumber = PhoneNumber;
                        tbUser.MobileNumber = MobileNumber;
                        conn.Update(tbUser);
                        if (FileImage != null)
                        {
                            string pathUpload = Path.Combine(AppSetting.PathFile, "ImgUsrProfile");
                            if (!Directory.Exists(pathUpload))
                                Directory.CreateDirectory(pathUpload);

                            if (tbUser.FileImage != null)
                            {
                                if (File.Exists(Path.Combine(pathUpload, tbUser.FileImage)))
                                {
                                    File.Delete(Path.Combine(pathUpload, tbUser.FileImage));
                                }
                            }

                            Random rnd = new Random();
                            var rr = rnd.Next(1, 13);
                            tbUser.FileImage = DateTime.Now.ToString("yyyyMMddHHmmss" + rr) + Path.GetExtension(FileImage.FileName);
                            using (var fileStream = new FileStream(Path.Combine(pathUpload, tbUser.FileImage), FileMode.Create, FileAccess.Write))
                            {
                                FileImage.CopyTo(fileStream);
                            }

                        }
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "UpdateProfile", ex);
            }

        }
        public string SetUserActive(int IdUser, int SetIdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUser tbUser = conn.Get<TbUser>(SetIdUser);
                        if (tbUser == null) { oMessage = "data tidak ada"; return null; }
                        if (tbUser.Status == StatusDataConstant.Aktif) { oMessage = "user sudah aktif"; return null; }
                        tbUser.Status = StatusDataConstant.Aktif;
                        tbUser.UpdatedBy = IdUser;
                        tbUser.UpdatedDate = commonService.GetCurrdate();
                        conn.Update(tbUser);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetUserActive", ex);
                return null;
            }
        }
        public string SetUserInActive(int IdUser, int SetIdUser, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbUser tbUser = conn.Get<TbUser>(SetIdUser);
                        if (tbUser == null) { oMessage = "data tidak ada"; return null; }
                        if (tbUser.Status == StatusDataConstant.TidakAktif) { oMessage = "user sudah tidak aktif"; return null; }

                        tbUser.Status = StatusDataConstant.TidakAktif;
                        tbUser.UpdatedBy = IdUser;
                        tbUser.UpdatedDate = commonService.GetCurrdate();
                        conn.Update(tbUser);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "SetUserInActive", ex);
                return null;
            }
        }
        #endregion
        #region AppTask
        public List<ApplModel> GetAppls(out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<ApplModel> ret = new List<ApplModel>();
                using (var conn = commonService.DbConnection())
                {
                    var tbAppls = conn.GetList<TbAppl>();
                    if (tbAppls == null || tbAppls.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var tbAppl in tbAppls.OrderBy(x => x.IdAppl))
                    {
                        ApplModel m = new ApplModel
                        {
                            IdAppl = tbAppl.IdAppl,
                            ApplName = tbAppl.ApplName,
                        };
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetAppls", ex);
                return null;
            }

        }
        public string AddAppl(int IdAppl, string ApplName)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbAppl tbAppl = new TbAppl { IdAppl = IdAppl, ApplName = ApplName };
                        conn.Insert(tbAppl);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddAppl", ex);
            }

        }
        public string EditAppl(int IdAppl, string ApplName)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbAppl tbAppl = conn.Get<TbAppl>(IdAppl);
                        if (tbAppl == null) return "data tidak ada";
                        tbAppl.ApplName = ApplName;
                        conn.Update(tbAppl);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditAppl", ex);
            }

        }

        public string AddApplTask(ApplTaskAddModel Data)
        {
            try
            {
                if (Data == null) return "data harus diisi";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {

                        TbApplTask tbApplTask = new TbApplTask();
                        if (Data.IdApplTaskParent == null)
                        {
                            tbApplTask.IdApplTask = (from a in conn.GetList<TbApplTask>()
                                                     where a.IdApplTaskParent == null
                                                     select a).Count() + 1;
                            tbApplTask.IdApplTask *= 1000000;
                        }
                        else
                        {
                            // if (Data.IdApplTaskParent.ToString().Length != 99) return "IdApplTaskParent salah";
                            var tbApplTasks = from a in conn.GetList<TbApplTask>()
                                              where a.IdApplTaskParent == Data.IdApplTaskParent
                                              select a;
                            if (tbApplTasks == null || tbApplTasks.Count() == 0)
                            {
                                var tbApplTaskParent = conn.Get<TbApplTask>(Data.IdApplTaskParent);
                                if (tbApplTaskParent == null) return "data parent tidak ada";

                                tbApplTask.IdApplTask = 1;
                            }
                            else
                            {
                                if (int.Parse(Data.IdApplTaskParent.ToString().Substring(6, 1)) > 0) return "level menu sudah habis";
                                tbApplTask.IdApplTask = tbApplTasks.Count() + 1;
                            }
                            if (int.Parse(Data.IdApplTaskParent.ToString().Substring(5, 1)) > 0)
                            {
                                tbApplTask.IdApplTask = Data.IdApplTaskParent.Value + tbApplTask.IdApplTask;
                            }
                            else if (int.Parse(Data.IdApplTaskParent.ToString().Substring(4, 1)) > 0)
                            {
                                tbApplTask.IdApplTask = Data.IdApplTaskParent.Value + tbApplTask.IdApplTask * 10;
                            }
                            else if (int.Parse(Data.IdApplTaskParent.ToString().Substring(2, 1)) > 0)
                            {
                                tbApplTask.IdApplTask = Data.IdApplTaskParent.Value + tbApplTask.IdApplTask * 100;
                            }
                            else
                            {
                                tbApplTask.IdApplTask = Data.IdApplTaskParent.Value + tbApplTask.IdApplTask * 10000;
                            }
                        }
                        tbApplTask.IdApplTaskParent = Data.IdApplTaskParent;
                        tbApplTask.ApplTaskName = Data.ApplTaskName;
                        tbApplTask.IdAppl = Data.IdAppl;
                        tbApplTask.ControllerName = Data.ControllerName;
                        tbApplTask.ActionName = Data.ActionName;
                        tbApplTask.Description = Data.Description;
                        tbApplTask.IconName = Data.IconName;
                        conn.Insert(tbApplTask);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddApplTask", ex);
            }

        }
        public string EditApplTask(ApplTaskEditModel Data)
        {
            try
            {
                if (Data == null) return "data harus diisi";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbApplTask tbApplTask = conn.Get<TbApplTask>(Data.IdApplTask);
                        if (tbApplTask == null) return "data tidak ada";
                        tbApplTask.ApplTaskName = Data.ApplTaskName;
                        tbApplTask.ControllerName = Data.ControllerName;
                        tbApplTask.ActionName = Data.ActionName;
                        tbApplTask.Description = Data.Description;
                        tbApplTask.IconName = Data.IconName;
                        conn.Update(tbApplTask);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditApplTask", ex);
            }

        }
        public string DeleteApplTask(int IdApplTask)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var tbApplTask = conn.Get<TbApplTask>(IdApplTask);
                        if (tbApplTask == null) return "data tidak ada";
                        conn.Delete(tbApplTask);
                        tx.Commit();
                        return string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteApplTask", ex);
            }

        }

        public ApplTaskRoleModel GetApplTaskByRole(int IdRole, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                ApplTaskRoleModel ret = new ApplTaskRoleModel();
                ret.AssignTasks = new List<ApplTaskModel>();
                ret.NoAssignTasks = new List<ApplTaskModel>();
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    var role = conn.Get<TbRole>(IdRole);
                    if (role == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdRole = IdRole;
                    ret.RoleName = role.RoleName;

                    var assignTasks = from a in conn.GetList<TbApplTask>()
                                      join b in conn.GetList<TbRoleApplTask>() on a.IdApplTask equals b.IdApplTask
                                      where (!string.IsNullOrEmpty(a.ControllerName) ? "ADA" : "") == "ADA"
                                      & b.IdRole == IdRole
                                      select a;
                    foreach (var item in assignTasks.OrderBy(x => x.IdApplTask))
                    {
                        ApplTaskModel m = new ApplTaskModel
                        {
                            ActionName = item.ActionName,
                            IdApplTask = item.IdApplTask,
                            ApplTaskName = item.ApplTaskName,
                            ControllerName = item.ControllerName,
                            Description = item.Description,
                            IconName = item.IconName,
                            IdAppl = item.IdAppl,
                            IdApplTaskParent = item.IdApplTaskParent
                        };
                        ret.AssignTasks.Add(m);
                    }
                    var noassignTasks = from a in conn.GetList<TbApplTask>()
                                        where (!string.IsNullOrEmpty(a.ControllerName) ? "ADA" : "") == "ADA"
                                        & !(from b in assignTasks
                                            select b.IdApplTask
                                               ).Contains(a.IdApplTask)
                                        select a;
                    foreach (var item in noassignTasks.OrderBy(x => x.IdApplTask))
                    {
                        ApplTaskModel m = new ApplTaskModel
                        {
                            ActionName = item.ActionName,
                            IdApplTask = item.IdApplTask,
                            ApplTaskName = item.ApplTaskName,
                            ControllerName = item.ControllerName,
                            Description = item.Description,
                            IconName = item.IconName,
                            IdAppl = item.IdAppl,
                            IdApplTaskParent = item.IdApplTaskParent
                        };
                        ret.NoAssignTasks.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetApplTaskRole", ex);
                return null;
            }

        }
        private void SetTaskParentRoleTask(IDbConnection conn, int IdRole, int? IdApplTask)
        {
            var tbApplTask = conn.Get<TbApplTask>(IdApplTask);
            Console.WriteLine("Masuk:");
            if (tbApplTask.IdApplTaskParent != null)
            {

                var tbRoleApplTask = (from a in conn.GetList<TbRoleApplTask>()
                                      where a.IdRole == IdRole & a.IdApplTask == tbApplTask.IdApplTaskParent.Value
                                      select a).FirstOrDefault();
                if (tbRoleApplTask == null)
                {
                    tbRoleApplTask = new TbRoleApplTask { IdRole = IdRole, IdApplTask = tbApplTask.IdApplTaskParent.Value };
                    conn.Insert(tbRoleApplTask);
                    Console.WriteLine("insert: " + tbRoleApplTask.IdApplTask);
                }
                SetTaskParentRoleTask(conn, IdRole, tbApplTask.IdApplTaskParent);
            }
        }
        public string AddTasksRole(int IdRole, List<int> IdApplTasks)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var oldTasksRole = from a in conn.GetList<TbRoleApplTask>()
                                           where a.IdRole == IdRole
                                           select a;
                        foreach (var item in oldTasksRole)
                        {
                            conn.Delete(item);
                        }
                        if (IdApplTasks != null & IdApplTasks.Count() > 0)
                        {
                            foreach (var item in IdApplTasks)
                            {
                                SetTaskParentRoleTask(conn, IdRole, item);
                                TbRoleApplTask tbRoleApplTask = new TbRoleApplTask { IdRole = IdRole, IdApplTask = item };
                                conn.Insert(tbRoleApplTask);
                            }
                        }
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddTasksRole", ex);
            }

        }

        public List<ApplTaskModel> GetMenus(int IdAppl, int IdUser, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var ret = (from a in conn.GetList<TbUserRole>()
                               join b in conn.GetList<TbRole>() on a.IdRole equals b.IdRole
                               join c in conn.GetList<TbRoleApplTask>() on a.IdRole equals c.IdRole
                               join d in conn.GetList<TbApplTask>() on c.IdApplTask equals d.IdApplTask
                               join e in conn.GetList<TbAppl>() on d.IdAppl equals e.IdAppl
                               where e.IdAppl == IdAppl
                               && a.IdUser == IdUser
                               select new ApplTaskModel
                               {
                                   IdApplTask = d.IdApplTask,
                                   ActionName = d.ActionName,
                                   ApplTaskName = d.ApplTaskName,
                                   ControllerName = d.ControllerName,
                                   Description = d.Description,
                                   IdAppl = d.IdAppl,
                                   IdApplTaskParent = d.IdApplTaskParent,
                                   IconName = d.IconName
                               }).OrderBy(x => x.IdApplTask).ToList();
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetMenus", ex);
                return null;
            }

        }
        public List<ApplTaskModel> GetMenus(int IdAppl, string Username, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var ret = (from a in conn.GetList<TbUserRole>()
                               join b in conn.GetList<TbRole>() on a.IdRole equals b.IdRole
                               join c in conn.GetList<TbRoleApplTask>() on a.IdRole equals c.IdRole
                               join d in conn.GetList<TbApplTask>() on c.IdApplTask equals d.IdApplTask
                               join e in conn.GetList<TbAppl>() on d.IdAppl equals e.IdAppl
                               join f in conn.GetList<TbUser>() on a.IdUser equals f.IdUser
                               where e.IdAppl == IdAppl
                               && f.Username == Username
                               select new ApplTaskModel
                               {
                                   IdApplTask = d.IdApplTask,
                                   ActionName = d.ActionName,
                                   ApplTaskName = d.ApplTaskName,
                                   ControllerName = d.ControllerName,
                                   Description = d.Description,
                                   IdAppl = d.IdAppl,
                                   IdApplTaskParent = d.IdApplTaskParent,
                                   IconName = d.IconName
                               }).OrderBy(x => x.IdApplTask).ToList();
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetMenus", ex);
                return null;
            }

        }
        public List<ApplTaskModel> GetApplTasks(int IdAppl, out string oMessage)
        {
            try
            {
                oMessage = string.Empty;
                List<ApplTaskModel> ret = new List<ApplTaskModel>();
                using (var conn = commonService.DbConnection())
                {
                    var tbApplTasks = (from a in conn.GetList<TbApplTask>()
                                       join b in conn.GetList<TbAppl>() on a.IdAppl equals b.IdAppl
                                       where a.IdAppl == IdAppl
                                       select new { a, b }).ToList();
                    if (tbApplTasks == null || tbApplTasks.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var tbApplTask in tbApplTasks.OrderBy(x => x.a.IdApplTask))
                    {
                        ApplTaskModel m = new ApplTaskModel
                        {
                            ApplName = tbApplTask.b.ApplName,
                            ActionName = tbApplTask.a.ActionName,
                            ApplTaskName = tbApplTask.a.ApplTaskName,
                            ControllerName = tbApplTask.a.ControllerName,
                            Description = tbApplTask.a.Description,
                            IconName = tbApplTask.a.IconName,
                            IdAppl = tbApplTask.a.IdAppl,
                            IdApplTask = tbApplTask.a.IdApplTask,
                            IdApplTaskParent = tbApplTask.a.IdApplTaskParent
                        };
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetApplTasks", ex);
                return null;
            }

        }

        #endregion
        private List<MenuChildModel> GetMenusByIdParent(List<ApplTaskModel> Data, int IdApplTaskParent)
        {
            List<MenuChildModel> ret = new List<MenuChildModel>();
            foreach (var item in Data.Where(x => x.IdApplTaskParent == IdApplTaskParent))
            {
                MenuChildModel m = new MenuChildModel();
                m._tag = "CSidebarNavItems";
                m.name = item.ApplTaskName;
                m.to = "/"+item.ControllerName+"/"+item.ActionName;
                m._children = null;
                
                var cek = from a in Data
                          where a.IdApplTaskParent == item.IdApplTask
                          select a;
                if (cek != null)
                {
                    var recursiveChild = GetMenusByIdParent(Data, item.IdApplTask);
                    //m._tag = "CSidebarNavItem";
                    m._tag = (GetMenusByIdParent(Data, item.IdApplTask).Count > 0) ? "CSidebarNavDropdown" : "CSidebarNavItem";
                    m._children = (GetMenusByIdParent(Data, item.IdApplTask).Count > 0) ? recursiveChild : null;
                }
                ret.Add(m);
            }
            return ret;
        }
        public List<MenuModel> GetMenus(int IdAppl, int? IdUser, string Username, out string oMessage)
        {
            oMessage = string.Empty;
            List<MenuModel> ret = new List<MenuModel>();
            try
            {
                using (IDbConnection conn = commonService.DbConnection())
                {
                    conn.Open();
                    var data = from a in conn.GetList<TbUserRole>()
                               join b in conn.GetList<TbRole>() on a.IdRole equals b.IdRole
                               join c in conn.GetList<TbRoleApplTask>() on a.IdRole equals c.IdRole
                               join d in conn.GetList<TbApplTask>() on c.IdApplTask equals d.IdApplTask
                               join e in conn.GetList<TbAppl>() on d.IdAppl equals e.IdAppl
                               join f in conn.GetList<TbUser>() on a.IdUser equals f.IdUser
                               where e.IdAppl == IdAppl
                               && f.IdUser == (IdUser == null ? f.IdUser : IdUser)
                               && f.Username == (string.IsNullOrEmpty(Username) ? f.Username : Username)
                               select new ApplTaskModel
                               {
                                   IdApplTask = d.IdApplTask,
                                   ActionName = d.ActionName,
                                   ApplTaskName = d.ApplTaskName,
                                   ControllerName = d.ControllerName,
                                   Description = d.Description,
                                   IdAppl = d.IdAppl,
                                   IdApplTaskParent = d.IdApplTaskParent,
                                   IconName = d.IconName
                               };
                    if (data == null) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in data.ToList().Where(x => x.IdApplTaskParent == null))
                    {
                        MenuModel m = new MenuModel
                        {
                            _tag = "CSidebarNavDropdown",
                            name = item.ApplTaskName,
                            route = item.ControllerName,
                            to = item.ActionName,
                            icon = item.IconName,
                            _children = null
                        };
                        m._children = GetMenusByIdParent(data.ToList(), item.IdApplTask);
                        ret.Add(m);
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetMenus", ex);
                return null;
            }
        }
    }
}
