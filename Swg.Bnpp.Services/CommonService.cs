﻿using Dapper;
using Npgsql;
using Swg.Bnpp.Models.UserApp;
using System;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using Swg.Bnpp.Entities.Mnv;
using Swg.Bnpp.Models.Constants;

namespace Swg.Bnpp.Services
{
    public interface ICommonService
    {
        IDbConnection DbConnection();
        void InitApplSetting();
        string GetErrorMessage(string MethodeName, Exception ex);
        string DecryptString(string cipherText);
        string EncryptString(string text);
        DateTime? StrToDateTime(string StrDate);
        DateTime GetCurrdate();
    }
    public class CommonService : ICommonService
    {
        private readonly string KeyString = AppSetting.Secret;
        private DateTime CurrDate = DateTime.Now;
        private void InitBobotValue(IDbConnection conn)
        {
            var bobotKondisis = conn.GetList<TbBobotKondisi>();
            BobotKondisiConstant.Dict = new System.Collections.Generic.Dictionary<int, string>();
            foreach (var item in bobotKondisis)
            {
                BobotKondisiConstant.Dict.Add(item.IdBobotKondisi, item.Nama);
                switch (item.Nama.Trim().Replace(" ", "").ToUpper())
                {
                    case "LAYAKATAS":
                        BobotKondisiConstant.LayakAtas = item.IdBobotKondisi;
                        break;
                    case "LAYAKBAWAH":
                        BobotKondisiConstant.LayakAtas = item.IdBobotKondisi;
                        break;
                    case "CUKUPLAYAKATAS":
                        BobotKondisiConstant.LayakAtas = item.IdBobotKondisi;
                        break;
                    case "CUKUPLAYAKBAWAH":
                        BobotKondisiConstant.LayakAtas = item.IdBobotKondisi;
                        break;
                    case "TIDAKLAYAKATAS":
                        BobotKondisiConstant.LayakAtas = item.IdBobotKondisi;
                        break;
                    case "TIDAKLAYAKBAWAH":
                        BobotKondisiConstant.LayakAtas = item.IdBobotKondisi;
                        break;
                    default:
                        break;
                }

            }
            var bobotKecukupan = conn.GetList<TbBobotKecukupan>();
            BobotKecukupanConstant.Dict = new System.Collections.Generic.Dictionary<int, string>();
            foreach (var item in bobotKecukupan)
            {
                BobotKecukupanConstant.Dict.Add(item.IdBobotKecukupan, item.Nama);
                switch (item.Nama.Trim().Replace(" ", "").ToUpper())
                {
                    case "CUKUP":
                        BobotKecukupanConstant.Cukup = item.IdBobotKecukupan;
                        break;
                    case "LUMAYANCUKUP":
                        BobotKecukupanConstant.LumayanCukup = item.IdBobotKecukupan;
                        break;
                    case "TIDAKCUKUP":
                        BobotKecukupanConstant.TidakCukup = item.IdBobotKecukupan;
                        break;
                    default:
                        break;
                }
            }
            var bobotKetersediaan = conn.GetList<TbBobotKetersediaan>();
            BobotKetersediaanConstant.Dict = new System.Collections.Generic.Dictionary<int, string>();
            foreach (var item in bobotKetersediaan)
            {
                BobotKetersediaanConstant.Dict.Add(item.IdBobotKetersediaan, item.Nama);
                switch (item.Nama.Trim().Replace(" ", "").ToUpper())
                {
                    case "CUKUP":
                        BobotKetersediaanConstant.Ya = item.IdBobotKetersediaan;
                        break;
                    case "LUMAYANCUKUP":
                        BobotKetersediaanConstant.Tidak = item.IdBobotKetersediaan;
                        break;
                    default:
                        break;
                }
            }
        }
        public IDbConnection DbConnection()
        {
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.PostgreSQL);
            return new NpgsqlConnection(AppSetting.ConnectionString);
        }
        public void InitApplSetting()
        {
            if (!Directory.Exists(AppSetting.PathFile))
                Directory.CreateDirectory(AppSetting.PathFile);
            using(var conn = DbConnection())
            {
                this.InitBobotValue(conn);
            }
        }
        public string GetErrorMessage(string MethodeName, Exception ex)
        {
            string errMessage = ex.Message;
            if (ex.InnerException != null)
            {
                errMessage = ex.InnerException.Message;
                if (ex.InnerException.InnerException != null)
                {
                    errMessage = ex.InnerException.InnerException.Message;
                    if (ex.InnerException.InnerException.InnerException != null)
                    {
                        errMessage = ex.InnerException.InnerException.InnerException.Message;
                    }
                }
            }
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return MethodeName + ", line: " + lineNumber + Environment.NewLine + "Error Message: " + errMessage;
        }
        public string DecryptString(string cipherText)
        {
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[16];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, iv.Length);
            var key = Encoding.UTF8.GetBytes(KeyString);

            using (var aesAlg = Aes.Create())
            {
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }
        public string EncryptString(string text)
        {
            var key = Encoding.UTF8.GetBytes(KeyString);

            using (var aesAlg = Aes.Create())
            {
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(text);
                        }

                        var iv = aesAlg.IV;

                        var decryptedContent = msEncrypt.ToArray();

                        var result = new byte[iv.Length + decryptedContent.Length];

                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);
                        string tempEncrypt = Convert.ToBase64String(result);
                        string tempDecrypt = DecryptString(tempEncrypt);
                        if (tempDecrypt == text)
                            return tempEncrypt;
                        return string.Empty;
                    }
                }
            }
        }
        public DateTime? StrToDateTime(string StrDate)
        {
            try
            {
                if (StrDate.Length == 10)
                {
                    return DateTime.ParseExact(StrDate.Replace("/", "-"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    return DateTime.ParseExact(StrDate.Replace("/", "-"), "dd-MM-yyyy HH24:mm:ss", CultureInfo.InvariantCulture);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DateTime GetCurrdate()
        {
            return CurrDate;
        }
    }
}
