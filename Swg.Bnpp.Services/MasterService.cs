﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swg.Bnpp.Models.Master;
using Dapper;
using Swg.Bnpp.Entities.Mnv;
using Swg.Bnpp.Entities.Public;
using Swg.Bnpp.Entities.Wil;
using Swg.Bnpp.Entities.Org;
using Swg.Bnpp.Models.UserApp;

namespace Swg.Bnpp.Services
{
    public interface IMasterService
    {
        List<ProvinsiModel> GetProvinsis(out string oMessage);
        ProvinsiModel GetProvinsi(int IdProvinsi, out string oMessage);
        string AddProvinsi(string Kode, string Nama);
        string EditProvinsi(ProvinsiModel Data);
        string DeleteProvinsi(int IdProvinsi);

        List<KabupatenModel> GetKabupatens(int IdProvinsi, out string oMessage);
        KabupatenModel GetKabupaten(int IdKabupaten, out string oMessage);
        string AddKabupaten(int IdProvinsi, string Kode, string Nama);
        string EditKabupaten(KabupatenModel Data);
        string DeleteKabupaten(int IdKabupaten);

        List<KecamatanEditModel> GetKecamatans(int IdKabupaten, out string oMessage);
        KecamatanEditModel GetKecamatan(int IdKecamatan, out string oMessage);
        string AddKecamatan(int IdKabupaten, string Kode, string Nama, string Alamat, double? Longitude, double? Latitude);
        string EditKecamatan(KecamatanEditModel Data);
        string DeleteKecamatan(int IdKecamatan);

        //jabatan
        List<JabatanModel> GetJabatans(out string oMessage);
        JabatanModel GetJabatan(int IdJabatan, out string oMessage);
        string AddJabatan(string Kode, string Nama);
        string EditJabatan(JabatanModel Data);
        string DeleteJabatan(int IdJabatan);
        //pegawai
        List<PegawaiListModel> GetPegawaiKecamatans(out string oMessage);
        List<PegawaiListModel> GetPegawaiKecamatansByKecamatan(int IdKecamatan, out string oMessage);
        List<PegawaiListModel> GetPegawaiKecamatansByJabatan(int IdJabatan,int IdKecamatan, out string oMessage);
        PegawaiJabatanModel GetPegawaiKecamatan(int IdPegawai, out string oMessage);
        string AddPegawaiKecamatan(int IdUser, UserAddModel DataUser, int IdJabatan, int IdKecamatan);
        string EditPegawaiKecamatan(int IdUser, UserModel DataUser, int IdJabatan, int IdKecamatan);
        //bobot
        List<BobotModel> GetBobotKecukupans(out string oMessage);
        BobotModel GetBobotKecukupan(int Id, out string oMessage);
        string AddBobotKecukupan(int IdUser, string Nama, double Bobot);
        string EditBobotKecukupan(int IdUser, BobotModel Data);
        string DeleteBobotKecukupan(int IdUser, int Id);

        List<BobotModel> GetBobotKetersediaans(out string oMessage);
        BobotModel GetBobotKetersediaan(int Id, out string oMessage);
        string AddBobotKetersediaan(int IdUser, string Nama, double Bobot);
        string EditBobotKetersediaan(int IdUser, BobotModel Data);
        string DeleteBobotKetersediaan(int IdUser, int Id);

        List<BobotModel> GetBobotKondisis(out string oMessage);
        BobotModel GetBobotKondisi(int Id, out string oMessage);
        string AddBobotKondisi(int IdUser, string Nama, double Bobot);
        string EditBobotKondisi(int IdUser, BobotModel Data);
        string DeleteBobotKondisi(int IdUser, int Id);

        List<JenisLokpriModel> GetJenisLokpris(out string oMessage);

        //variabel
        List<VariabelListModel> GetVariabels(out string oMessage);
        VariabelModel GetVariabel(int IdVariabel, out string oMessage);
        string AddVariabel(int IdUser, int IdJenisVariabel, string Nama, int Presentase);
        string EditVariabel(int IdUser, VariabelModel Data);
        string DeleteVariabel(int IdUser, int IdVariabel);

        // jenis variabel
        List<JenisVariabelModel> GetJenisVariabels(out string oMessage);

        //Pertanyaan
        List<PertanyaanModel> GetPertanyaans(out string oMessage);
        PertanyaanModel GetPertanyaan(int IdPertanyaan, out string oMessage);
        string AddPertanyaan(int IdUser, string Pertanyaan, double? BobotDefault);
        string EditPertanyaan(int IdUser, PertanyaanModel Data);
        string DeletePertanyaan(int IdUser, int IdPertanyaan);
        //Indikator
        List<IndikatorListModel> GetIndikators(out string oMessage);
        IndikatorVariabelModel GetIndikatorByIdVariabels(int IdVariabel, out string oMessage);
        IndikatorModel GetIndikator(int IdIndikator, out string oMessage);
        string AddIndikator(int IdUser, int IdVariabel, string Nama, int Type);
        string EditIndikator(int IdUser, IndikatorModel Data);
        string DeleteIndikator(int IdUser, int IdIndikator);

        //Indikator Pertanyaan
        List<IndikatorPertanyaanModel> GetIndikatorPertanyaans(out string oMessage);
        List<IndikatorPertanyaanModel> GetIndikatorPertanyaanByIdIndikator(int IdIndikator, out string oMessage);
        IndikatorPertanyaanModel GetIndikatorPertanyaan(int IdIndikatorPertanyaan, out string oMessage);
        string AddIndikatorPertanyaan(int IdUser, int IdIndikator, int IdJenisLokpri, int IdPertanyaan);
        string AddIndikatorPertanyaan(int IdUser, int IdIndikator, int IdJenisLokpri, List<int> IdPertanyaan);
        string DeleteIndikatorPertanyaan(int IdUser, int IdIndikatorPertanyaan);
    }

    public class MasterService : IMasterService
    {
        private readonly string ServiceName = "MasterService.";
        private readonly ICommonService commonService;
        private readonly IUserAppService userAppService;
        public MasterService(ICommonService CommonService, IUserAppService UserAppService)
        {
            commonService = CommonService;
            userAppService = UserAppService;
        }
        #region wilayah administrasi
        public List<ProvinsiModel> GetProvinsis(out string oMessage)
        {
            oMessage = string.Empty;
            List<ProvinsiModel> ret = new List<ProvinsiModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbProvinsi>();
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        ProvinsiModel m = new ProvinsiModel();
                        m.IdProvinsi = item.IdProvinsi;
                        m.Kode = item.Kode;
                        m.Nama = item.Nama;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetProvinsis", ex);
                return null;
            }
        }
        public ProvinsiModel GetProvinsi(int IdProvinsi, out string oMessage)
        {
            oMessage = string.Empty;
            ProvinsiModel ret = new ProvinsiModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbProvinsi>(IdProvinsi);
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdProvinsi = item.IdProvinsi;
                    ret.Kode = item.Kode;
                    ret.Nama = item.Nama;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetProvinsi", ex);
                return null;
            }
        }
        public string AddProvinsi(string Kode, string Nama)
        {
            try
            {
                if (string.IsNullOrEmpty(Kode)) return "kode tidak boleh kosong";
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbProvinsi tb = new TbProvinsi
                        {
                            Kode = Kode,
                            Nama = Nama
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddProvinsi", ex);
            }
        }
        public string EditProvinsi(ProvinsiModel Data)
        {
            try
            {
                if (string.IsNullOrEmpty(Data.Kode)) return "kode tidak boleh kosong";
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbProvinsi tb = conn.Get<TbProvinsi>(Data.IdProvinsi);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.Kode = Data.Kode;
                        tb.Nama = Data.Nama;
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditProvinsi", ex);
            }
        }
        public string DeleteProvinsi(int IdProvinsi)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbProvinsi tb = conn.Get<TbProvinsi>(IdProvinsi);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteProvinsi", ex);
            }
        }
        public List<KabupatenModel> GetKabupatens(int IdProvinsi, out string oMessage)
        {
            oMessage = string.Empty;
            List<KabupatenModel> ret = new List<KabupatenModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbKabupaten>()
                               where a.IdProvinsi == IdProvinsi
                               select a;
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        KabupatenModel m = new KabupatenModel();
                        m.IdKabupaten = item.IdKabupaten;
                        m.IdProvinsi = item.IdProvinsi;
                        m.Kode = item.Kode;
                        m.Nama = item.Nama;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKabupatens", ex);
                return null;
            }
        }
        public KabupatenModel GetKabupaten(int IdKabupaten, out string oMessage)
        {
            oMessage = string.Empty;
            KabupatenModel ret = new KabupatenModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbKabupaten>(IdKabupaten);
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdProvinsi = item.IdProvinsi;
                    ret.IdKabupaten = item.IdKabupaten;
                    ret.Kode = item.Kode;
                    ret.Nama = item.Nama;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKabupaten", ex);
                return null;
            }
        }
        public string AddKabupaten(int IdProvinsi, string Kode, string Nama)
        {
            try
            {
                if (string.IsNullOrEmpty(Kode)) return "kode tidak boleh kosong";
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKabupaten tb = new TbKabupaten
                        {
                            IdProvinsi = IdProvinsi,
                            Kode = Kode,
                            Nama = Nama
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddKabupaten", ex);
            }
        }
        public string EditKabupaten(KabupatenModel Data)
        {
            try
            {
                if (string.IsNullOrEmpty(Data.Kode)) return "kode tidak boleh kosong";
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKabupaten tb = conn.Get<TbKabupaten>(Data.IdKabupaten);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.IdProvinsi = Data.IdProvinsi;
                        tb.Kode = Data.Kode;
                        tb.Nama = Data.Nama;
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditKabupaten", ex);
            }
        }
        public string DeleteKabupaten(int IdKabupaten)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKabupaten tb = conn.Get<TbKabupaten>(IdKabupaten);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteKabupaten", ex);
            }
        }

        private string QueryKecamatanByIdKabupaten(int IdKabupaten)
        {
            string sql = "";
            sql += " select a.id_kecamatan as IdKecamatan,a.id_kabupaten as IdKabupaten,a.kode,a.nama, b.alamat,b.longitude,b.latitude";
            sql += " from wil.tb_kecamatan a";
            sql += " inner join org.tb_kecamatan_profil b on a.id_kecamatan = b.id_kecamatan";
            sql += " where a.id_kabupaten =" + IdKabupaten;

            return sql;
        }
        public List<KecamatanEditModel> GetKecamatans(int IdKabupaten, out string oMessage)
        {
            oMessage = string.Empty;
            List<KecamatanEditModel> ret = new List<KecamatanEditModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    //var data = from a in conn.GetList<TbKecamatan>()
                    //           join b in conn.GetList<TbKecamatanProfil>() on a.IdKecamatan equals b.IdKecamatan
                    //           where a.IdKabupaten == IdKabupaten
                    //           select new { a, b };
                    string query = this.QueryKecamatanByIdKabupaten(IdKabupaten);
                    var data = conn.Query<KecamatanEditModel>(query);

                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        KecamatanEditModel m = new KecamatanEditModel();
                        m.IdKecamatan = item.IdKecamatan;
                        m.IdKabupaten = item.IdKabupaten;
                        m.Kode = item.Kode;
                        m.Nama = item.Nama;
                        m.Alamat = item.Alamat;
                        m.Longitude = item.Longitude;
                        m.Latitude = item.Latitude;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKecamatans", ex);
                return null;
            }
        }

        private string QueryKecamatanByIdKecamatan (int IdKecamatan)
        {
            string sql = "";
            sql += " select a.id_kecamatan as IdKecamatan,a.id_kabupaten as IdKabupaten,a.kode,a.nama, b.alamat,b.longitude,b.latitude";
            sql += " from wil.tb_kecamatan a";
            sql += " inner join org.tb_kecamatan_profil b on a.id_kecamatan = b.id_kecamatan";
            sql += " where a.id_kecamatan =" + IdKecamatan;

            return sql;
        }
        public KecamatanEditModel GetKecamatan(int IdKecamatan, out string oMessage)
        {
            oMessage = string.Empty;
            KecamatanEditModel ret = new KecamatanEditModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    //var item = (from a in conn.GetList<TbKecamatan>()
                    //           join b in conn.GetList<TbKecamatanProfil>() on a.IdKecamatan equals b.IdKecamatan
                    //           where a.IdKabupaten == IdKecamatan
                    //           select new { a, b }).FirstOrDefault();
                    var Query = this.QueryKecamatanByIdKecamatan(IdKecamatan);
                    var item = conn.Query<KecamatanEditModel>(Query).FirstOrDefault();
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdKecamatan = item.IdKecamatan;
                    ret.IdKabupaten = item.IdKabupaten;
                    ret.Kode = item.Kode;
                    ret.Nama = item.Nama;
                    ret.Alamat = item.Alamat;
                    ret.Longitude = item.Longitude;
                    ret.Latitude = item.Latitude;

                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetKecamatan", ex);
                return null;
            }
        }
        public string AddKecamatan(int IdKabupaten, string Kode, string Nama, string Alamat, double? Longitude, double? Latitude)
        {
            try
            {
                if (string.IsNullOrEmpty(Kode)) return "kode tidak boleh kosong";
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKecamatan tb = new TbKecamatan
                        {
                            IdKabupaten = IdKabupaten,
                            Kode = Kode,
                            Nama = Nama
                        };
                        var idKecamatan = conn.Insert(tb);
                        TbKecamatanProfil tbKecamatanProfil = new TbKecamatanProfil
                        {
                            Alamat = Alamat,
                            IdKecamatan = (int)idKecamatan,
                            Latitude = Latitude,
                            Longitute = Longitude
                        };
                        conn.Insert(tbKecamatanProfil);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddKecamatan", ex);
            }
        }
        private string QueryKecamatanProfilByIdKecamatan (int IdKecamatan)
        {
            string sql = "";
            sql += " select alamat as Alamat, longitude as Longitude, latitude as Latitude";
            sql += " from";
            sql += " org.tb_kecamatan_profil where id_kecamatan = " + IdKecamatan;
            return sql;
        }
        public string EditKecamatan(KecamatanEditModel Data)
        {
            try
            {
                if (string.IsNullOrEmpty(Data.Kode)) return "kode tidak boleh kosong";
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKecamatan tb = conn.Get<TbKecamatan>(Data.IdKecamatan);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.IdKabupaten = Data.IdKabupaten;
                        tb.Kode = Data.Kode;
                        tb.Nama = Data.Nama;
                        
                        conn.Update(tb);
                        string sqlProfilKecByIdKec = this.QueryKecamatanProfilByIdKecamatan(Data.IdKecamatan);
                        var tbKecamatanProfil = conn.Query<TbKecamatanProfil> (sqlProfilKecByIdKec).FirstOrDefault();
                        //TbKecamatanProfil tbKecamatanProfil = conn.Get<TbKecamatanProfil>(Data.IdKecamatan);
                        tbKecamatanProfil.Alamat = Data.Alamat;
                        tbKecamatanProfil.Longitute = Data.Longitude;
                        tbKecamatanProfil.Latitude = Data.Latitude;
                        conn.Update(tbKecamatanProfil);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditKecamatan", ex);
            }
        }
        public string DeleteKecamatan(int IdKecamatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbKecamatan tb = conn.Get<TbKecamatan>(IdKecamatan);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteKecamatan", ex);
            }
        }
        #endregion
        #region jabatan
        public List<JabatanModel> GetJabatans(out string oMessage)
        {
            oMessage = string.Empty;
            List<JabatanModel> ret = new List<JabatanModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJabatan>();
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        JabatanModel m = new JabatanModel();
                        m.IdJabatan = item.IdJabatan;
                        m.Kode = item.Kode;
                        m.Nama = item.Nama;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJabatans", ex);
                return null;
            }

        }
        public JabatanModel GetJabatan(int IdJabatan, out string oMessage)
        {
            oMessage = string.Empty;
            JabatanModel ret = new JabatanModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbJabatan>(IdJabatan);
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdJabatan = item.IdJabatan;
                    ret.Kode = item.Kode;
                    ret.Nama = item.Nama;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJabatan", ex);
                return null;
            }
        }
        public string AddJabatan(string Kode, string Nama)
        {
            try
            {
                if (string.IsNullOrEmpty(Kode)) return "kode tidak boleh kosong";
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJabatan tb = new TbJabatan
                        {
                            Kode = Kode,
                            Nama = Nama
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddJabatan", ex);
            }

        }
        public string EditJabatan(JabatanModel Data)
        {
            try
            {
                if (string.IsNullOrEmpty(Data.Kode)) return "kode tidak boleh kosong";
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJabatan tb = conn.Get<TbJabatan>(Data.IdJabatan);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.Kode = Data.Kode;
                        tb.Nama = Data.Nama;
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditJabatan", ex);
            }
        }
        public string DeleteJabatan(int IdJabatan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbJabatan tb = conn.Get<TbJabatan>(IdJabatan);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteJabatan", ex);
            }
        }
        #endregion
        #region pegawai
        private List<PegawaiListModel> GetPegawais(int? IdKecamatan, int? IdJabaran, out string oMessage)
        {
            oMessage = string.Empty;
            List<PegawaiListModel> ret = new List<PegawaiListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbPegawaiKecamatan>()
                               join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                               join c in conn.GetList<TbJabatan>() on a.IdJabatan equals c.IdJabatan
                               join d in conn.GetList<TbKecamatanProfil>() on a.IdKecamatan equals d.IdKecamatan
                               join e in conn.GetList<TbKecamatan>() on a.IdKecamatan equals e.IdKecamatan
                               join f in conn.GetList<TbKabupaten>() on e.IdKabupaten equals f.IdKabupaten
                               join g in conn.GetList<TbProvinsi>() on f.IdProvinsi equals g.IdProvinsi
                               where a.IdKecamatan == (IdKecamatan == null ? a.IdKecamatan : IdKecamatan)
                               & a.IdJabatan == (IdJabaran == null ? a.IdJabatan : IdJabaran)
                               select new
                               {
                                   a,
                                   FirstName = b.FirstName,
                                   MiddleName = b.MiddleName,
                                   LastName = b.LastName,
                                   Jabatan = c.Nama,
                                   Kecamatan = e.Nama,
                                   Kabupaten = f.Nama,
                                   Provinsi = g.Nama,
                                   Email = b.Email,
                                   Alamat = d.Alamat,
                                   MobileNumber = b.MobileNumber
                                   
                               };
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        PegawaiListModel m = new PegawaiListModel();
                        m.IdPegawai = item.a.IdPegawai;
                        m.FullName = item.FirstName;
                        if (!string.IsNullOrEmpty(item.MiddleName))
                            m.FullName += " " + item.MiddleName;
                        if (!string.IsNullOrEmpty(item.LastName))
                            m.FullName += " " + item.LastName;
                        m.IdJabatan = item.a.IdJabatan;
                        m.Jabatan = item.Jabatan;
                        m.Email = item.Email;
                        m.MobileNumber = item.MobileNumber;
                        m.AlamatKantor = item.Alamat;
                        m.Kecamatan = item.Kecamatan;
                        m.Kabupaten = item.Kabupaten;
                        m.Provinsi = item.Provinsi;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawais", ex);
                return null;
            }

        }
        public List<PegawaiListModel> GetPegawaiKecamatans(out string oMessage)
        {
            return GetPegawais(null, null, out oMessage);
        }
        public List<PegawaiListModel> GetPegawaiKecamatansByKecamatan(int IdKecamatan, out string oMessage)
        {
            return GetPegawais(IdKecamatan, null, out oMessage);
        }
        public List<PegawaiListModel> GetPegawaiKecamatansByJabatan(int IdJabatan,int IdKecamatan, out string oMessage)
        {
            return GetPegawais(IdKecamatan, IdJabatan, out oMessage);
        }
        public PegawaiJabatanModel GetPegawaiKecamatan(int IdPegawai, out string oMessage)
        {
            oMessage = string.Empty;
            PegawaiJabatanModel ret = new PegawaiJabatanModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = (from a in conn.GetList<TbPegawaiKecamatan>()
                                join b in conn.GetList<TbUser>() on a.IdPegawai equals b.IdUser
                                join c in conn.GetList<TbKecamatanProfil>() on a.IdKecamatan equals c.IdKecamatan
                                join d in conn.GetList<TbKecamatan>() on c.IdKecamatan equals d.IdKecamatan
                                join e in conn.GetList<TbKabupaten>() on d.IdKabupaten equals e.IdKabupaten
                                where a.IdPegawai == IdPegawai
                                select new { a, b, c, d, e }).FirstOrDefault();
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdUser = item.b.IdUser;
                    ret.Username = item.b.Username;
                    ret.Password = "";
                    ret.Email = item.b.Email;
                    ret.FirstName = item.b.FirstName;
                    ret.MiddleName = item.b.MiddleName;
                    ret.LastName = item.b.LastName;
                    ret.Address = item.b.Address;
                    ret.PhoneNumber = item.b.PhoneNumber;
                    ret.MobileNumber = item.b.MobileNumber;
                    ret.FileImage = item.b.FirstName;
                    ret.IdJabatan = item.a.IdJabatan;
                    ret.IdKecamatan = item.a.IdKecamatan;
                    ret.OfficeAddress = item.c.Alamat;
                    ret.IdKabupaten = item.e.IdKabupaten;
                    ret.IdProvinsi = item.e.IdProvinsi;

                    string fullnameTmp = item.b.FirstName;
                    if (!string.IsNullOrEmpty(item.b.MiddleName))
                        fullnameTmp += " " + item.b.MiddleName;
                    if (!string.IsNullOrEmpty(item.b.LastName))
                        fullnameTmp += " " + item.b.LastName;
                    ret.FullName = fullnameTmp;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPegawaiKecamatan", ex);
                return null;
            }

        }
        public string AddPegawaiKecamatan(int IdUser, UserAddModel DataUser, int IdJabatan, int IdKecamatan)
        {
            try
            {
                if (IdJabatan <= 0) return "jabatan harus dipilih";
                if (IdKecamatan <= 0) return "kecamatan harus dipilih";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {

                        string err = userAppService.AddUser(conn, IdUser, DataUser, out int oIdUser);
                        if (!string.IsNullOrEmpty(err)) return err;

                        TbPegawaiKecamatan tb = new TbPegawaiKecamatan
                        {
                            IdJabatan = IdJabatan,
                            IdKecamatan = IdKecamatan,
                            IdPegawai = oIdUser
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddPegawaiKecamatan", ex);
            }
        }
        public string EditPegawaiKecamatan(int IdUser, UserModel DataUser, int IdJabatan, int IdKecamatan)
        {
            try
            {
                if (IdJabatan <= 0) return "jabatan harus dipilih";
                if (IdKecamatan <= 0) return "kecamatan harus dipilih";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        string err = userAppService.EditUser(conn, IdUser, DataUser);
                        if (!string.IsNullOrEmpty(err)) return err;


                        TbPegawaiKecamatan tb = conn.Get<TbPegawaiKecamatan>(DataUser.IdUser);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.IdJabatan = IdJabatan;
                        tb.IdKecamatan = IdKecamatan;
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditPegawaiKecamatan", ex);
            }
        }
        #endregion
        #region bobot
        public List<BobotModel> GetBobotKecukupans(out string oMessage)
        {
            oMessage = string.Empty;
            List<BobotModel> ret = new List<BobotModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbBobotKecukupan>();
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        BobotModel m = new BobotModel();
                        m.Id = item.IdBobotKecukupan;
                        m.Nama = item.Nama;
                        m.Bobot = item.Bobot;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetBobotKecukupans", ex);
                return null;
            }
        }
        public BobotModel GetBobotKecukupan(int Id, out string oMessage)
        {
            oMessage = string.Empty;
            BobotModel ret = new BobotModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbBobotKecukupan>(Id);
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.Id = item.IdBobotKecukupan;
                    ret.Nama = item.Nama;
                    ret.Bobot = item.Bobot;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetBobotKecukupan", ex);
                return null;
            }
        }
        public string AddBobotKecukupan(int IdUser, string Nama, double Bobot)
        {
            try
            {
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                if (Bobot < 0 || Bobot > 100) return "bobot salah";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKecukupan tb = new TbBobotKecukupan
                        {
                            Nama = Nama,
                            Bobot = Bobot,
                            Status = 1,
                            CreatedBy = IdUser,
                            CreatedDate = commonService.GetCurrdate()
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddBobotKecukupan", ex);
            }
        }
        public string EditBobotKecukupan(int IdUser, BobotModel Data)
        {
            try
            {
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                if (Data.Bobot < 0 || Data.Bobot > 100) return "bobot salah";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKecukupan tb = conn.Get<TbBobotKecukupan>(Data.Id);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.Nama = Data.Nama;
                        tb.Bobot = Data.Bobot;
                        tb.UpdatedBy = IdUser;
                        tb.UpdatedDate = commonService.GetCurrdate();
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditBobotKecukupan", ex);
            }
        }
        public string DeleteBobotKecukupan(int IdUser, int Id)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKecukupan tb = conn.Get<TbBobotKecukupan>(Id);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteBobotKecukupan", ex);
            }
        }

        public List<BobotModel> GetBobotKetersediaans(out string oMessage)
        {
            oMessage = string.Empty;
            List<BobotModel> ret = new List<BobotModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbBobotKetersediaan>();
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        BobotModel m = new BobotModel();
                        m.Id = item.IdBobotKetersediaan;
                        m.Nama = item.Nama;
                        m.Bobot = item.Bobot;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetBobotKetersediaans", ex);
                return null;
            }
        }
        public BobotModel GetBobotKetersediaan(int Id, out string oMessage)
        {
            oMessage = string.Empty;
            BobotModel ret = new BobotModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbBobotKetersediaan>(Id);
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.Id = item.IdBobotKetersediaan;
                    ret.Nama = item.Nama;
                    ret.Bobot = item.Bobot;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetBobotKetersediaan", ex);
                return null;
            }
        }
        public string AddBobotKetersediaan(int IdUser, string Nama, double Bobot)
        {
            try
            {
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                if (Bobot < 0 || Bobot > 100) return "bobot salah";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKetersediaan tb = new TbBobotKetersediaan
                        {
                            Nama = Nama,
                            Bobot = Bobot,
                            Status = 1,
                            CreatedBy = IdUser,
                            CreatedDate = commonService.GetCurrdate()
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddBobotKetersediaan", ex);
            }
        }
        public string EditBobotKetersediaan(int IdUser, BobotModel Data)
        {
            try
            {
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                if (Data.Bobot < 0 || Data.Bobot > 100) return "bobot salah";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKetersediaan tb = conn.Get<TbBobotKetersediaan>(Data.Id);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.Nama = Data.Nama;
                        tb.Bobot = Data.Bobot;
                        tb.UpdatedBy = IdUser;
                        tb.UpdatedDate = commonService.GetCurrdate();
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditBobotKetersediaan", ex);
            }
        }
        public string DeleteBobotKetersediaan(int IdUser, int Id)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKetersediaan tb = conn.Get<TbBobotKetersediaan>(Id);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteBobotKetersediaan", ex);
            }
        }

        public List<BobotModel> GetBobotKondisis(out string oMessage)
        {
            oMessage = string.Empty;
            List<BobotModel> ret = new List<BobotModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbBobotKondisi>();
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        BobotModel m = new BobotModel();
                        m.Id = item.IdBobotKondisi;
                        m.Nama = item.Nama;
                        m.Bobot = item.Bobot;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetBobotKondisis", ex);
                return null;
            }
        }
        public BobotModel GetBobotKondisi(int Id, out string oMessage)
        {
            oMessage = string.Empty;
            BobotModel ret = new BobotModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbBobotKondisi>(Id);
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.Id = item.IdBobotKondisi;
                    ret.Nama = item.Nama;
                    ret.Bobot = item.Bobot;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetBobotKondisi", ex);
                return null;
            }
        }
        public string AddBobotKondisi(int IdUser, string Nama, double Bobot)
        {
            try
            {
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                if (Bobot < 0 || Bobot > 100) return "bobot salah";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKondisi tb = new TbBobotKondisi
                        {
                            Nama = Nama,
                            Bobot = Bobot,
                            Status = 1,
                            CreatedBy = IdUser,
                            CreatedDate = commonService.GetCurrdate()
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddBobotKondisi", ex);
            }
        }
        public string EditBobotKondisi(int IdUser, BobotModel Data)
        {
            try
            {
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                if (Data.Bobot < 0 || Data.Bobot > 100) return "bobot salah";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKondisi tb = conn.Get<TbBobotKondisi>(Data.Id);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.Nama = Data.Nama;
                        tb.Bobot = Data.Bobot;
                        tb.UpdatedBy = IdUser;
                        tb.UpdatedDate = commonService.GetCurrdate();
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditBobotKondisi", ex);
            }
        }
        public string DeleteBobotKondisi(int IdUser, int Id)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbBobotKondisi tb = conn.Get<TbBobotKondisi>(Id);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteBobotKondisi", ex);
            }
        }


        #endregion
        public List<JenisLokpriModel> GetJenisLokpris(out string oMessage)
        {
            oMessage = string.Empty;
            List<JenisLokpriModel> ret = new List<JenisLokpriModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = conn.GetList<TbJenisLokpri>();
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        JenisLokpriModel m = new JenisLokpriModel();
                        m.IdJenisLokpri = item.IdJenisLokpri;
                        m.Nama = item.Nama;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisLokpris", ex);
                return null;
            }

        }
        #region variabel
        public List<VariabelListModel> GetVariabels(out string oMessage)
        {
            oMessage = string.Empty;
            List<VariabelListModel> ret = new List<VariabelListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbVariabel>()
                               join b in conn.GetList<TbJenisVariabel>() on a.IdJenisVariabel equals b.IdJenisVariabel
                               select new { a, b };
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        VariabelListModel m = new VariabelListModel();
                        m.IdVariabel = item.a.IdVariabel;
                        m.JenisVariabel = item.b.Nama;
                        m.Nama = item.a.Nama;
                        m.Presentase = item.a.Presentase;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetVariabels", ex);
                return null;
            }

        }
        public VariabelModel GetVariabel(int IdVariabel, out string oMessage) {
            oMessage = string.Empty;
            VariabelModel ret = new VariabelModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var item = conn.Get<TbVariabel>(IdVariabel);
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdVariabel = item.IdVariabel;
                    ret.IdJenisVariabel = item.IdJenisVariabel;
                    ret.Nama = item.Nama;
                    ret.Presentase = item.Presentase;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetVariabel", ex);
                return null;
            }
        }
        public string AddVariabel(int IdUser, int IdJenisVariabel, string Nama, int Presentase) {
            try
            {
                if (IdJenisVariabel <= 0) return "jenis variabel harus dipilih";
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                if (Presentase <= 0) return "presentase harus lebih besar dari nol";
                if (Presentase >100 ) return "presentase harus lebih kecil dari 100";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbVariabel tb = new TbVariabel
                        {
                            IdJenisVariabel = IdJenisVariabel,
                            Nama = Nama,
                            Presentase = Presentase,
                            CreatedBy = IdUser,
                            CreatedDate = commonService.GetCurrdate()
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddVariabel", ex);
            }
        }
        public string EditVariabel(int IdUser, VariabelModel Data)
        {
            try
            {
                if (Data.IdJenisVariabel <= 0) return "jenis variabel harus dipilih";
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                if (Data.Presentase <= 0) return "presentase harus lebih besar dari nol";
                if (Data.Presentase > 100) return "presentase harus lebih kecil dari 100";
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbVariabel tb = conn.Get<TbVariabel>(Data.IdVariabel);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.IdJenisVariabel = Data.IdJenisVariabel;
                        tb.Nama = Data.Nama;
                        tb.Presentase = Data.Presentase;
                        tb.UpdatedBy = IdUser;
                        tb.UpdatedDate = commonService.GetCurrdate();
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditVariabel", ex);
            }

        }
        public string DeleteVariabel(int IdUser, int IdVariabel)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbVariabel tb = conn.Get<TbVariabel>(IdVariabel);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteVariabel", ex);
            }

        }
        #endregion
        #region pertanyaan
        public List<PertanyaanModel> GetPertanyaans(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    List<PertanyaanModel> ret = new List<PertanyaanModel>();
                    var data = conn.GetList<TbPertanyaan>();
                    if (data == null||data.Count()==0) { oMessage = "data tida ada"; return null; }
                    foreach (var item in data.OrderBy(x=>x.IdPertanyaan))
                    {
                        ret.Add(new PertanyaanModel
                        {
                            IdPertanyaan = item.IdPertanyaan,
                            Pertanyaan = item.Pertanyaan,
                            BobotDefault = item.BobotDefault
                        });
                    }
                    return ret;
                }
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetPertanyaans", ex);
                return null;
            }
        }
        public PertanyaanModel GetPertanyaan(int IdPertanyaan, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    TbPertanyaan item = conn.Get<TbPertanyaan>(IdPertanyaan);
                    if (item == null) { oMessage = "data tida ada"; return null; }
                    return new PertanyaanModel
                    {
                        IdPertanyaan = item.IdPertanyaan,
                        Pertanyaan = item.Pertanyaan,
                        BobotDefault = item.BobotDefault
                    };
                }
            }
            catch (System.Exception ex)
            {
                oMessage= commonService.GetErrorMessage(ServiceName + "GetPertanyaan", ex);
                return null;
            }
        }
        public string AddPertanyaan(int IdUser, string Pertanyaan, double? BobotDefault)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPertanyaan tb = (from a in conn.GetList<TbPertanyaan>()
                                           where a.Pertanyaan.Trim().ToLower() == Pertanyaan.Trim().ToLower()
                                           select a).FirstOrDefault();
                        if (tb != null) return "data sudah ada";
                        tb = new TbPertanyaan
                        {
                            Pertanyaan = Pertanyaan,
                            BobotDefault = BobotDefault
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditPertanyaan", ex);
            }
        }
        public string EditPertanyaan(int IdUser, PertanyaanModel Data)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPertanyaan tb = conn.Get<TbPertanyaan>(Data.IdPertanyaan);
                        if (tb == null) return "data tidak ada";
                        tb.Pertanyaan = Data.Pertanyaan;
                        tb.BobotDefault = Data.BobotDefault;
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditPertanyaan", ex);
            }
        }
        public string DeletePertanyaan(int IdUser, int IdPertanyaan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbPertanyaan tb = conn.Get<TbPertanyaan>(IdPertanyaan);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeletePertanyaan", ex);
            }
        }

        #endregion
        #region Indikator
        public List<IndikatorListModel> GetIndikators(out string oMessage)
        {
            oMessage = string.Empty;
            List<IndikatorListModel> ret = new List<IndikatorListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbIndikator>()
                               join b in conn.GetList<TbVariabel>() on a.IdVariabel equals b.IdVariabel
                               select new { a, b };
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    foreach (var item in data)
                    {
                        IndikatorListModel m = new IndikatorListModel();
                        m.IdIndikator = item.a.IdIndikator;
                        m.Variabel = item.b.Nama;
                        m.Nama = item.a.Nama;
                        m.Type = item.a.Type;
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetIndikators", ex);
                return null;
            }

        }

        public IndikatorVariabelModel GetIndikatorByIdVariabels(int IdVariabel, out string oMessage)
        {
            oMessage = string.Empty;
            IndikatorVariabelModel ret = new IndikatorVariabelModel();
            ret.Indikators = new List<IndikatorListModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var data = (from a in conn.GetList<TbIndikator>()
                               join b in conn.GetList<TbVariabel>() on a.IdVariabel equals b.IdVariabel
                               where a.IdVariabel == IdVariabel
                               select new { a, b }).ToList();

                    var getNamaVariabel = conn.Get<TbVariabel>(IdVariabel);
                    if (data == null || data.Count() == 0)
                    {
                        oMessage = "Data Indikator Tidak ada";
                        return null;
                    }

                    ret.Variabel = getNamaVariabel.Nama;
                    foreach (var item in data)
                    {
                        IndikatorListModel m = new IndikatorListModel();
                        m.IdIndikator = item.a.IdIndikator;
                        m.Variabel = item.b.Nama;
                        m.Nama = item.a.Nama;
                        m.Type = item.a.Type;
                        ret.Indikators.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetIndikatorByIdVariabels", ex);
                return null;
            }
        }
        public IndikatorModel GetIndikator(int IdIndikator, out string oMessage)
        {
            oMessage = string.Empty;
            IndikatorModel ret = new IndikatorModel();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    //var item = conn.Get<TbIndikator>(IdIndikator);
                    var item = (from a in conn.GetList<TbIndikator>()
                                join b in conn.GetList<TbVariabel>() on a.IdVariabel equals b.IdVariabel
                                where a.IdIndikator == IdIndikator
                                select new { a, b}
                                ).FirstOrDefault();
                    if (item == null)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }
                    ret.IdIndikator = item.a.IdIndikator;
                    ret.IdVariabel = item.a.IdVariabel;
                    ret.Nama = item.a.Nama;
                    ret.Type = item.a.Type;
                    ret.Variabel = item.b.Nama;
                }
                return ret;
            }
            catch (System.Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetIndikator", ex);
                return null;
            }
        }
        public string AddIndikator(int IdUser, int IdVariabel, string Nama, int Type)
        {
            try
            {
                if (IdVariabel <= 0) return "variabel harus dipilih";
                if (string.IsNullOrEmpty(Nama)) return "nama tidak boleh kosong";
                switch (Type)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        break;
                    default:
                        return "type salah";
                }
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbIndikator tb = new TbIndikator
                        {
                            IdVariabel = IdVariabel,
                            Nama = Nama,
                            Type = Type,
                            CreatedBy = IdUser, 
                            CreatedDate = commonService.GetCurrdate()
                        };
                        conn.Insert(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddIndikator", ex);
            }
        }
        public string EditIndikator(int IdUser, IndikatorModel Data)
        {
            try
            {
                if (Data.IdVariabel <= 0) return "variabel harus dipilih";
                if (string.IsNullOrEmpty(Data.Nama)) return "nama tidak boleh kosong";
                switch (Data.Type)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        break;
                    default:
                        return "type salah";
                }
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbIndikator tb = conn.Get<TbIndikator>(Data.IdIndikator);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        tb.IdVariabel = Data.IdVariabel;
                        tb.Nama = Data.Nama;
                        tb.Type = Data.Type;
                        tb.UpdatedBy = IdUser;
                        tb.UpdatedDate = commonService.GetCurrdate();
                        conn.Update(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "EditIndikator", ex);
            }

        }
        public string DeleteIndikator(int IdUser, int IdIndikator)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        TbIndikator tb = conn.Get<TbIndikator>(IdIndikator);
                        if (tb == null)
                        {
                            return "data tidak ada";
                        }
                        conn.Delete(tb);
                        tx.Commit();
                    }
                }
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteIndikator", ex);
            }

        }
        #endregion
        #region Indikator Pertanyaan
        public List<IndikatorPertanyaanModel> GetIndikatorPertanyaans(out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                List<IndikatorPertanyaanModel> ret = new List<IndikatorPertanyaanModel>();
                using(var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbIndikatorPertanyaan>()
                               join b in conn.GetList<TbIndikator>() on a.IdIndikator equals b.IdIndikator
                               join c in conn.GetList<TbPertanyaan>() on a.IdPertanyaan equals c.IdPertanyaan
                               join d in conn.GetList<TbJenisLokpri>() on a.IdJenisLokpri equals d.IdJenisLokpri
                               select new { a, b, c, d };
                    if(data==null || data.Count()==0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in data)
                    {
                        IndikatorPertanyaanModel m = new IndikatorPertanyaanModel
                        {
                            IdIndikatorPertanyaan = item.a.IdIndikatorPertanyaan,
                            BobotDefault = item.c.BobotDefault,
                            IdIndikator = item.a.IdIndikator,
                            IdJenisLokpri = item.a.IdJenisLokpri,
                            IdPertanyaan = item.a.IdPertanyaan,
                            Indikator = item.b.Nama,
                            JenisLokpri = item.d.Nama,
                            Pertanyaan = item.b.Nama
                        };
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage= commonService.GetErrorMessage(ServiceName + "GetIndikatorPertanyaans", ex);
                return null;
            }
        }

        public List<IndikatorPertanyaanModel> GetIndikatorPertanyaanByIdIndikator(int IdIndikator, out string oMessage)
        {
            oMessage = string.Empty;
            
            try
            {
                List<IndikatorPertanyaanModel> ret = new List<IndikatorPertanyaanModel>();
                using (var conn = commonService.DbConnection())
                {
                    var data = from a in conn.GetList<TbIndikatorPertanyaan>()
                               join b in conn.GetList<TbIndikator>() on a.IdIndikator equals b.IdIndikator
                               join c in conn.GetList<TbPertanyaan>() on a.IdPertanyaan equals c.IdPertanyaan
                               join d in conn.GetList<TbJenisLokpri>() on a.IdJenisLokpri equals d.IdJenisLokpri
                               where a.IdIndikator == IdIndikator
                               select new { a, b, c, d };
                    if (data == null || data.Count() == 0) { oMessage = "data tidak ada"; return null; }
                    foreach (var item in data)
                    {
                        IndikatorPertanyaanModel m = new IndikatorPertanyaanModel
                        {
                            IdIndikatorPertanyaan = item.a.IdIndikatorPertanyaan,
                            BobotDefault = item.c.BobotDefault,
                            IdIndikator = item.a.IdIndikator,
                            IdJenisLokpri = item.a.IdJenisLokpri,
                            IdPertanyaan = item.a.IdPertanyaan,
                            Indikator = item.b.Nama,
                            JenisLokpri = item.d.Nama,
                            Pertanyaan = item.c.Pertanyaan
                        };
                        ret.Add(m);
                    }
                
            }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + " GetIndikatorPertanyaanByIdIndikator", ex);
                return null;
            }
        }
        public IndikatorPertanyaanModel GetIndikatorPertanyaan(int IdIndikatorPertanyaan, out string oMessage)
        {
            oMessage = string.Empty;
            try
            {
                IndikatorPertanyaanModel ret = new IndikatorPertanyaanModel();
                using (var conn = commonService.DbConnection())
                {
                    var item = (from a in conn.GetList<TbIndikatorPertanyaan>()
                                join b in conn.GetList<TbIndikator>() on a.IdIndikator equals b.IdIndikator
                                join c in conn.GetList<TbPertanyaan>() on a.IdPertanyaan equals c.IdPertanyaan
                                join d in conn.GetList<TbJenisLokpri>() on a.IdJenisLokpri equals d.IdJenisLokpri
                                where a.IdIndikatorPertanyaan == IdIndikatorPertanyaan
                                select new { a, b, c, d }).FirstOrDefault();
                    if (item == null) { oMessage = "data tidak ada"; return null; }
                    ret = new IndikatorPertanyaanModel
                    {
                        IdIndikatorPertanyaan = item.a.IdIndikatorPertanyaan,
                        BobotDefault = item.c.BobotDefault,
                        IdIndikator = item.a.IdIndikator,
                        IdJenisLokpri = item.a.IdJenisLokpri,
                        IdPertanyaan = item.a.IdPertanyaan,
                        Indikator = item.b.Nama,
                        JenisLokpri = item.d.Nama,
                        Pertanyaan = item.b.Nama
                    };
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetIndikatorPertanyaan", ex);
                return null;
            }
        }
        public string AddIndikatorPertanyaan(int IdUser, int IdIndikator, int IdJenisLokpri, int IdPertanyaan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using(var tx = conn.BeginTransaction())
                    {
                        var item = (from a in conn.GetList<TbIndikatorPertanyaan>()
                                    join b in conn.GetList<TbIndikator>() on a.IdIndikator equals b.IdIndikator
                                    join c in conn.GetList<TbPertanyaan>() on a.IdPertanyaan equals c.IdPertanyaan
                                    join d in conn.GetList<TbJenisLokpri>() on a.IdJenisLokpri equals d.IdJenisLokpri
                                    where a.IdIndikator == IdIndikator
                                    & a.IdPertanyaan == IdPertanyaan
                                    & a.IdJenisLokpri == IdJenisLokpri
                                    select a).FirstOrDefault();
                        if (item == null)
                        {
                            item = new TbIndikatorPertanyaan
                            {
                                CreatedBy = IdUser,
                                CreatedDate = commonService.GetCurrdate(),
                                IdIndikator = IdIndikator,
                                IdJenisLokpri = IdJenisLokpri,
                                IdPertanyaan = IdPertanyaan
                            };
                            conn.Insert(item);
                        }
                        else
                        {
                            item.UpdatedBy = IdUser;
                            item.UpdatedDate = commonService.GetCurrdate();
                            conn.Update(item);
                        }
                        tx.Commit();
                    }
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
               return commonService.GetErrorMessage(ServiceName + "AddIndikatorPertanyaan", ex);
            }
        }
        public string AddIndikatorPertanyaan(int IdUser, int IdIndikator, int IdJenisLokpri, List<int> IdPertanyaan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        for (int i = 0; i < IdPertanyaan.Count(); i++)
                        {
                            var item = (from a in conn.GetList<TbIndikatorPertanyaan>()
                                        join b in conn.GetList<TbIndikator>() on a.IdIndikator equals b.IdIndikator
                                        join c in conn.GetList<TbPertanyaan>() on a.IdPertanyaan equals c.IdPertanyaan
                                        join d in conn.GetList<TbJenisLokpri>() on a.IdJenisLokpri equals d.IdJenisLokpri
                                        where a.IdIndikator == IdIndikator
                                        & a.IdPertanyaan == IdPertanyaan[i]
                                        & a.IdJenisLokpri == IdJenisLokpri
                                        select a).FirstOrDefault();
                            if (item == null)
                            {
                                item = new TbIndikatorPertanyaan
                                {
                                    CreatedBy = IdUser,
                                    CreatedDate = commonService.GetCurrdate(),
                                    IdIndikator = IdIndikator,
                                    IdJenisLokpri = IdJenisLokpri,
                                    IdPertanyaan = IdPertanyaan[i]
                                };
                                conn.Insert(item);
                            }
                            else
                            {
                                item.UpdatedBy = IdUser;
                                item.UpdatedDate = commonService.GetCurrdate();
                                conn.Update(item);
                            }
                        }                        
                        tx.Commit();
                    }
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "AddIndikatorPertanyaan", ex);
            }
        }
        public string DeleteIndikatorPertanyaan(int IdUser, int IdIndikatorPertanyaan)
        {
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    conn.Open();
                    using (var tx = conn.BeginTransaction())
                    {
                        var item = (from a in conn.GetList<TbIndikatorPertanyaan>()
                                    join b in conn.GetList<TbIndikator>() on a.IdIndikator equals b.IdIndikator
                                    join c in conn.GetList<TbPertanyaan>() on a.IdPertanyaan equals c.IdPertanyaan
                                    join d in conn.GetList<TbJenisLokpri>() on a.IdJenisLokpri equals d.IdJenisLokpri
                                    where a.IdIndikatorPertanyaan == IdIndikatorPertanyaan
                                    select a).FirstOrDefault();
                        if (item == null) return "data tidak ada";
                        conn.Delete(item);
                        tx.Commit();
                    }
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                return commonService.GetErrorMessage(ServiceName + "DeleteIndikatorPertanyaan", ex);
            }
        }
        #endregion
        #region MasterJenisVariabel
        public List<JenisVariabelModel> GetJenisVariabels(out string oMessage)
        {
            oMessage = string.Empty;
            List<JenisVariabelModel> ret = new List<JenisVariabelModel>();
            try
            {
                using (var conn = commonService.DbConnection())
                {
                    var datas = from a in conn.GetList<TbJenisVariabel>()
                               select a;
                    if (datas == null || datas.Count() == 0)
                    {
                        oMessage = "data tidak ada";
                        return null;
                    }

                    foreach (var item in datas)
                    {
                        JenisVariabelModel m = new JenisVariabelModel()
                        {
                            IdJenisVariabel = item.IdJenisVariabel,
                            Nama = item.Nama
                        };
                        ret.Add(m);
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                oMessage = commonService.GetErrorMessage(ServiceName + "GetJenisVariabels", ex);
                return null;
            }
        }

        




        #endregion

    }
}
