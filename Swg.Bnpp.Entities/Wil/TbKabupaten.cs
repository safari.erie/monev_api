using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Wil
{
    [Table("tb_kabupaten", Schema = "wil")]
    public class TbKabupaten
    {
        [Key]
        [Column("id_kabupaten")]
        public int IdKabupaten { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("id_provinsi")]
        public int IdProvinsi { get; set; }
    }
}
