using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Wil
{
    [Table("tb_provinsi", Schema = "wil")]
    public class TbProvinsi
    {
        [Key]
        [Column("id_provinsi")]
        public int IdProvinsi { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
