using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Wil
{
    [Table("tb_kecamatan", Schema = "wil")]
    public class TbKecamatan
    {
        [Key]
        [Column("id_kecamatan")]
        public int IdKecamatan { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("id_kabupaten")]
        public int IdKabupaten { get; set; }
        [Column("geom")]
        public string Geom { get; set; }
    }
}
