using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Org
{
    [Table("tb_jabatan", Schema = "org")]
    public class TbJabatan
    {
        [Key]
        [Column("id_jabatan")]
        public int IdJabatan { get; set; }
        [Column("kode")]
        public string Kode { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
