using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Org
{
    [Table("tb_kecamatan_profil", Schema = "org")]
    public class TbKecamatanProfil
    {
        [Key, Required]
        [Column("id_kecamatan")]
        public int IdKecamatan { get; set; }
        [Column("alamat")]
        public string Alamat { get; set; }
        [Column("longitude")]
        public double? Longitute { get; set; }
        [Column("latitude")]
        public double? Latitude { get; set; }
        //[Column("geom")]
        //public string Geom { get; set; }
    }
}
