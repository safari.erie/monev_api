using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Org
{
    [Table("tb_pegawai_kecamatan", Schema = "org")]
    public class TbPegawaiKecamatan
    {
        [Key, Required]
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Column("id_kecamatan")]
        public int IdKecamatan { get; set; }
        [Column("id_jabatan")]
        public int IdJabatan { get; set; }
    }
}
