using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Public
{
    [Table("tb_appl", Schema = "public")]
    public class TbAppl
    {
        [Key]
        [Column("id_appl")]
        public int IdAppl { get; set; }
        [Column("appl_name")]
        public string ApplName { get; set; }
    }
}
