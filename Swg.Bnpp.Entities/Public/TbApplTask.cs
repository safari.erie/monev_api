using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Public
{
    [Table("tb_appl_task", Schema = "public")]
    public class TbApplTask
    {
        [Key]
        [Column("id_appl_task")]
        public int IdApplTask { get; set; }
        [Column("id_appl_task_parent")]
        public int? IdApplTaskParent { get; set; }
        [Column("id_appl")]
        public int IdAppl { get; set; }
        [Column("appl_task_name")]
        public string ApplTaskName { get; set; }
        [Column("controller_name")]
        public string ControllerName { get; set; }
        [Column("action_name")]
        public string ActionName { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [Column("icon_name")]
        public string IconName { get; set; }
    }
}
