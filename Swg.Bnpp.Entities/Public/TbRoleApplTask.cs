using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Public
{
    [Table("tb_role_appl_task", Schema = "public")]
    public class TbRoleApplTask
    {
        [Key, Required]
        [Column("id_role")]
        public int IdRole { get; set; }
        [Key, Required]
        [Column("id_appl_task")]
        public int IdApplTask { get; set; }
    }
}
