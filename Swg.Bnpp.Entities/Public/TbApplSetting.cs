using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Public
{
    [Table("tb_appl_setting", Schema = "public")]
    public class TbApplSetting
    {
        [Key, Required]
        [Column("code")]
        public string Code { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("value_type")]
        public int ValueType { get; set; }
        [Column("value_date")]
        public DateTime? ValueDate { get; set; }
        [Column("value_number")]
        public double? ValueNumber { get; set; }
        [Column("value_string")]
        public string ValueString { get; set; }
    }
}
