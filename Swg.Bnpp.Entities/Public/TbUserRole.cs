using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Public
{
    [Table("tb_user_role", Schema = "public")]
    public class TbUserRole
    {
        [Key, Required]
        [Column("id_user")]
        public int IdUser { get; set; }
        [Key, Required]
        [Column("id_role")]
        public int IdRole { get; set; }
    }
}
