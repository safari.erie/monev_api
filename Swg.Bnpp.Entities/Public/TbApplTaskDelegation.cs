using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Public
{
    [Table("tb_appl_task_delegation", Schema = "public")]
    public class TbApplTaskDelegation
    {
        [Key]
        [Column("id_appl_task_delegation")]
        public int IdApplTaskDelegation { get; set; }
        [Column("id_appl_task")]
        public int IdApplTask { get; set; }
        [Column("delegate_for")]
        public int DelegateFor { get; set; }
        [Column("delegate_by")]
        public int DelegateBy { get; set; }
        [Column("approved_by")]
        public int ApprovedBy { get; set; }
        [Column("start_date")]
        public DateTime StartDate { get; set; }
        [Column("end_date")]
        public DateTime EndDate { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
