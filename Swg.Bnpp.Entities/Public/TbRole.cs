using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Public
{
    [Table("tb_role", Schema = "public")]
    public class TbRole
    {
        [Key]
        [Column("id_role")]
        public int IdRole { get; set; }
        [Column("role_name")]
        public string RoleName { get; set; }
    }
}
