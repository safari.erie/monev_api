using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_variabel", Schema = "mnv")]
    public class TbVariabel
    {
        [Key]
        [Column("id_variabel")]
        public int IdVariabel { get; set; }
        [Column("id_jenis_variabel")]
        public int IdJenisVariabel { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("presentase")]
        public int Presentase { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
