using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_jenis_lokpri", Schema = "mnv")]
    public class TbJenisLokpri
    {
        [Key, Required]
        [Column("id_jenis_lokpri")]
        public int IdJenisLokpri { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
