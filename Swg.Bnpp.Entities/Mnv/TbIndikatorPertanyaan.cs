﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_indikator_pertanyaan", Schema = "mnv")]
    public class TbIndikatorPertanyaan
    {
        [Key]
        [Column("id_indikator_pertanyaan")]
        public int IdIndikatorPertanyaan { get; set; }
        [Column("id_pertanyaan")]
        public int IdPertanyaan { get; set; }
        [Column("id_indikator")]
        public int IdIndikator { get; set; }
        [Column("id_jenis_lokpri")]
        public int IdJenisLokpri { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
