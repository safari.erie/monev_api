using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_jenis_variabel", Schema = "mnv")]
    public class TbJenisVariabel
    {
        [Key]
        [Column("id_jenis_variabel")]
        public int IdJenisVariabel { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
    }
}
