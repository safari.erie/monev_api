using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_jawaban", Schema = "mnv")]
    public class TbJawaban
    {
        [Key]
        [Column("id_jawaban")]
        public int IdJawaban { get; set; }
        [Column("id_indikator_pertanyaan")]
        public int IdIndikatorPertanyaan { get; set; }
        [Column("id_monev_detil")]
        public int IdMonevDetil { get; set; }
        [Column("ketersediaan")]
        public string Ketersediaan { get; set; }
        [Column("bobot_ketersediaan")]
        public double? BobotKetersediaan { get; set; }
        [Column("kecukupan")]
        public string Kecukupan { get; set; }
        [Column("bobot_kecukupan")]
        public double? BobotKecukupan { get; set; }
        [Column("kondisi")]
        public string Kondisi { get; set; }
        [Column("bobot_kondisi")]
        public double? BobotKondisi { get; set; }
        [Column("kebutuhan")]
        public double? Kebutuhan { get; set; }
        [Column("keterisian")]
        public double? Keterisian { get; set; }
        [Column("bobot_keb_ket")]
        public double? BobotKebKet { get; set; }
        [Column("jumlah")]
        public double? Jumlah { get; set; }
        [Column("bobot_nilai")]
        public double? BobotNilai { get; set; }
        [Column("jumlah_bobot_nilai")]
        public double? JumlahBobotNilai { get; set; }
    }
}
