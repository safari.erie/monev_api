using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_bobot_kecukupan", Schema = "mnv")]
    public class TbBobotKecukupan
    {
        [Key]
        [Column("id_bobot_kecukupan")]
        public int IdBobotKecukupan { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("bobot")]
        public double Bobot { get; set; }
        [Column("status")]
        public int Status { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
