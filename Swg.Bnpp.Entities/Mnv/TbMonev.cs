using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_monev", Schema = "mnv")]
    public class TbMonev
    {
        [Key]
        [Column("id_monev")]
        public int IdMonev { get; set; }
        [Column("id_jenis_lokpri")]
        public int IdJenisLokpri { get; set; }
        [Column("id_pegawai")]
        public int IdPegawai { get; set; }
        [Column("rata_rata_ciq")]
        public double RataRataCiq { get; set; }
        [Column("rata_rata_pap")]
        public double RataRataPap { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
