using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_pertanyaan", Schema = "mnv")]
    public class TbPertanyaan
    {
        [Key]
        [Column("id_pertanyaan")]
        public int IdPertanyaan { get; set; }
        [Column("pertanyaan")]
        public string Pertanyaan { get; set; }
        [Column("bobot_default")]
        public double? BobotDefault { get; set; }
    }
}
