using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_monev_detil", Schema = "mnv")]
    public class TbMonevDetil
    {
        [Key]
        [Column("id_monev_detil")]
        public int IdMonevDetil { get; set; }
        [Column("id_variabel")]
        public int IdVariabel { get; set; }
        [Column("id_monev")]
        public int IdMonev { get; set; }
        [Column("rata2")]
        public double Rata2 { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
