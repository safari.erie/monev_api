using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Swg.Bnpp.Entities.Mnv
{
    [Table("tb_indikator", Schema = "mnv")]
    public class TbIndikator
    {
        [Key]
        [Column("id_indikator")]
        public int IdIndikator { get; set; }
        [Column("id_variabel")]
        public int IdVariabel { get; set; }
        [Column("type")]
        public int Type { get; set; }
        [Column("nama")]
        public string Nama { get; set; }
        [Column("created_by")]
        public int CreatedBy { get; set; }
        [Column("created_date")]
        public DateTime CreatedDate { get; set; }
        [Column("updated_by")]
        public int? UpdatedBy { get; set; }
        [Column("updated_date")]
        public DateTime? UpdatedDate { get; set; }
    }
}
